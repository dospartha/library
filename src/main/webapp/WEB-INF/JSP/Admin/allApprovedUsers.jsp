<title>All Approved Users</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">All Approved Users Details</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Name</th>
                                <th>Email Id</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="modal fade" id="userModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"> User Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="masonry-item col-md-12">
                                            <div class="bgc-white p-20 bd">
                                                <fieldset disabled="disabled">
                                                    <div class="form-row">
                                                        <input type="hidden" id="id">

                                                        <div class="form-group col-md-3">
                                                            <label for="name">Name</label>
                                                            <input type="text" class="form-control" id="name"
                                                                   name="name">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="email">Email</label>
                                                            <input type="text" class="form-control" id="email"
                                                                   name="email">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="email">Contact No</label>
                                                            <div><input type="text" class="form-control" id="contactNo"
                                                                        name="contactNo"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="dob" class="col-form-label">Date of
                                                                Birth</label>
                                                            <div><input type="text" class="form-control" id="dob"
                                                                        name="dob"></div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label for="gender" class="col-form-label">Gender</label>
                                                            <input type="text" class="form-control" id="gender"
                                                                   name="gender">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="adharNo" class="col-form-label">Adhar Card
                                                                No</label>
                                                            <div><input type="text" class="form-control" id="adharNo"
                                                                        name="adharNo"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="address" class="col-form-label">Address</label>
                                                            <div><input type="text" class="form-control" id="address"
                                                                        name="address"></div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label for="libraryCode">Library Code</label>
                                                            <input type="text" class="form-control" id="libraryCode"
                                                                   name="name">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="libraryName">Library Name</label>
                                                            <input type="text" class="form-control" id="libraryName"
                                                                   name="name">
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label for="librarySubDivision" class="col-form-label">Library
                                                                Sub-Division</label>
                                                            <div><input type="text" class="form-control"
                                                                        name="librarySubDivision"
                                                                        id="librarySubDivision"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="blockMuniDetails" class="col-form-label">
                                                                Library Block/Municipality</label>
                                                            <input type="text" class="form-control"
                                                                   name="blockMuniDetails"
                                                                   id="blockMuniDetails">
                                                        </div>
                                                        <div class="form-group col-md-3" style="display: none">
                                                            <label for="panchayetDetails" class="col-form-label">Library
                                                                Panchayet</label>
                                                            <input type="text" class="form-control" name="panchayetDetails" id="panchayetDetails">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>--%>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal"> Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllApprovedUsers",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Scheme', className: 'btn-sm'},
                {extend: 'pdf', title: 'Scheme', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "name"},
                {data: "email"},
                {defaultContent: "<button id='details' class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"}
            ]
        });
        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
            console.log(data);
            $('#libraryCode').val(data.library.libraryCode);
            $('#libraryName').val(data.library.libraryName);
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#email').val(data.email);
            $('#contactNo').val(data.contactNo);
            $('#dob').val(data.dob);
            $('#gender').val(data.gender);
            $('#adharNo').val(data.adharNo);
            $('#address').val(data.address);
            if (data.library.municipalityDetails != null) {
                $('#panchayetDetails').parent('div').hide();
                $('#librarySubDivision').val(data.library.municipalityDetails.subDivisionDetails.name);
                $('#blockMuniDetails').val(data.library.municipalityDetails.name);
            } else {
                $('#panchayetDetails').parent('div').show();
                $('#librarySubDivision').val(data.library.panchayetDetails.blockDetails.subDivisionDetails.name);
                $('#blockMuniDetails').val(data.library.panchayetDetails.blockDetails.name);
                $('#panchayetDetails').val(data.library.panchayetDetails.name);
            }
        });
        $('.fa-chevron-up').click(function () {
            $('.hpanel').show();
        });
    });
</script>