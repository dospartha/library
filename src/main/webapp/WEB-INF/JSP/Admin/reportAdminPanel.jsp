<title>Report Section Admin Panel</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">


                <div class="alert alert-danger alert-dismissible " role="alert" id="messsage" style="display: none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Please select</strong> the <b>Dates</b> between in which you want to see the Courses....
                </div>


                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Monthly-Yearly and Audit Report</h4>
                    <div class="mT-30">
                        <form name="searchBox" id="searchBox">
                            <!-- for alert -->
                            <input type="hidden" id="status" value="${status}">

                            <div class="form-row">
                                <!--                                <div class="form-group col-md-4">
                                                                    <label for="inputForwardBy"> Notice Send By </label>
                                                                    <select name="forwardBy" id="forwardBy" class="form-control">
                                                                        <option value="" selected hidden> --Select-- </option>
                                <%String forwardBy[] = {"Librarian",
                                        "Admin",
                                        "User"};
                                    for (int i = 0; i < 3; i++) {
                                %>
                                <option><%= forwardBy[i]%></option>
                                <%}%>
                            </select>
                        </div>-->
                                <div class="form-group col-md-4">
                                    <label for="inputLibraryCode"> Library Code or Id </label>
                                    <input type="text" class="form-control" name="libraryCode" id="libraryCode" placeholder="Library Code or Id">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputLibraryName"> Library Name </label>
                                    <input type="text" class="form-control" name="libraryName" id="libraryName" placeholder="Library Name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputLibrarianName"> Librarian Name </label>
                                    <input type="text" class="form-control" name="librarianName" id="librarianName" placeholder="Librarian Name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="inputDate"> Date Range </label><font color="red">*</font>
                                    <div class="input-group input-daterange">
                                        <input id="fromDate" name="entryDate" type="text" class="form-control" autocomplete="off">
                                        <span class="input-group-addon"> 
                                            <span class="fa fa-calendar"></span>
                                        </span> 
                                        <span class="input-group-addon">to</span> 
                                        <input id="toDate" name="endDate" type="text" class="form-control">
                                        <span class="input-group-addon"> 
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <font color="red">*</font> Check between two Dates....
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="button" class="btn btn-primary btn-block mT-30" id="search"><i class="c-white-500 fa fa-search" aria-hidden="true"></i>&nbsp&nbsp&nbsp&nbsp&nbspSearch</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Monthly-Yearly and Audit Report</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Library Code</th>
                                    <th>Type of Report</th>
                                    <th>Date of Upload</th>
                                    <th>View</th>
                                    <th>Status</th>
                                    <th>Approve</th>
                                    <th>Reject</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Library Code</th>
                                    <th>Type of Report</th>
                                    <th>Date of Upload</th>
                                    <th>View</th>
                                    <th>Status</th>
                                    <th>Approve</th>
                                    <th>Reject</th>

                                </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/moment.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DatePicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery_Toastr/toastr.min.js"></script>

<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getAllReports/report",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],

            columns: [
                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "libraryCode"},
                {data: "reportType"},
                {data: "entryDate",
                    render: function (data, type, row, meta) {
                        var dateTime = dateTimeFormat(data);
                        return dateTime;
                    }
                },
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '"><button type="button" class="btn btn-primary">' + 'view' + '  <i class=\"c-white-500 fa fa-external-link\"></i></button></a>';
                    }
                },
                {data: "status",
                    render: function (data, type, row) {
                        if (data === 1) {
                            return "<span class=\"badge badge-pill badge-success lh-0 p-15\"> Approved </span>";
                        }
                        if (data === 0) {
                            return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                        }
                        if (data === -1) {
                            return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                        }
                    }
                },
                {defaultContent: "<button id='approve' class=\"btn btn-success\" type=\"button\"><i class=\"c-white-500 fa fa-check\"></i> Approve </button>"},
                {defaultContent: "<button id='reject' class=\"btn btn-danger\" type=\"button\"><i class=\"c-white-500 fa fa-trash-o\"></i> Reject </button>"}
            ]
        });
        $("#fromDate,#toDate").datepicker({
            autoclose: true,
            format: 'dd/M/yyyy'
        });
        $(document).ready(function () {
            $('.input-daterange').datepicker({
            });
        });
        $('#search').click(function () {
            if ($('#fromDate').val() === "" || $('#toDate').val() === "") {
                $('#messsage').css('display', 'block');
                setTimeout(function () {
                    $('.alert-dismissible').css('display', 'none');
                }, 8000);
            } else {
                $('#dTable').dataTable().fnDestroy();
                table = $('#dTable').DataTable({
                    "ajax": {
                        "url": "./searchReport",
                        "type": "post",
                        "data": function () {
                            return $('#searchBox').serialize();
                        }
                    },
                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                    "lengthMenu": [10, 25, 50, 100],
                    "ordering": false,

                    columns: [
                        {data: "id", "visible": false},
                        {data: null,
                            render: function (data, type, row, meta) {
                                return (meta.row + meta.settings._iDisplayStart + 1);
                            }
                        },
                        {data: "libraryCode"},
                        {data: "reportType"},
                        {data: "entryDate",
                            render: function (data, type, row, meta) {
                                var dateTime = dateTimeFormat(data);
                                return dateTime;
                            }
                        },
                        {data: "attachment",
                            render: function (data, type, row, meta) {
                                return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '"><button type="button" class="btn btn-primary">' + 'view' + '  <i class=\"c-white-500 fa fa-external-link\"></i></button></a>';
                            }
                        },
                        {data: "status",
                            render: function (data, type, row) {
                                if (data === 1) {
                                    return "<span class=\"badge badge-pill badge-success lh-0 p-15\"> Approved </span>";
                                }
                                if (data === 0) {
                                    return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                                }
                                if (data === -1) {
                                    return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                                }
                            }
                        },
                        {defaultContent: "<button id='approve' class=\"btn btn-success\" type=\"button\"><i class=\"c-white-500 fa fa-check\"></i> Approve </button>"},
                        {defaultContent: "<button id='reject' class=\"btn btn-danger\" type=\"button\"><i class=\"c-white-500 fa fa-trash-o\"></i> Reject </button>"}
                    ]
                });
            }
        });
        function dateTimeFormat(data) {
            var month = ["January", "February", "March", "April",
                "May", "June", "July", "August",
                "September", "October", "November", "December"];
            var date = data.substr(0, 10);
            var time = data.substr(11, 8);

            var dd = date.substr(8, 2);
            var M = date.substr(5, 2);
            var yyyy = date.substr(0, 4);
            var hh = time.substr(0, 2);
            var mm = time.substr(3, 2);
            var ss = time.substr(6, 2);

            dd = parseInt(dd, 10);
            M = parseInt(M, 10);
            yyyy = parseInt(yyyy, 10);
            hh = parseInt(hh, 10);
            mm = parseInt(mm, 10);
            ss = parseInt(ss, 10);

            var hm = 0;
            mm = mm + 30;
            if (mm > 60) {
                hm = 1;
                mm = mm - 60;
            }
            hh = hh + 5 + hm;
            var timespan = 'AM';
            if (hh >= 12) {
                if (hh > 12) {
                    hh = hh - 12;
                }
                timespan = 'PM';
            }

            var dateTime = n(dd) + "/" + month[M - 1] + "/" + yyyy + "&nbsp&nbsp" + n(hh) + ":" + n(mm) + ":" + n(ss) + timespan;
            return dateTime;
        }
        ;
        function n(n) {
            return n > 9 ? "" + n : "0" + n;
        }
        ;
        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();

            $('#id').val(data.id);
            $('#subjectModal').val(data.subject);
            $('#addedByModal').val(data.addedBy);
            var dateOfNotification = moment(data.addedDate).format('DD/MMM/YYYY LT');
            $('#addedDateModal').val(dateOfNotification);
            $('#descriptionModal').val(data.description);
            var path = "${pageContext.request.contextPath}/resources" + (data.attachment);
            $('#attachmentModal').attr('href', path);
            var attachmentFileName = (data.attachment).substr((data.attachment).indexOf("r") + 2);
            $('#attachmentFileNameModal').html(attachmentFileName);
            $('#sendToModal').val(data.forwardTo);
        });
        $('#dTable tbody').on('click', 'a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#attachmentModal').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#dTable tbody').on('click', '#approve', function () {
            var job = {};
            var data = table.row($(this).parents('tr')).data();
            job["id"] = data.id;
            $.ajax({
                url: "./approveReport",
                type: 'POST',
                data: JSON.stringify(job),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        $(document).ready(function () {
                            table.ajax.reload("Report is Granted.");
                        });
                    } else {
                        toastr["error"]("Report is not Granted.");
                    }
                }
            });
        });
        $('#dTable tbody').on('click', '#reject', function () {
            var job = {};
            var data = table.row($(this).parents('tr')).data();
            job["id"] = data.id;
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this "+data.reportType+" ?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function () {
                $.ajax({
                    url: "./rejectReport",
                    type: 'POST',
                    data: JSON.stringify(job),
                    dataType: "json",
                    contentType: 'application/json',
                    complete: function (data) {
                        if (data.responseText === "SUCCESS") {
                            $(document).ready(function () {
                                table.ajax.reload();
                                toastr["success"]("Report is canceled.");
                            });
                        } else {
                            toastr["error"]("Report is not canceled.");
                        }
                    }
                });
            });
        });
    });
</script>
<script type="text/javascript">
    $('#libraryCode').focusout(function () {
        var libraryCode = document.getElementById("libraryCode").value;
        console.log(libraryCode);
        $.ajax({
            url: "./getLibraryInformations/" + libraryCode,
            type: 'GET',
            success: function (data) {
                console.log(data);
                document.getElementById("libraryName").value = data[0];
                document.getElementById("librarianName").value = data[1];
            }
        });
    });
</script>