<title>Notification Details Admin Panel</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">


                <div class="alert alert-danger alert-dismissible " role="alert" id="messsage" style="display: none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Please select</strong> the <b>Dates</b> between in which you want to see the Courses....
                </div>


                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Notification</h4>
                    <div class="mT-30">
                        <form name="searchBox" id="searchBox">
                            <!-- for alert -->
                            <input type="hidden" id="status" value="${status}">

                            <div class="form-row">
                                <!--                                <div class="form-group col-md-4">
                                                                    <label for="inputForwardBy"> Notice Send By </label>
                                                                    <select name="forwardBy" id="forwardBy" class="form-control">
                                                                        <option value="" selected hidden> --Select-- </option>
                                <%String forwardBy[] = {"Librarian",
                                        "Admin",
                                        "User"};
                                    for (int i = 0; i < 3; i++) {
                                %>
                                <option><%= forwardBy[i]%></option>
                                <%}%>
                            </select>
                        </div>-->
                                <div class="form-group col-md-4">
                                    <label for="inputLibraryCode"> Library Code or Id </label>
                                    <input type="text" class="form-control" name="libraryCode" id="libraryCode" placeholder="Library Code or Id">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputLibraryName"> Library Name </label>
                                    <input type="text" class="form-control" name="libraryName" id="libraryName" placeholder="Library Name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputLibrarianName"> Librarian Name </label>
                                    <input type="text" class="form-control" name="librarianName" id="librarianName" placeholder="Librarian Name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="inputDate"> Date Range </label><font color="red">*</font>
                                    <div class="input-group input-daterange">
                                        <input id="fromDate" name="entryDate" type="text" class="form-control" autocomplete="off">
                                        <span class="input-group-addon"> 
                                            <span class="fa fa-calendar"></span>
                                        </span> 
                                        <span class="input-group-addon">to</span> 
                                        <input id="toDate" name="endDate" type="text" class="form-control">
                                        <span class="input-group-addon"> 
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <font color="red">*</font> Check between two Dates....
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="button" class="btn btn-primary btn-block mT-30" id="search"><i class="c-white-500 fa fa-search" aria-hidden="true"></i>&nbsp&nbsp&nbsp&nbsp&nbspSearch</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Notices</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Library Code</th>
                                    <th>Subject</th>
                                    <th>Notification send To</th>
                                    <th>Forward By</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Description</th>
                                    <th>Attached File</th>
                                    <th>Status</th>
                                    <th>Details</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Library Code</th>
                                    <th>Subject</th>
                                    <th>Notification send To</th>
                                    <th>Forward By</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Description</th>
                                    <th>Attached File</th>
                                    <th>Status</th>
                                    <th>Details</th>

                                </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">View Notifications Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="masonry-item col-md-12">
                                            <div class="bgc-white p-20 bd">
                                                <div class="mT-30">
                                                    <fieldset disabled="disabled">
                                                        <div class="form-row">
                                                            <input type="hidden" id="id">
                                                            <div class="form-group col-md-4">
                                                                <label for="inputsubject"> Subject</label>
                                                                <input type="text" class="form-control" name="subject" id="subjectModal">
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputForwardBy"> Added by</label>
                                                                <input type="text" class="form-control" name="addedBy" id="addedByModal">
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputDate"> Date of Notification</label>
                                                                <input type="text" class="form-control" name="addedDate" id="addedDateModal">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="inputDescription"> Description of Notification</label>
                                                                <textarea name="description" id="descriptionModal" class="form-control" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="inputAttachment"> Attachment</label>
                                                                <div style="padding: 8px;border: 1px solid rgba(0, 0, 0, 0.22)!important;border-radius: 5px;">
                                                                    <a id="attachmentModal" href=""><span id="attachmentFileNameModal" value=""></span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label for="inputFowardTo"> Forward To</label>
                                                            <select name="sendTo" id="sendToModal" class="form-control">
                                                                <option value="" selected disabled hidden> -- Select -- </option>
                                                                <%
                                                                    String person[] = {"Library Members",
                                                                        "Admin",
                                                                        "Both"};
                                                                    for (int i = 0; i < 3; i++) {
                                                                %>
                                                                <option value="<%= person[i]%>"><%= person[i]%></option>
                                                                <%}%>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" id="approve" data-dismiss="modal"><i class="c-white-500 fa fa-check"></i> Approve </button>
                                        <button type="button" class="btn btn-danger" id="reject" data-dismiss="modal"><i class="c-white-500 fa fa-trash-o"></i> Reject </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/moment.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DatePicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery_Toastr/toastr.min.js"></script>

<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getAllNotifications",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "libraryCode"},
                {data: "subject"},
                {data: "sendTo"},
                {data: "forwardBy"},
                {data: "notificationDate"},
                {data: "notificationTime"},
                {data: "description"},
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '">' + data.substr(data.indexOf("r") + 2) + '</a>';
                    }
                },
                {data: "status",
                    render: function (data, type, row) {
                        if (data === 1) {
                            return "<span class=\"badge badge-pill badge-success lh-0 p-15\"> Approved </span>";
                        }
                        if (data === 0) {
                            return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                        }
                        if (data === -1) {
                            return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                        }
                    }
                },
                {defaultContent: "<button id='details' class=\"btn cur-p btn-info\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details <i class=\"c-white-500 fa fa-info-circle\" aria-hidden=\"true\"></i></button>"}
            ]
        });
        $("#fromDate,#toDate").datepicker({
            autoclose: true,
            format: 'dd/M/yyyy'
        });
        $(document).ready(function () {
            $('.input-daterange').datepicker({
            });
        });
        $('#search').click(function () {
            if ($('#fromDate').val() === "" || $('#toDate').val() === "") {
                $('#messsage').css('display', 'block');
                setTimeout(function () {
                    $('.alert-dismissible').css('display', 'none');
                }, 8000);
            } else {
                $('#dTable').dataTable().fnDestroy();
                table = $('#dTable').DataTable({
                    "ajax": {
                        "url": "./searchNotification",
                        "type": "post",
                        "data": function () {
                            return $('#searchBox').serialize();
                        }
                    },
                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                    "lengthMenu": [10, 25, 50, 100],
                    "ordering": false,

                    columns: [
                        {data: "id", "visible": false},
                        {data: null,
                            render: function (data, type, row, meta) {
                                return (meta.row + meta.settings._iDisplayStart + 1);
                            }
                        },
                        {data: "libraryCode"},
                        {data: "subject"},
                        {data: "sendTo"},
                        {data: "forwardBy"},
                        {data: "notificationDate"},
                        {data: "notificationTime"},
                        {data: "description"},
                        {data: "attachment",
                            render: function (data, type, row, meta) {
                                return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '">' + data.substr(data.indexOf("r") + 2) + '</a>';
                            }
                        },
                        {data: "status",
                            render: function (data, type, row) {
                                if (data === 1) {
                                    return "<span class=\"badge badge-pill badge-success lh-0 p-15\"> Approved </span>";
                                }
                                if (data === 0) {
                                    return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                                }
                                if (data === -1) {
                                    return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                                }
                            }
                        },
                        {defaultContent: "<button id='details' class=\"btn cur-p btn-info\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details <i class=\"c-white-500 fa fa-info-circle\" aria-hidden=\"true\"></i></button>"}
                    ]
                });
            }
        });
        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();

            $('#id').val(data.id);
            $('#subjectModal').val(data.subject);
            $('#addedByModal').val(data.addedBy);
            var dateOfNotification = moment(data.addedDate).format('DD/MMM/YYYY LT');
            $('#addedDateModal').val(dateOfNotification);
            $('#descriptionModal').val(data.description);
            var path = "${pageContext.request.contextPath}/resources" + (data.attachment);
            $('#attachmentModal').attr('href', path);
            var attachmentFileName = (data.attachment).substr((data.attachment).indexOf("r") + 2);
            $('#attachmentFileNameModal').html(attachmentFileName);
            $('#sendToModal').val(data.forwardTo);
        });
        $('#dTable tbody').on('click', 'a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#attachmentModal').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#approve').click(function () {
            var job = {};
            job["id"] = $("#id").val();
            job["sendTo"] = $('#sendToModal').val();
            console.log(job);
            $.ajax({
                url: "./approveNotification",
                type: 'POST',
                data: JSON.stringify(job),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        $(document).ready(function () {
                            table.ajax.reload(null, false);
                            toastr["success"]("Notification is approved.");
                        });
                    } else {
                        toastr["error"]("Notification is not approved.");
                    }
                }
            });
        });
        $('#reject').click(function () {
            var job = {};
            job["id"] = $("#id").val();
            console.log(job);
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this notification?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function () {
                $.ajax({
                    url: "./rejectNotification",
                    type: 'POST',
                    data: JSON.stringify(job),
                    dataType: "json",
                    contentType: 'application/json',
                    complete: function (data) {
                        if (data.responseText === "SUCCESS") {
                            $(document).ready(function () {
                                table.ajax.reload();
                                toastr["success"]("Notification deleted is canceled.");
                            });
                        } else {
                            toastr["error"]("Notification is not canceled.");
                        }
                    }
                });
            });
        });
    });
</script>
<script type="text/javascript">
    $('#libraryCode').focusout(function () {
        var libraryCode = document.getElementById("libraryCode").value;
        console.log(libraryCode);
        $.ajax({
            url: "./getLibraryInformations/" + libraryCode,
            type: 'GET',
            success: function (data) {
                console.log(data);
                document.getElementById("libraryName").value = data[0];
                document.getElementById("librarianName").value = data[1];
            }
        });
    });
</script>