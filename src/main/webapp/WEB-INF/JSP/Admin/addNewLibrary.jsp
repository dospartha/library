<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Add New Library</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> New Library is Successfully Added.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add New Library</h4>
                    <div class="mT-30">
                        <form method="post" action="./addLibrary" name="addLibrary" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="libraryCode"> Library Code </label>
                                    <input type="text" class="form-control" name="libraryCode" id="libraryCode"
                                           placeholder="Enter Library Code" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="libraryName"> Library Name </label>
                                    <input type="text" class="form-control" name="libraryName" id="libraryName"
                                           placeholder="Enter Library Name" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="email"> Email Id </label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="Enter Email Id" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="contactNumber"> Contact Number </label>
                                    <input type="text" class="form-control" name="contactNumber" id="contactNumber"
                                           placeholder="Enter Contact Number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group col-lg-3">
                                    <label>Select Sub-Division</label>
                                    <select class="form-control" name="sdoId" id="sdoId" required>
                                        <option value="">Select</option>
                                        <c:forEach var="allsdo" items="${allSDO}">
                                            <option value="${allsdo.id}">${allsdo.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>Select Block/Municipality</label>
                                    <select class="form-control" id="blockMuniId" name="blockMuniId" required>
                                        <option value="">Select</option>
                                        <option value="1">Block</option>
                                        <option value="2">Municipality</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3" style="display: none">
                                    <label>Select Corresponding Area</label>
                                    <select class="form-control" id="municipalityId" name="municipalityDetails">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>Select Corresponding Area</label>
                                    <select class="form-control" id="blockId" name="blockDetails">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3" <%--style="display: none"--%>>
                                    <label>Select Corresponding Panchayet</label>
                                    <select class="form-control" id="panchayetId" name="panchayetDetails">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="latitude"> Latitude </label>
                                    <input type="text" class="form-control" name="latitude" id="latitude"
                                           placeholder="Latitude value" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="longitude"> Longitude </label>
                                    <input type="text" class="form-control" name="longitude" id="longitude"
                                           placeholder="Longitude value" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="image">Upload Image</label>
                                    <input id="image" type="file" required="" name="imageLibrary"
                                           placeholder="Upload Image" class="form-control">
                                    <div class="invalid-feedback">Please provide the your image.</div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label>Select Librarian</label>
                                    <select class="form-control" name="librarian" id="librarian" required>
                                        <option value="">Select</option>
                                        <c:forEach var="alllibrarian" items="${allLibrarian}">
                                            <option value="${alllibrarian.id}">${alllibrarian.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <button type="submit" class="btn btn-space btn-primary" style=" margin-top: 29px">Add
                                    Library
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Library</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Library Code</th>
                                <th>Library Name</th>
                                <th>Email Id</th>
                                <th>Contact No</th>
                            </tr>
                            </thead>
                            <%--<tfoot>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Library Code</th>
                                <th>Library Name</th>
                                <th>Email Id</th>
                                <th>Contact No</th>
                                <th>Locality</th>
                                <th>Police Station</th>
                                <th>District</th>
                                <th>State</th>
                            </tr>
                            </tfoot>--%>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    $('#sdoId').change(function () {
        if ($('#blockMuniId').val() != 'Select') {
            if ($('#blockMuniId').val() == 1) {
                $('#blockId').parent('div').show();
                $('#blockId').html("");
                $.ajax({
                    url: "./getAllBdoDataBySDO?sdoId=" + $('#sdoId').val(),
                    type: "GET",
                    async: false,
                    success: function (data) {
                        console.log(data)

                        var strVar = "";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"\">Select <\/option>";
                            for (var i = 0; i < data.length; i++) {
                                strVar += " <option value=\"" + data[i].id + "\" >"
                                    + data[i].name
                                    + "<\/option>";
                            }
                        }
                        $('#blockId').html(strVar);
                    }
                });
            } else if ($('#blockMuniId').val() == 2) {
                $('#blockId').parent('div').hide();
                $("#municipalityId").parent('div').css("display", "block");
                $('#municipalityId').html("");
                $.ajax({
                    url: "./getAllMunicipalityDataBySDO?sdoId=" + $('#sdoId').val(),
                    type: "GET",
                    async: false,
                    success: function (data) {
                        console.log(data);
                        var strVar = "";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"\">Select <\/option>";
                            for (var i = 0; i < data.length; i++) {
                                strVar += " <option value=\"" + data[i].id + "\" >"
                                    + data[i].name
                                    + "<\/option>";
                            }
                        }
                        $('#municipalityId').html(strVar);
                    }
                });
            }
        } else {
            $('#blockMuniId').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#blockMuniId').on('change', function () {
        var blockMuniId = this.value;
        var status = $('#sdoId option:selected').val();
        if (blockMuniId == '1') {
            $('#blockId').parent('div').show();
            $("#municipalityId").parent('div').hide();
            $('#blockId').html("");
            $.ajax({
                url: "./getAllBdoDataBySDO?sdoId=" + status,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].id + "\" >"
                                + data[i].name
                                + "<\/option>";
                        }
                    }
                    $('#blockId').html(strVar);
                }
            });
        } else if (blockMuniId == '2') {
            $('#blockId').parent('div').hide();
            $("#municipalityId").parent('div').show();
            $('#municipalityId').html("");
            console.log(status);
            $.ajax({
                url: "./getAllMunicipalityDataBySDO?sdoId=" + status,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].id + "\" >"
                                + data[i].name
                                + "<\/option>";
                        }
                    }
                    $('#municipalityId').html(strVar);
                }
            });
            $('#panchayetId').html("<option value=\"\">Select <\/option>");
        } else {
            $('#blockId').html("<option value=\"\">Select <\/option>");
            $('#municipalityId').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#blockId').on('change', function () {
        if ($('#blockId').val() != 'Select') {
            if ($('#blockMuniId').val() == 1) {
                $('#panchayetId').html("");
                console.log($('#blockId :selected').val());
                $.ajax({
                    url: "./getAllPanchayetByBlock?bdoId=" + $('#blockId :selected').val(),
                    type: "GET",
                    async: false,
                    success: function (data) {
                        console.log(data)

                        var strVar = "";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"\">Select <\/option>";
                            for (var i = 0; i < data.length; i++) {
                                strVar += " <option value=\"" + data[i].id + "\" >"
                                    + data[i].name
                                    + "<\/option>";
                            }
                        }
                        $('#panchayetId').html(strVar);
                    }
                });
            } else {
                $('#panchayetId').html("<option value=\"\">Select <\/option>");
            }
        }
    });

    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getAllLibrary",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {data: "libraryCode"},
                {data: "libraryName"},
                {data: "email"},
                {data: "contactNumber"}
            ]
        });
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</html>