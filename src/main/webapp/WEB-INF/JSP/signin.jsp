<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <link rel="icon" href="${pageContext.request.contextPath}/resources/Images/icon.png" type="image/png" sizes="16x16">
        <title>Sign In</title>
        <style>
            #loader {
                transition: all .3s ease-in-out;
                opacity: 1;
                visibility: visible;
                position: fixed;
                height: 100vh;
                width: 100%;
                background: #fff;
                z-index: 90000
            }

            #loader.fadeOut {
                opacity: 0;
                visibility: hidden
            }

            .spinner {
                width: 40px;
                height: 40px;
                position: absolute;
                top: calc(50% - 20px);
                left: calc(50% - 20px);
                background-color: #333;
                border-radius: 100%;
                -webkit-animation: sk-scaleout 1s infinite ease-in-out;
                animation: sk-scaleout 1s infinite ease-in-out
            }

            @-webkit-keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    opacity: 0
                }
            }

            @keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0);
                    transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    transform: scale(1);
                    opacity: 0
                }
            }</style>
        <link href="${pageContext.request.contextPath}/resources/CSS/style.css" rel="stylesheet">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/SweetAlert/sweetalert.css">
    </head>
    <body class="app">
        <div id="loader">
            <div class="spinner"></div>
        </div>
        <script type="text/javascript">
            window.addEventListener('load', function() {
                const loader = document.getElementById('loader');
                setTimeout(function() {
                    loader.classList.add('fadeOut');
                }, 300);
            });</script>
        <%String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
        %>
        <div class="peers ai-s fxw-nw h-100vh">
            <div class="peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv"
                 style="background-image:url(${pageContext.request.contextPath}/resources/Images/Library.JPG)">
                <div class="pos-a centerXY">
                    <%-- <div class="bgc-white bdrs-50p pos-r" style="width:120px;height:120px"><img class="pos-a centerXY"
                                                                                                src="${pageContext.request.contextPath}/Images/logo.png"
                                                                                                alt="logo"></div> --%>
                </div>
            </div>
            <div class="col-12 col-md-4 peer pX-40 pY-40 h-100 bgc-white scrollable pos-r" style="min-width:320px">

                <div class="col-md-12" style="width: 100%; height: 120px">
                    <img class="pos-a centerXY" src="${pageContext.request.contextPath}/resources/Images/logo_1_1.png" alt="">
                </div>

                <h4 class="fw-300 c-grey-900 mB-40 mT-40">Login</h4>
                    <form action="signin" method="post"  novalidate="" id="needs-validation" name="loginForm">
                        <input type="hidden" id="status" value="${status}">
                        <div class="form-group"><label class="text-normal text-dark">Email or Username or Phone No</label>
                            <input type="text" class="form-control" id = "firstField" name = "firstField" required placeholder="Enter Email or Username or Phone No" value="">
                            <%--<div class="invalid-feedback">Please provide the registered Email Id.</div>--%>
                        </div>

                        <div class="form-group"><label class="text-normal text-dark">Password</label>
                            <input type="password" class="form-control" placeholder="Password" id = "password" name = "password" required="" value="">
                            <div class="invalid-feedback">Please provide the Password.</div>
                        </div>
                        <div class="form-group">
                            <div class="peers ai-c jc-sb fxw-nw">
                                <%--<div class="peer">
                                    <div class="checkbox checkbox-circle checkbox-info peers ai-c"><input type="checkbox"
                                                                                                          id="rememberMe"
                                                                                                          name="rememberMe"
                                                                                                          class="peer"><label
                                                                                                          for="rememberMe" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Remember Me</span></label>
                                    </div>
                                </div>--%>
                                <div class="peer">
                                    <button type="submit" class="btn btn-primary">Login</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="peers ai-c jc-sb fxw-nw">
                                <div class="peer">
                                    <label class="text-normal text-dark">New User....  <a href="<%--<%=path%>--%>signup">signup</a></label>
                                </div>
                                <div class="peer">
                                    <label class="text-normal text-dark"><a href="forgetpassword">Forget Password....</a></label>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
        <footer class="bdT ta-c p-30 fsz-sm c-grey-600">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
            <span>Copyright ? 2017 Designed by
                <a href="http://dos-infotech.com/" target="_blank" title="Dos_Infotech">Dos Infotech</a>. All rights reserved.</span>
                </div>
                <div class="col-md-3"></div>
            </div>
        </footer>
        <script type="text/javascript">
            !function () {
                "use strict";
                window.addEventListener("load", function () {
                    var t = document.getElementById("needs-validation");
                    t.addEventListener("submit", function (e) {
                        !1 === t.checkValidity() && (e.preventDefault(), e.stopPropagation()), t.classList.add("was-validated")
                    }, !1);
                }, !1);
            }();
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/cookie.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/vendor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/bundle.js"></script>
        <script type="text/javascript">
            /* Back Button Disable */
            function reset() {
                history.pushState({}, "", "");
            }
            var time = 0;
            function doStuff() {

            }
            reset();
            setTimeout(function () {
                window.onpopstate = function (event) {
                    doStuff();
                    reset();
                };
            }, 1);

            /* Sweet Alert Enable */
            $(document).ready(function () {
                var url = $(location).attr("href");
                var arr = url.split("/");
//                console.log(arr)
                if ([arr.length - 1] !== "Admin") {
                    if ($("#status").val() !== '') {
                        if ($("#status").val() === "Signup") {
                            swal("Success", "you should wait for approval.Thank you.", "success");
                        } else if ($("#status").val() === "Signin") {
                            swal("Sorry", "you should wait for approval.Thank you.", "warning");
                        }
                        else {
                            swal("Oops!",$("#status").val(), "error");
                        }
                    }
                }else {
                    swal("Oops!",$("#status").val(), "error");
                }
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                var remember = $.cookie('rememberMe');
                if (remember === 'true') {
                    var email = $.cookie('email');
                    var password = $.cookie('password');
                    $('#email').attr("value", email);
                    $('#password').attr("value", password);
                    $('#rememberMe').attr('checked', true);
                }
                $('#needs-validation').submit(function (event) {
                    if ($('#rememberMe').is(":checked")) {
                        $.cookie('email', $('#email').val());
                        $.cookie('password', $('#password').val());
                        $.cookie('rememberMe', true);
                    }
                });
            });
        </script>
    </body>
</html>
