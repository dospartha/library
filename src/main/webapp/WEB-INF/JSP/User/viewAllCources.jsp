<%--
  Created by IntelliJ IDEA.
  User: Partha Sarothi Banerjee
  Date: 9/17/2018
  Time: 1:12 PM
  To change this template use File | Settings | File Templates.
--%>
<title>View Book Details</title>
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="bgc-white p-20 bd">
                <h4 class="c-grey-900">Search Courses</h4>
                <div class="mT-30">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="courseType"> Course Type </label>
                                <select name="courseType" id="courseType" class="form-control">
                                    <option value="" selected hidden> --Choose Course Type--</option>
                                    <%
                                        String course[] = {"Academic Course",
                                                "Vocational Course",
                                                "Professinal",
                                                "Technical"};
                                        for (int i = 0; i < 4; i++) {
                                    %>
                                    <option><%= course[i]%>
                                    </option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="education"> Education Qualification </label>
                                <select name="education" id="education" class="form-control">
                                    <option value="" selected hidden> --Choose Education Qualification--</option>
                                    <%
                                        String education[] = {"After PostGraduate",
                                                "After Graduate",
                                                "After Higher Secondary"};
                                        for (int i = 0; i < 3; i++) {
                                    %>
                                    <option><%= education[i]%>
                                    </option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="stream"> Select Stream </label>
                                <select name="stream" id="stream" class="form-control">
                                    <option value="" selected hidden> --Choose Stream Type--</option>
                                    <%
                                        String stream[] = {"Science",
                                                "Arts",
                                                "Commerce",
                                                "Medical",
                                                "Engineering"};
                                        for (int i = 0; i < 5; i++) {
                                    %>
                                    <option><%= stream[i]%>
                                    </option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block mT-30" id="searchButton">Search course</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent1">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <div id="searchDiv">
                            <h5 class="c-grey-900 mB-20">List of All Cources</h5>
                        </div>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Course Type</th>
                                <th>Education Qualification</th>
                                <th>Select Stream</th>
                                <th>Library Name</th>
                                <th>URL</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<%--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./viewAllCoursesDetails",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "courseType"},
                {data: "education"},
                {data: "stream"},
                {data: "library.libraryName"},
                {data: "url",
                    render: function (data, type, row, meta) {
                        return '<a href="' + data + '">' + data + '</a>';
                    }
                }
            ]
        });
    });

    $('#searchButton').click(function (e) {
        $('h5').text("List of All cources by search parameter")
        e.preventDefault();
        var values = {
            courseType: $('#courseType').val(),
            education: $('#education').val(),
            stream: $('#stream').val()
        };
        $.ajax({
            url: "./searchCources",
            type: 'POST',
            data: JSON.stringify(values),
            dataType: "json",
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                table.destroy();
                table = $('#dTable').DataTable({
                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                    "lengthMenu": [10, 25, 50, 100],
                    "data": data,
                    columns: [
                        {data: "id", "visible": false},
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                return (meta.row + meta.settings._iDisplayStart + 1);
                            }
                        },
                        {data: "courseType"},
                        {data: "education"},
                        {data: "stream"},
                        {data: "library.libraryName"},
                        {data: "url",
                            render: function (data, type, row, meta) {
                                return '<a href="' + data + '">' + data + '</a>';
                            }
                        }
                    ]
                });
            }

        });
    });

    $('#dTable tbody').on('click', 'a', function (e) {
        $('#dTable tbody tr').css("background-color", "white");
        table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
        e.preventDefault();
        var url = $(this).attr('href');
        window.open(url, '_blank');
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</main>
</html>