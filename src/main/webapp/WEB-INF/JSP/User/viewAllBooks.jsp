<%--<%@ page import="com.example.LibraryProject.Entity.User" %>--%>
<%--
  Created by IntelliJ IDEA.
  User: Partha Sarothi Banerjee
  Date: 9/5/2018
  Time: 3:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" session="false"%>
<%
    HttpSession session = request.getSession(false);
    User user = (User) session.getAttribute("UserSession");
    out.println("User session: " + user.getId());
//    String location = (String) sessionsa.getAttribute("location");
%>--%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>View Book Details</title>
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
                <div id="message" class="" role="" style="display: none">
                    <%--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> Book request is Success.--%>
                </div>
            <div class="bgc-white p-20 bd">
                <h4 class="c-grey-900">Search Books</h4>
                <div class="mT-30">
                    <form>
                        <div class="form-row">
                            <div class="form-group ui-widget col-md-3">
                                <label for="searchWord">  </label>
                                <input type="text" class="form-control" name="searchWord" id="searchWord" style="margin-top: 8px">
                            </div>

                            <%--<div class="form-group col-md-3">
                                <label for="authorName"> Author Name</label>
                                <input type="text" class="form-control" name="authorName" id="authorName"
                                       placeholder="Enter Author Name">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="bookName"> Book Name</label>
                                <input type="text" class="form-control" name="bookName" id="bookName"
                                       placeholder="Enter Book Name">
                            </div>--%>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block mT-30" id="searchButton">Search
                                    Book
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent1">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of All Books</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Publisher Name</th>
                                <th>Subject</th>
                                <th>Book Name</th>
                                <th>Author Name</th>
                                <th>Edition</th>
                                <th>Library Name</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal with Dynamic Content</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="masonry-item col-md-12">
                        <div class="bgc-white p-20 bd">
                            <div class="mT-30">
                                <fieldset disabled="disabled">
                                    <div class="form-row">
                                        <input type="hidden" id="id">
                                        <input type="hidden" id="libraryId">
                                        <div class="form-group col-md-3">
                                            <label for="libraryName"> Library Name</label>
                                            <input type="text" class="form-control" name="libraryName" id="libraryName">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="libraryCode"> Library Code</label>
                                            <input type="text" class="form-control" name="libraryCode" id="libraryCode">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="librarySubDivision"> Library Sub Division</label>
                                            <input type="text" class="form-control" name="librarySubDivision"
                                                   id="librarySubDivision">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="blockMuniDetails"> Library Block/Municipality</label>
                                            <input type="text" class="form-control" name="blockMuniDetails"
                                                   id="blockMuniDetails">
                                        </div>
                                        <div class="form-group col-md-4" style="display: none">
                                            <label for="panchayetDetails">Library Panchayet</label>
                                            <input type="text" class="form-control" name="panchayetDetails"
                                                   id="panchayetDetails">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="bookRequest" data-dismiss="modal">Book
                            Request
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./showAllBooks",
//        }
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "publisherName"},
                {data: "subject.subjectName"},
                {data: "bookName"},
                {data: "authorName"},
                {data: "edition"},
                {data: "library.libraryName"},
//                {defaultContent: "<button id='libraryDetails' class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#myModal\">Library Details</button>"}
                {defaultContent: "<button id='bookRequest' class=\"btn btn-primary\">Book Request</button>"}
            ]
        });
    });
    $('#searchButton').click(function (e) {
        e.preventDefault();
        /*var values = {
            publisherName: $('#publisherName').val(),
            authorName: $('#authorName').val(),
            bookName: $('#bookName').val()
        };
        console.log(values);*/
        $.ajax({
            url: "./searchBooks?search=" + $('#searchWord').val(),
            type: 'POST',
            success: function (data) {
                console.log(data);
                table.destroy();
                table = $('#dTable').DataTable({
                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                    "lengthMenu": [10, 25, 50, 100],
                    "data": data,
                    columns: [
                        {data: "id", "visible": false},
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                return (meta.row + meta.settings._iDisplayStart + 1);
                            }
                        },
                        {data: "publisherName"},
                        {data: "subject.subjectName"},
                        {data: "bookName"},
                        {data: "authorName"},
                        {data: "edition"},
                        {data: "library.libraryName"},
                        {defaultContent: "<button id='bookRequest' class=\"btn btn-primary\">Book Request</button>"}

                    ]
                });
            }

        });
    });

    $('#dTable tbody').on('click', 'button#bookRequest', function () {

        $('#dTable tbody tr').css("background-color", "white");
        table.row($(this).parents('tr').css("background-color", "rgb(202, 207, 210)"));
        var data = table.row($(this).parents('tr')).data();
        console.log(data);
        var values = {
            bookId: data.id,
            libraryId: data.library.id
        };
        $.ajax({
            url: "./requestBooks",
            type: 'POST',
            data: JSON.stringify(values),
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                if (data=="Success"){
                    sweetAlert("Success", "Book request is Success.", "success");
                }else if(data=="Failure"){
                    $('#message').show();
//                    $('#message').delay(5000).fadeOut();
                    $('#message').attr("class", "alert alert-danger alert-dismissible")
                    $('#message').attr("role", "alert")
                    var strVar="";
                    strVar += "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;<\/a>";
                    strVar += "<strong>Oops!<\/strong> Book request is not  Success.";
                    $('#message').html(strVar);
                }else {
                    sweetAlert("Sorry", data, "error");
                    /*$('#message').show();
//                    $('#message').delay(5000).fadeOut();
                    $('#message').attr("class", "alert alert-warning alert-dismissible")
                    $('#message').attr("role", "alert")
                    var strVar="";
                    strVar += "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;<\/a>";
                    strVar += "<strong>Sorry <\/strong>"+ data;
                    $('#message').html(strVar);*/
                }
            }
        });

    });

    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</main>
</html>
