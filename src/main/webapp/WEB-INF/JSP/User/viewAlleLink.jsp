<%--
  Created by IntelliJ IDEA.
  User: Partha Sarothi Banerjee
  Date: 9/8/2018
  Time: 1:13 PM
  To change this template use File | Settings | File Templates.
--%>
<title>View Book Details</title>
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="bgc-white p-20 bd">
                <h4 class="c-grey-900">Search eLink</h4>
                <div class="mT-30">
                    <form>
                        <div class="form-row">
                            <div class="form-group ui-widget col-md-3">
                                <label for="topic"> Topic Name</label>
                                <input type="text" class="form-control" name="topic" id="topic"
                                       placeholder="Enter Topic Name">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block mT-30" id="searchButton">Search eLink</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent1">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <div id="searchDiv">
                        <h5 class="c-grey-900 mB-20">List of All eLinks</h5>
                        </div>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Topic Name</th>
                                <th>Subject</th>
                                <th>Library Name</th>
                                <th>URL</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<%--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./viewAllELearningEntry",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "topic"},
                {data: "subject.subjectName"},
                {data: "library.libraryName"},
                {data: "url",
                    render: function (data, type, row, meta) {
                        return '<a href="' + data + '">' + data + '</a>';
                    }
                }
            ]
        });
    });
    $('#searchButton').click(function (e) {
        $('h5').text("List of All eLinks by search parameter")
        e.preventDefault();
        $.ajax({
            url: "./searcheLink?topic=" + $('#topic').val(),
            type: 'GET',
            success: function (data) {
                console.log(data);
                table.destroy();
                table = $('#dTable').DataTable({
                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                    "lengthMenu": [10, 25, 50, 100],
                    "data": data,
                    columns: [
                        {data: "id", "visible": false},
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                return (meta.row + meta.settings._iDisplayStart + 1);
                            }
                        },
                        {data: "topic"},
                        {data: "subject.subjectName"},
                        {data: "library.libraryName"},
                        {data: "url",
                            render: function (data, type, row, meta) {
                                return '<a href="' + data + '">' + data + '</a>';
                            }
                        }
                    ]
                });
            }

        });
    });

    $('#dTable tbody').on('click', 'a', function (e) {
        $('#dTable tbody tr').css("background-color", "white");
        table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
        e.preventDefault();
        var url = $(this).attr('href');
        window.open(url, '_blank');
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</main>
</html>

