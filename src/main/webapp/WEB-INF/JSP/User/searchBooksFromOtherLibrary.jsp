<%--
  Created by IntelliJ IDEA.
  User: Partha Sarothi Banerjee
  Date: 10/1/2018
  Time: 3:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Search Book Details</title>
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div id="message" class="" role="" style="display: none">
            </div>
            <div class="bgc-white p-20 bd">
                <h4 class="c-grey-900">Search Books</h4>
                <%--<h5 class="text-primary">Please Enter value in any two field of them</h5>--%>
                <div class="mT-30">
                    <form>
                        <div class="form-row">
                            <div class="form-group ui-widget col-md-3">
                                <label for="searchWord">  </label>
                                <input type="text" class="form-control" name="searchWord" id="searchWord" style="margin-top: 8px">
                            </div>
                            <%--<div class="form-group ui-widget col-md-3">
                                <label for="publisherName"> Publisher Name</label>
                                <input type="text" class="form-control" name="publisherName" id="publisherName"
                                       placeholder="Enter Publisher Name">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="authorName"> Author Name</label>
                                <input type="text" class="form-control" name="authorName" id="authorName"
                                       placeholder="Enter Author Name">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="bookName"> Book Name</label>
                                <input type="text" class="form-control" name="bookName" id="bookName"
                                       placeholder="Enter Book Name">
                            </div>--%>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block mT-30" id="searchButton">Search
                                    Book
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent1">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12" id="tableDiv" style="display: none">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of All Search Books</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Publisher Name</th>
                                <th>Subject</th>
                                <th>Book Name</th>
                                <th>Author Name</th>
                                <th>Edition</th>
                                <th>Library Name</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal with Dynamic Content</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="masonry-item col-md-12">
                        <div class="bgc-white p-20 bd">
                            <div class="mT-30">
                                <fieldset disabled="disabled">
                                    <div class="form-row">
                                        <input type="hidden" id="id">
                                        <input type="hidden" id="libraryId">
                                        <div class="form-group col-md-3">
                                            <label for="libraryName"> Library Name</label>
                                            <input type="text" class="form-control" name="libraryName" id="libraryName">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="libraryCode"> Library Code</label>
                                            <input type="text" class="form-control" name="libraryCode" id="libraryCode">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="librarySubDivision"> Library Sub Division</label>
                                            <input type="text" class="form-control" name="librarySubDivision"
                                                   id="librarySubDivision">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="blockMuniDetails"> Library Block/Municipality</label>
                                            <input type="text" class="form-control" name="blockMuniDetails"
                                                   id="blockMuniDetails">
                                        </div>
                                        <div class="form-group col-md-4" style="display: none">
                                            <label for="panchayetDetails">Library Panchayet</label>
                                            <input type="text" class="form-control" name="panchayetDetails"
                                                   id="panchayetDetails">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="bookRequest" data-dismiss="modal">Book
                            Request
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    var table;
    $('#searchButton').click(function (e) {
        e.preventDefault();
        /*var values = {
            publisherName: $('#publisherName').val(),
            authorName: $('#authorName').val(),
            bookName: $('#bookName').val()
        };*/
//        console.log(values);
        $.ajax({
            url: "./searchOtherLibraryBooks?search=" + $('#searchWord').val(),
            type: 'POST',
            /*data: JSON.stringify(values),
            dataType: "json",
            contentType: 'application/json',*/
            success: function (data) {
//                console.log(data);
                $('#tableDiv').show();
                if ( $.fn.DataTable.isDataTable('#dTable') ) {
                    $('#dTable').DataTable().destroy();
                }
                table =  $('#dTable').DataTable({
                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                    "lengthMenu": [10, 25, 50, 100],
                    "data": data,
                    columns: [
                        {data: "id", "visible": false},
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                return (meta.row + meta.settings._iDisplayStart + 1);
                            }
                        },
                        {data: "publisherName"},
                        {data: "subject.subjectName"},
                        {data: "bookName"},
                        {data: "authorName"},
                        {data: "edition"},
                        {data: "library.libraryName"},
                        {defaultContent: "<button id='libraryDetails' class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#myModal\">Library Details</button>"}
                    ]
                });
            }

        });
    });

    $('#dTable tbody').on('click', 'button#libraryDetails', function () {
        $('#dTable tbody tr').css("background-color", "white");
        table.row($(this).parents('tr').css("background-color", "rgb(202, 207, 210)"));
        var data = table.row($(this).parents('tr')).data();
        console.log(data);
        $('#id').val(data.id);
        $('#libraryId').val(data.library.id);
        $('#libraryName').val(data.library.libraryName);
        $('#libraryCode').val(data.library.libraryCode);
        if (data.library.municipalityDetails != null) {
            $('#panchayetDetails').parent('div').hide();
            $('#librarySubDivision').val(data.library.municipalityDetails.subDivisionDetails.name);
            $('#blockMuniDetails').val(data.library.municipalityDetails.name);
        } else {
            $('#panchayetDetails').parent('div').show();
            $('#librarySubDivision').val(data.library.panchayetDetails.blockDetails.subDivisionDetails.name);
            $('#blockMuniDetails').val(data.library.panchayetDetails.blockDetails.name);
            $('#panchayetDetails').val(data.library.panchayetDetails.name);
        }
    });

    $('#bookRequest').on('click', function () {
        var values = {
            bookId: $('#id').val(),
            libraryId: $('#libraryId').val()
        };
//        var url = "./requestBooks?bookId="+$('#id').val()+"&libraryId="+$('#libraryId').val();
        $.ajax({
            url: "./requestBooks",
            type: 'POST',
            data: JSON.stringify(values),
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                if (data=="Success"){
                    $('#message').show();
//                    $('#message').delay(5000).fadeOut();
                    $('#message').attr("class", "alert alert-success alert-dismissible")
                    $('#message').attr("role", "alert")
                    var strVar="";
                    strVar += "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;<\/a>";
                    strVar += "                    <strong>Success!<\/strong> Book request is Success.";
                    $('#message').html(strVar);

                }else if(data=="Failure"){
                    $('#message').show();
                    $('#message').attr("class", "alert alert-danger alert-dismissible")
                    $('#message').attr("role", "alert")
                    var strVar="";
                    strVar += "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;<\/a>";
                    strVar += "                    <strong>Oops!<\/strong> Book request is not  Success.";
                    $('#message').html(strVar);
                }else {
                    $('#message').show();
                    $('#message').attr("class", "alert alert-warning alert-dismissible")
                    $('#message').attr("role", "alert")
                    var strVar="";
                    strVar += "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;<\/a>";
                    strVar += "                    <strong>Sorry <\/strong>"+ data;
                    $('#message').html(strVar);
                }
            }
        });
    });

    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</main>
</html>
