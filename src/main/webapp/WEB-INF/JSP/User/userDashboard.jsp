<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<title>Dashboard</title>
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <c:choose>
                        <c:when test="${fn:length(totalIssuedBooks)==0}">
                            <h4 id="bookNotIssued"> No book issued</h4>
                        </c:when>
                        <c:otherwise>
                            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                                <h4 class="c-grey-900 mB-20">Member issued book details: </h4>
                                <table class="table table-bordered table-hover" id="issuedBookTable">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Sl.No</th>
                                        <th scope="col">Book Name</th>
                                        <th scope="col">Author Name</th>
                                        <th scope="col">Publisher Name</th>
                                        <th scope="col">Issue Date</th>
                                        <th scope="col">Quantity</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${totalIssuedBooks}" var="totalIssuedBooks" varStatus="loop">
                                        <tr>
                                            <th scope="row">${loop.count}</th>
                                            <td>${totalIssuedBooks.book.bookName}</td>
                                            <td>${totalIssuedBooks.book.authorName}</td>
                                            <td>${totalIssuedBooks.book.publisherName}</td>
                                            <td>${totalIssuedBooks.issueDate}</td>
                                            <td>${totalIssuedBooks.quantity}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</main>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script>

    var paramOne =JSON.parse("${totalIssuedBooks}");
    console.log(paramOne.length)
        if (paramOne.length == 0){
            $('#bookNotIssued').show();
        }else {
            $('#issuedBookTable').parent().show();
        }
</script>--%>


