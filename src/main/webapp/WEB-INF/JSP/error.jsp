<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!--        <title>404</title>-->
        <link href="${pageContext.request.contextPath}/CSS/style.css" rel="stylesheet">
    </head>
    <body class="app">
        <%
            int status = response.getStatus();
            System.out.println(status);
        %>
        <title><%=status%></title>
        <div class="pos-a t-0 l-0 bgc-white w-100 h-100 d-f fxd-r fxw-w ai-c jc-c pos-r p-30">
            <div class="mR-60"><img alt="#" src="${pageContext.request.contextPath}/Images/<%=status%>.png"></div>
            <div class="d-f jc-c fxd-c">
                <h1 class="mB-30 fw-900 lh-1 c-red-500" style="font-size:60px"><%=status%></h1>
                <h3 class="mB-10 fsz-lg c-grey-900 tt-c">
                    <%if(status == 404){%>Oops Page Not Found
                    <%}if(status == 500){%>Internal server error<%}%>
                </h3>
                <p class="mB-30 fsz-def c-grey-700">
                    <%if(status == 404){%>The page you are looking for doesnot exist or has been moved.
                    <%}if(status == 500){%>Something goes wrong with our servers, please try again later.<%}%>
                </p>
                <div>
                    <a href="javascript:history.go(-1)" type="primary" class="btn btn-primary"> Back </a>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/JS/vendor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/JS/bundle.js"></script>
    </body>
</html>
