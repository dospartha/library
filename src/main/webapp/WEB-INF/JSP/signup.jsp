<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%--<%String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
//    out.println(path);
    path = path.split("/")[2];
//    out.println(path);
%>--%>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
    <link rel="icon" href="${pageContext.request.contextPath}/resources/Images/icon.png" type="image/png" sizes="16x16">
    <title>Sign Up</title>
    <style>
        #loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }

        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }

        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }

        .radio-inline {
            position: relative;
            display: inline-block;
            padding-left: 20px;
            margin-bottom: 0;
            font-weight: normal;
            vertical-align: middle;
            cursor: pointer;
        }

        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }

        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }
    </style>
    <link href="${pageContext.request.contextPath}/resources/CSS/style.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/SweetAlert/sweetalert.css">
</head>
<body class="app">
<div id="loader">
    <div class="spinner"></div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function () {
        const loader = document.getElementById('loader');
        setTimeout(function () {
            loader.classList.add('fadeOut');
        }, 300);
    });
</script>
<div class="peers ai-s fxw-nw h-100vh">
    <div class="peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv"
         style="background-image: url(${pageContext.request.contextPath}/resources/Images/Library.JPG)">
        <div class="pos-a centerXY">
            <%-- <div class="bgc-white bdrs-50p pos-r"
                    style="width: 120px; height: 120px">
                    <img class="pos-a centerXY" src="${pageContext.request.contextPath}/Images/logo.png"
                            alt="">
            </div> --%>
        </div>
    </div>
    <div class="col-12 col-md-6 peer pX-40 pY-40 h-100 bgc-white scrollable pos-r" style="min-width: 320px">

        <c:if test="${message != null}">
            <div class="alert alert-danger alert-dismissible " role="alert">
                <input type="hidden" id="status" value="${message}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Danger!</strong> <b>File Size</b> is more than Specified Size.
            </div>
        </c:if>

        <div class="col-md-12" style="width: 100%; height: 100px">
            <img class="pos-a centerXY" src="${pageContext.request.contextPath}/resources/Images/logo_1_1.png" alt="">
        </div>

        <h2 class="fw-300 c-grey-900 mB-40 mT-40"><b>Signup</b></h2>
        <form action="signup" method="post" data-parsley-validate="" novalidate="" id="needs-validation"
              name="signupForm" enctype="multipart/form-data">
            <input type="hidden" id="status" value="${status}">
            <h3>Personal Details</h3>
            <div class="form-group row">
                <div class="form-group col-sm-6">
                    <label for="name" class="col-form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" required placeholder="Enter Name">
                    <div class="invalid-feedback">Please provide an name.</div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="username" class="col-form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" required pattern="[a-z]{6,}" placeholder="Enter Username"  minlength="6">
                    <div class="invalid-feedback">Please provide minimum 6 character value</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-sm-6">
                    <label for="email" class="col-form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" required placeholder="Email">
                    <div class="invalid-feedback">Please provide a valid Email</div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="password" class="col-form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password" required pattern="[a-zA-Z0-9]{6,}" placeholder="Password">
                    <div class="invalid-feedback">Please provide minimum 6 length value</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-sm-6">
                    <label for="contactNo" class="col-form-label">Contact No</label>
                    <input type="text" class="form-control" id="contactNo" name="contactNo" required pattern="[0-9]{10}" placeholder="Contact No">
                    <div class="invalid-feedback">Please provide a 10 digit number</div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="dob" class="col-form-label">Date of Birth</label>
                    <input type="date" class="form-control" id="dob" name="dob" value="2000-01-08" placeholder="Date of Birth" required>
                    <div class="invalid-feedback">Please provide DOB</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-sm-6" style="margin-top: 38px;">
                    <label class="col-form-label"><b>Gender</b></label>
                    <div class="radio radio-inline">
                        <input type="radio" id="male" value="male" name="gender" checked>
                        <label for="male">male</label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" id="female" value="female" name="gender">
                        <label for="female">female</label>
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="address" class="col-form-label">Address</label>
                    <textarea name="address" id="address" class="form-control" placeholder="Address" rows="4"></textarea>
                    <div class="invalid-feedback">Please provide Address</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-sm-6">
                    <label for="image">Upload Image</label>
                    <input id="image" type="file" name="imageUser" placeholder="Upload Image"
                           class="form-control">
                    <div class="invalid-feedback">Please provide your image.</div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="adharNo" class="col-form-label">Adhar Card No</label>
                    <input type="text" class="form-control" id="adharNo" name="adharNo" pattern="[0-9]{12}" placeholder="Adhar No" required>
                    <div class="invalid-feedback">Please provide a valid Adhar No</div>
                </div>
            </div>

            <div id="libraryRequest" style="display: none">
                <h3>Get Your Library</h3>
                <div class="form-group row">
                    <div class="form-group col-lg-3">
                        <label>Select Sub-Division</label>
                        <select class="form-control" name="sdoId" id="sdoId">
                            <option value="">Select</option>
                            <c:forEach var="allsdo" items="${allSDO}">
                                <option value="${allsdo.id}">${allsdo.name}</option>
                            </c:forEach>
                        </select>
                        <div class="invalid-feedback">Please provide Sub-Division</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Select Block/Municipality</label>
                        <select class="form-control" id="blockMuniId" name="blockMuniId">
                            <option value="">Select</option>
                            <option value="1">Block</option>
                            <option value="2">Municipality</option>
                        </select>
                        <div class="invalid-feedback">Please provide Block/Municipality</div>
                    </div>
                    <div class="form-group col-lg-5" style="display: none">
                        <label>Select Corresponding Municipality</label>
                        <select class="form-control" id="municipalityId" name="municipalityDetails">
                            <option value="">Select</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Select Corresponding Block</label>
                        <select class="form-control" id="blockId" name="blockDetails">
                            <option value="">Select</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-5" style="display: none">
                        <label>Select Corresponding Panchayet</label>
                        <select class="form-control" id="panchayetId" name="panchayetDetails">
                            <option value="">Select</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Select Library</label>
                        <select class="form-control" name="library" id="library">
                            <option value="">Select</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="peers ai-c jc-sb fxw-nw">
                    <div class="peer">
                        Back to <a href="./signin">Sign in</a>
                    </div>
                    <div class="peer">
                        <button type="submit" class="btn btn-space btn-primary">Submit</button>
                        <%--<button type="reset" class="btn btn-space btn-secondary">Reset</button>--%>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    !function () {
        "use strict";
        window.addEventListener("load", function () {
            var t = document.getElementById("needs-validation");
            t.addEventListener("submit", function (e) {
                !1 === t.checkValidity() && (e.preventDefault(), e.stopPropagation()), t.classList.add("was-validated")
            }, !1)
        }, !1)
    }()
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        /*$('#username').keyup(function() {
//            $("#username").css("background-color", "lavender");
            $.ajax({
                url: "./checkUsernameInput?username=" + $('#username').val(),
                type: "GET",
                async: false,
                success: function (data) {
                    console.log(data)
                    if (data==$('#username').val()){
                        console.log("Error")
//                        $('#usernameCheck').removeClass("invalid-feedback");
                        $('#usernameCheck').html("already exist");
                    }else
                    {
//                        $('#usernameCheck').addClass("invalid-feedback");
                    }
                }
            });
        });*/
        var url = $(location).attr("href");
//        console.log(url);
        var arr = url.split("/");
        console.log(arr);
        if (arr[4] == "signup") {
            $("#libraryRequest").css('display', 'block');
            /*$("#sdoId").attr('required', true);
            $('#blockMuniId').attr('required', true);
            $('#bdomuniId').attr('required', true);
            $('#library').attr('required', true);*/
        }
        if ($("#status").val() !== '') {
            swal("Oops!", $("#status").val(), "error");
        }
    });

    $('#sdoId').change(function () {
        var sdoId = this.value;
//        console.log(sdoId)
        if(sdoId != '') {
            $('#blockMuniId').html("");
            $("#blockMuniId").append("<option value=\"\">Select</option>\n" +
                "  <option value=\"1\">Block</option>\n" +
                "  <option value=\"2\">Municipality</option>")
            if ($('#blockMuniId').val().trim().length!=0) {
                if ($('#blockMuniId').val() == 1) {
                    $('#blockId').parent('div').show();
                    $('#blockId').html("");
                    $.ajax({
                        url: "./getAllBdoDataBySDO?sdoId=" + $('#sdoId').val(),
                        type: "GET",
                        async: false,
                        success: function (data) {
//                            console.log(data)

                            var strVar = "";
                            for (var i = 0; i < data.length; i++) {
                                strVar += " <option value=\"\">Select <\/option>";
                                for (var i = 0; i < data.length; i++) {
                                    strVar += " <option value=\"" + data[i].id + "\" >"
                                        + data[i].name
                                        + "<\/option>";
                                }
                            }
                            $('#blockId').html(strVar);
                        }
                    });
                } else {
                    $('#blockId').parent('div').hide();
                    $("#municipalityId").parent('div').css("display", "block");
                    $('#municipalityId').html("");
                    $.ajax({
                        url: "./getAllMunicipalityDataBySDO?sdoId=" + $('#sdoId').val(),
                        type: "GET",
                        async: false,
                        success: function (data) {
//                            console.log(data);
                            var strVar = "";
                            for (var i = 0; i < data.length; i++) {
                                strVar += " <option value=\"\">Select <\/option>";
                                for (var i = 0; i < data.length; i++) {
                                    strVar += " <option value=\"" + data[i].id + "\" >"
                                        + data[i].name
                                        + "<\/option>";
                                }
                            }
                            $('#municipalityId').html(strVar);
                        }
                    });
                }
            }else {
                $('#blockId').parent('div').show();
                $("#municipalityId").parent('div').hide();
                $('#blockId').html("<option value=\"\">Select <\/option>");
                /*$('#municipalityId').html("<option value=\"\">Select <\/option>");*/
//                $('#panchayetId').html("<option value=\"\">Select <\/option>");
            }
        }else {
            $('#blockId').parent('div').show();
            $('#blockMuniId').html("<option value=\"\">Select <\/option>");
            $('#blockId').html("<option value=\"\">Select <\/option>");
//            $('#panchayetId').html("<option value=\"\">Select <\/option>");
            $('#library').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#blockMuniId').on('change', function () {
        var blockMuniId = this.value;
        var status = $('#sdoId option:selected').val();
        if (blockMuniId == '1') {
            $('#blockId').parent('div').show();
            $("#municipalityId").parent('div').hide();
            $('#blockId').html("");
            $.ajax({
                url: "./getAllBdoDataBySDO?sdoId=" + status,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].id + "\" >"
                                + data[i].name
                                + "<\/option>";
                        }
                    }
                    $('#blockId').html(strVar);
                }
            });
        } else if (blockMuniId == '2') {
            $('#blockId').parent('div').hide();
            $("#municipalityId").parent('div').show();
            $('#municipalityId').html("");
//            console.log(status);
            $.ajax({
                url: "./getAllMunicipalityDataBySDO?sdoId=" + status,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].id + "\" >"
                                + data[i].name
                                + "<\/option>";
                        }
                    }
                    $('#municipalityId').html(strVar);
                }
            });
//            $('#panchayetId').html("<option value=\"\">Select <\/option>");
        } else {
            $('#blockId').html("<option value=\"\">Select <\/option>");
            $('#municipalityId').html("<option value=\"\">Select <\/option>");
//            $('#panchayetId').html("<option value=\"\">Select <\/option>");
            $('#library').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#blockId').on('change', function () {
        var blockId = this.value;
//        console.log("blockId : "+ blockId);
        /*if ($('#blockId').val() != 'Select') {*/
        if (blockId != ''){
            if ($('#blockMuniId').val() == 1) {
                $('#panchayetId').parent('div').show();
                $('#panchayetId').html("");
//                console.log($('#blockId :selected').val());
                $.ajax({
                    url: "./getAllPanchayetByBlock?bdoId=" + $('#blockId :selected').val(),
                    type: "GET",
                    async: false,
                    success: function (data) {
//                        console.log(data)

                        var strVar = "";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"\">Select <\/option>";
                            for (var i = 0; i < data.length; i++) {
                                strVar += " <option value=\"" + data[i].id + "\" >"
                                    + data[i].name
                                    + "<\/option>";
                            }
                        }
                        $('#panchayetId').html(strVar);
                    }
                });
            }
        }else {
            $('#panchayetId').parent('div').hide();
//            $('#panchayetId').html("<option value=\"\">Select <\/option>");
            $('#library').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#panchayetId').change(function () {
        $('#library').html("");
        if ($('#panchayetId').val() != '') {
            $.ajax({
                url: "./getAllLibraryByPanchayet?panchayetId=" + $('#panchayetId :selected').val(),
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].id + "\" >"
                                + data[i].libraryName
                                + "<\/option>";
                        }
                    }
                    $('#library').html(strVar);
                }
            });
        }else {
            $('#library').html("<option value=\"\">Select <\/option>");
        }
    });
    $('#municipalityId').on('change', function () {
        $('#library').html("");
        if ($('#municipalityId').val() != '') {
            $.ajax({
                url: "./getAllLibraryByMunicipalityId?municipalityId=" + $('#municipalityId :selected').val(),
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].id + "\" >"
                                + data[i].libraryName
                                + "<\/option>";
                        }
                    }
                    $('#library').html(strVar);
                }
            });
        }else {
            $('#library').html("<option value=\"\">Select <\/option>");
        }
    });
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/vendor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/bundle.js"></script>
</body>
</html>
