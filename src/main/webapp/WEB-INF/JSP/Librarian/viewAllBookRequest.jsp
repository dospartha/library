<%--
  Created by IntelliJ IDEA.
  User: Partha Sarothi Banerjee
  Date: 9/13/2018
  Time: 6:37 PM
  To change this template use File | Settings | File Templates.
--%>
<title>View All Book Request</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">View All Book Request Details</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">User Id</th>
                                <th style="display: none">Book Id</th>
                                <th>Serial number</th>
                                <th>Book Name</th>
                                <th>Book Author Name</th>
                                <th>Book Publisher Name</th>
                                <th>Book Edition</th>
                                <th>Request date</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="modal fade" id="userModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"> User Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="masonry-item col-md-12">
                                            <div class="bgc-white p-20 bd">
                                                <fieldset disabled="disabled">
                                                    <div class="form-row">
                                                        <input type="hidden" id="bookId">
                                                        <input type="hidden" id="libraryId" name="bookLibraryId">
                                                        <input type="hidden" id="libraryEmail" name="bookLibraryEmail">
                                                        <input type="hidden" id="userLibraryId">
                                                        <div class="form-group col-md-2">
                                                            <label for="userId" class="col-form-label">User Id</label>
                                                            <div><input type="text" class="form-control" id="userId"
                                                                        name="userId"></div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="name" class="col-form-label">Name</label>
                                                            <div><input type="text" class="form-control" id="name"
                                                                        name="name"></div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="email" class="col-form-label">Email</label>
                                                            <div><input type="text" class="form-control" id="email"
                                                                        name="email"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="contactNo" class="col-form-label">Contact
                                                                No</label>
                                                            <div><input type="text" class="form-control" id="contactNo"
                                                                        name="contactNo"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="dob" class="col-form-label">Date of
                                                                Birth</label>
                                                            <div><input type="text" class="form-control" id="dob"
                                                                        name="dob"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="gender" class="col-form-label">Gender</label>
                                                            <div><input type="text" class="form-control" id="gender"
                                                                        name="gender"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="adharNo" class="col-form-label">Adhar Card
                                                                No</label>
                                                            <div><input type="text" class="form-control" id="adharNo"
                                                                        name="adharNo"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="address" class="col-form-label">Address</label>
                                                            <div><input type="text" class="form-control" id="address"
                                                                        name="address"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="userLibraryName" class="col-form-label">User
                                                                Library Name</label>
                                                            <div><input type="text" class="form-control"
                                                                        id="userLibraryName"
                                                                        name="userLibraryName"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="librarySubDivision" class="col-form-label">User
                                                                Library
                                                                Sub-Division</label>
                                                            <div><input type="text" class="form-control"
                                                                        name="librarySubDivision"
                                                                        id="librarySubDivision"></div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="blockMuniDetails" class="col-form-label">
                                                                User Library Block/Municipality</label>
                                                            <input type="text" class="form-control"
                                                                   name="blockMuniDetails"
                                                                   id="blockMuniDetails">
                                                        </div>
                                                        <div class="form-group col-md-3" style="display: none">
                                                            <label for="panchayetDetails" class="col-form-label">User
                                                                Library
                                                                Panchayet</label>
                                                            <input type="text" class="form-control"
                                                                   name="panchayetDetails"
                                                                   id="panchayetDetails">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" id="approveBookRequest"><i
                                                class="c-white-500 fa fa-check"></i> Approve Request
                                        </button>
                                        <button type="button" class="btn btn-danger" id="discardBookRequest"
                                        <%--style="display: none"--%>><i
                                                class="c-white-500 fa fa-trash-o"></i> Discard Request
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./showAllBookRequest",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Scheme', className: 'btn-sm'},
                {extend: 'pdf', title: 'Scheme', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
            columns: [
                {data: "user.id", "visible": false},
                {data: "book.id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                /*{data: "user.name"},*/
                {data: "book.bookName"},
                {data: "book.authorName"},
                {data: "book.publisherName"},
                {data: "book.edition"},
                {data: "requestDate"},
                {defaultContent: "<button id='details' class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\">User Details </button>"}
            ]
        });

        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
//            console.log("data is: " +data)
            $('#bookId').val(data.book.id);
            $('#libraryId').val(data.book.library.id);
            $('#libraryEmail').val(data.book.library.email);
            $('#userId').val(data.user.id);
            $('#userLibraryId').val(data.user.library.id);
            $('#userLibraryName').val(data.user.library.libraryName);
            $('#name').val(data.user.name);
            $('#email').val(data.user.email);
            $('#contactNo').val(data.user.contactNo);
            $('#dob').val(data.user.dob);
            $('#gender').val(data.user.gender);
            $('#adharNo').val(data.user.adharNo);
            $('#address').val(data.user.address);
            if (data.user.library.municipalityDetails != null) {
                $('#panchayetDetails').parent('div').hide();
                $('#librarySubDivision').val(data.user.library.municipalityDetails.subDivisionDetails.name);
                $('#blockMuniDetails').val(data.user.library.municipalityDetails.name);
            } else {
                $('#panchayetDetails').parent('div').show();
                $('#librarySubDivision').val(data.user.library.panchayetDetails.blockDetails.subDivisionDetails.name);
                $('#blockMuniDetails').val(data.user.library.panchayetDetails.blockDetails.name);
                $('#panchayetDetails').val(data.user.library.panchayetDetails.name);
            }
            if (data.requestStatus == true) {
//                console.log("abcd")
                $('#approveBookRequest').css("display", "none");
                $('#discardBookRequest').css("display", "block");
            }
        });

        $('#approveBookRequest').click(function () {
            var values = {
                userId: $("#userId").val(),
                userName: $('#name').val(),
                userEmail: $('#email').val(),
                bookId: $('#bookId').val()
            };
            $.ajax({
                url: "./approveUserBookRequest",
                type: 'POST',
                data: JSON.stringify(values),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        swal({
                            title: "Success",
                            text: "User Book Request is Approved",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeCofirm: true
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: data.responseText,
                            type: "error"
                        }, function () {
                            window.location.reload();
                        });
                    }
                }
            });
        });

        $('#discardBookRequest').click(function () {
            var values = {
                userId: $("#userId").val(),
                userName: $('#name').val(),
                userEmail: $('#email').val(),
                bookId: $('#bookId').val()
            };
            $.ajax({
                url: "./discardUserBookRequest",
                type: 'POST',
                data: JSON.stringify(values),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        swal({
                            title: "Success",
                            text: "User Book Request is Discard",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeCofirm: true
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: data.responseText,
                            type: "error"
                        }, function () {
                            window.location.reload();
                        });
                    }
                }

            });

        });

        $('.fa-chevron-up').click(function () {
            $('.hpanel').show();
        });
    });
</script>
