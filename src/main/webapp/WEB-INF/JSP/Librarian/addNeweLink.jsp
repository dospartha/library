<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>e-Learning</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">

                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> New e-Learning Link is Successfully Added.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add New e-Learning Link</h4>
                    <div class="mT-30">
                        <form method="post" action="./addeLink" name="addeLink">
                            <div class="form-row">
                                <div class="form-group col-md-3" style="display: none">
                                    <label for="library"> Library</label>
                                    <input type="text" class="form-control" name="library" id="library"
                                           value="${library.id}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="subject"> Subject </label>
                                    <select name="subject" id="subject" class="form-control">
                                        <option value="" selected hidden> --Choose Subject--</option>
                                        <c:forEach var="allsubject" items="${allSubject}">
                                            <option value="${allsubject.id}">${allsubject.subjectName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="topic"> Topic of Link</label>
                                    <input type="text" class="form-control" name="topic" id="topic"
                                           placeholder="Topic of Link" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="description"> Description </label>
                                    <input type="text" class="form-control" name="description" id="description"
                                           placeholder="Description" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="url"> Enter url </label>
                                    <input type="text" class="form-control" name="url" id="url" value="https://"
                                           placeholder="Enter url">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block mT-30">Add e-Link</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of e-Links</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Topic of Link</th>
                                <th>Subject</th>
                                <th>Description</th>
                                <th>url</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAlleLinks",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "topic"},
                {data: "subject.subjectName"},
                {data: "description"},
                {data: "url",
                    render: function (data, type, row, meta) {
                        return '<a href="' + data + '">' + data + '</a>';
                    }
                }
            ]
        });
        $('#dTable tbody').on('click', 'a', function (e) {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,255,255)"));
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>