<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Request & Suggestion Box</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">

                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Suggestion Successfully Added.
                    </div>
                </c:if>

                <c:if test="${message != null}">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${message}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> <b>File Size</b> is more than Specified Size.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add Request or Suggestion</h4>
                    <div class="mT-30">
                        <form method="post" action="./addSuggestion" name="addSuggestion" enctype="multipart/form-data">
                            <div class="form-row">
                                <input type="hidden" name="type" value="suggestion">

                                <div class="form-group col-md-4">
                                    <label for="inputSubjectofNotification"> Subject </label>
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputFowardTo"> Send To </label>
                                    <select name="sendTo" id="sendTo" class="form-control">
                                        <option value="" selected hidden> -- Select -- </option>
                                        <%
                                            String person[] = {"Library Members",
                                                "Librarian",
                                                "Admin",
                                                "All"};
                                            for (int i = 0; i < 4; i++) {
                                        %>
                                        <option><%= person[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputAttachedFile"> Attachment File </label>
                                    <input type="file"  class="form-control" name="attachmentFile" id="attachmentFile" placeholder="Attached the File" accept="image/jpeg,image/jpg,application/pdf" value="" style="padding: 3px; padding-left: 8px;">
                                    <input type="hidden" id="fileName" name="fileName">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputDescription"> Write Request of Suggestion </label>
                            <textarea name="description" id="description" class="form-control" placeholder="Write Request of Suggestion" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">

                        </div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </div>
                        <div class="form-group col-md-2">

                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <h4 class="c-grey-900 mT-10 mB-30"></h4>
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h4 class="c-grey-900 mB-20">List of Request/Suggestion</h4>
                    <table id="dTable" class="table table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Subject</th>
                                <th>Request/Suggestion To</th>
                                <th>Date and Time</th>
                                <th>Description</th>
                                <th>Attached File</th>
                                <th>Feedback</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Subject</th>
                                <th>Request/Suggestion To</th>
                                <th>Date and Time</th>
                                <th>Description</th>
                                <th>Attached File</th>
                                <th>Feedback</th>

                            </tr>
                        </tfoot>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="feedbackMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg"  role="document">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">FeedBack</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="id">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputFeedbackSentBy"> Feedback Sent by </label>
                                        <input type="text" class="form-control" name="feedbackByModal" id="feedbackByModal">
                                    </div>
                                    <div class="form-group col-md-4">

                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputFeedbackDate"> Feedback Date </label>
                                        <input type="text" class="form-control" name="feedbackDateModal" id="feedbackDateModal">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="inputDescription"> Write Request of Suggestion </label>
                                        <textarea name="feedbackMessageModal" id="feedbackMessageModal" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-row">
                                    <div class="">
                                        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>

<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllSuggestions",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],

            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "subject"},
                {data: "sendTo"},
                {data: "entryDate",
                    render: function (data, type, row, meta) {
                        var dateTime = dateTimeFormat(data);
                        return dateTime;
                    }
                },
                {data: "description"},
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '"><button type="button" class="btn btn-info">' + 'view' + '</button></a>';
                    }
                },
//                {data: "attachment",
//                    render: function (data, type, row, meta) {
//                        return "<button id='feedbackButton' class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#feedbackMessage\"> Details </button>";
//                    }
//                }
                
                {defaultContent: "<button id='feedbackButton' class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#feedbackMessage\"> Details </button>"}

            ]
        });
        $('#dTable tbody').on('click', 'a', function (e) {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#dTable tbody').on('click', 'button#feedbackButton', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();

            $('#id').val(data.id);
            $('#feedbackByModal').val(data.sendTo);
            $('#feedbackMessageModal').val(data.description);
            var feedbackDate = dateTimeFormat(data.updateDate);
            $('#feedbackDateModal').val(feedbackDate);
        });
        function dateTimeFormat(data) {
            var month = ["January", "February", "March", "April",
                "May", "June", "July", "August",
                "September", "October", "November", "December"];
            var date = data.substr(0, 10);
            var time = data.substr(11, 8);

            var dd = date.substr(8, 2);
            var M = date.substr(5, 2);
            var yyyy = date.substr(0, 4);
            var hh = time.substr(0, 2);
            var mm = time.substr(3, 2);
            var ss = time.substr(6, 2);

            dd = parseInt(dd, 10);
            M = parseInt(M, 10);
            yyyy = parseInt(yyyy, 10);
            hh = parseInt(hh, 10);
            mm = parseInt(mm, 10);
            ss = parseInt(ss, 10);

            var hm = 0;
            mm = mm + 30;
            if (mm > 60) {
                hm = 1;
                mm = mm - 60;
            }
            hh = hh + 5 + hm;
            var timespan = 'AM';
            if (hh >= 12) {
                if (hh > 12) {
                    hh = hh - 12;
                }
                timespan = 'PM';
            }

            var dateTime = n(dd) + "/" + month[M - 1] + "/" + yyyy + " "+" " + n(hh) + ":" + n(mm) + ":" + n(ss) + timespan;
            return dateTime;
        }
        ;
        function n(n) {
            return n > 9 ? "" + n : "0" + n;
        }
        ;
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });


</script>