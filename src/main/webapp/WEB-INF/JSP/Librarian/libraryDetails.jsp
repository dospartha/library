<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Library Details</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                
                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Library Details is Successfully Uploaded.
                    </div>
                </c:if>

                <c:if test="${message != null}">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${message}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> <b>File Size</b> is too lage.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add Library Details</h4>
                    <div class="row">
                        <div class="col-md-8 mT-40">
                            <form method="post" action="./addLibraryDetails" name="addLibraryDetails" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputLibraryCode"> Library Code </label>
                                        <input type="text" class="form-control" name="libraryCode" id="libraryCode" placeholder="Library Code" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputLibraryName"> Name of the Library </label>
                                        <input type="text" class="form-control" name="libraryName" id="libraryName" placeholder="Name of the Library" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputLibrarianName"> Name of the Librarian </label>
                                        <input type="text" class="form-control" name="librarianName" id="librarianName" placeholder="Name of the Librarian" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputAttachedFile"> Upload Library Image </label>
                                        <input type="file"  class="form-control" name="attachmentFile" id="attachmentFile" placeholder="Upload Library Image" accept="image/jpeg,image/jpg" value="" style="padding: 3px; padding-left: 8px;" required>
                                    </div>
                                </div>
                                <h5 class="mT-20">Address</h5>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputVillage"> Street/Road/Avenue No </label>
                                        <input type="text" class="form-control" name="streetNo" id="streetNo" placeholder="Street/Road/Avenue No">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputVillage"> Village/Town Name </label>
                                        <input type="text" class="form-control" name="village" id="village" placeholder="Village/Town/Street No" required>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputMunicipality"> Municipality/Block Name </label>
                                        <input type="text" class="form-control" name="municipality" id="municipality" placeholder="Municipality/Block Name" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="inputPostOffice"> Post Office </label>
                                        <input type="text" class="form-control" name="postOffice" id="postOffice" placeholder="Post Office" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="inputPoliceStation"> Police Station </label>
                                        <input type="text" class="form-control" name="policeStation" id="policeStation" placeholder="Police Station" required>
                                    </div>                                   
                                    <div class="form-group col-md-3">
                                        <label for="inputSubDivision"> Sub-Division Name </label>
                                        <input type="text" class="form-control" name="subDivisionName" id="subDivisionName" placeholder="Sub-Division Name" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="inputDistrict"> District Name </label>
                                        <input type="text" class="form-control" name="district" id="district" placeholder="District Name" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="inputState"> State Name </label>
                                        <input type="text" class="form-control" name="stateName" id="state" placeholder="State Name" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="inputPinCode"> Pin Code Number </label>
                                        <input type="text" class="form-control" name="pinCode" id="pinCode" placeholder="Pin Code Number" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="inputLatitude"> Latitude </label>
                                        <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Latitude" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="inputLongitude"> Longitude </label>
                                        <input type="text" class="form-control" name="longitude" id="longitude" placeholder="Longitude" required>
                                    </div>
                                </div>
                                <h5 class="mT-20">Other Details</h5>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPermanentStuff"> Total Number of Permanent Stuff </label>
                                        <input type="text" class="form-control" name="totalPermanentStuff" id="totalPermanentStuff" placeholder="Total Number of Permanent Stuff" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputCasualStuff"> Total Number of Casual Stuff </label>
                                        <input type="text" class="form-control" name="totalCasualStuff" id="totalCasualStuff" placeholder="Total Number of Casual Stuff" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputTotalBooks"> Total Number of Books </label>
                                        <input type="text" class="form-control" name="totalBooks" id="totalBooks" placeholder="Total Number of Books" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputLibraryArea"> Total Area of Library </label>
                                        <input type="text" class="form-control" name="totalLibraryArea" id="totalLibraryArea" placeholder="Total Area of Library" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputCourseOffered"> Course Offered </label>
                                        <textarea name="courseOffered" id="courseOffered" class="form-control" placeholder="Course Offered" rows="2" required></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputOtherFacilities"> Other Facilities </label>
                                        <textarea name="otherFacilities" id="otherFacilities" class="form-control" placeholder="Other Facilities" rows="2" required></textarea>
                                    </div>
                                </div>
                                <h5 class="mT-20">Contacts</h5>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputLibraryCode"> Contact Number </label>
                                        <textarea name="contactNumber" id="contactNumber" class="form-control" placeholder="Contact Number" rows="2" required></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputLibraryName"> Email Id </label>
                                        <textarea name="email" id="email" class="form-control" placeholder="Email Id " rows="2" required></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">

                                    </div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                    </div>
                                    <div class="form-group col-md-2">

                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <div class="mT-5">
                                <div id="libraryImage" style="height: 250px;border: 8px solid #ddd;">
                                    <img src="${pageContext.request.contextPath}/resources/Images/noImage.png" style="padding:1%; height: 100%; width: 100%;">
                                </div>
                                <div id="libraryLocation" class="mT-10" style="height: 700px;border: 8px solid #ddd;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var mapProp = {
            center: new google.maps.LatLng(22.567280, 88.347352),
            zoom: 7
        };
        var map = new google.maps.Map(document.getElementById("libraryLocation"), mapProp);
    });
    $('#pinCode').focusout(function () {
        var image_holder = $("#libraryLocation");
        image_holder.empty();
        GetLocation();
    });
    $("#attachmentFile").on('change', function () {
        if (typeof (FileReader) !== "undefined") {
            var image_holder = $("#libraryImage");
            image_holder.empty();
            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image img-responsive",
                    "style": "padding:1%; height: 100%; width: 100%;"
                }).appendTo(image_holder);
            };
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
<script type="text/javascript">
    function GetLocation() {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById("pinCode").value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                var mapProp = {
                    center: new google.maps.LatLng(latitude, longitude),
                    zoom: 16
                };
                var map = new google.maps.Map(document.getElementById("libraryLocation"), mapProp);
                var myLatLng = {lat: latitude, lng: longitude};
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    draggable: true,
                    title: 'Select Location'
                });
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;
                //Add listener
                google.maps.event.addListener(marker, "mouseout", function (event) {
                    latitude = this.position.lat();
                    longitude = this.position.lng();
                    document.getElementById("latitude").value = latitude;
                    document.getElementById("longitude").value = longitude;

                });
            } else {
                alert("Request failed.");
            }
        });
    }
    ;
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCR9f3iCzVdgEHp3VjxAcVNXT7t2hiVS8s"></script>
<script type="text/javascript">
    $('#libraryCode').focusout(function () {
        var libraryCode = document.getElementById("libraryCode").value;
        console.log(libraryCode);
        $.ajax({
            url: "./getLibraryTotalBooks/"+libraryCode,
            type: 'GET',
            success: function (data) {
                console.log(data);
                document.getElementById("totalBooks").value = data;
            }
        });
    });
</script>