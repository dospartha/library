<title>View All Courses</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Search Various Course</h4>
                    <div class="mT-30">
                        <form name="searchCourse" id="searchCourse">
                            <!-- for alert -->
                            <input type="hidden" id="status" value="${status}">

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="courseType">Course Type</label>
                                    <select  name="courseType" id="courseType" class="form-control">
                                        <option value="" selected hidden> --Choose Course Type-- </option>
                                        <%String course[] = {"Academic Course",
                                                "Vocational Course",
                                                "Professinal",
                                                "Technical"};
                                            for (int i = 0; i < 4; i++) {
                                        %>
                                        <option><%= course[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="education"> Education Qualification </label>
                                    <select  name="education" id="education" class="form-control">
                                        <option value="" selected hidden> --Choose Education Qualification-- </option>
                                        <%String education[] = {"After PostGraduate",
                                                "After Graduate",
                                                "After Higher Secondary"};
                                            for (int i = 0; i < 3; i++) {
                                        %>
                                        <option><%= education[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="stream"> Select Stream </label>
                                    <select  name="stream" id="stream" class="form-control">
                                        <option value="" selected hidden> --Choose Stream Type-- </option>
                                        <%String stream[] = {"Science",
                                                "Arts",
                                                "Comarce",
                                                "Medical",
                                                "Engineering"};
                                            for (int i = 0; i < 5; i++) {
                                        %>
                                        <option><%= stream[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="addedBy"> Added by </label>
                                    <select  name="addedBy" id="addedBy" class="form-control">
                                        <option value="" selected hidden> --Select-- </option>
                                        <%String addedBy[] = {"Librarian",
                                                "Admin",};
                                            for (int i = 0; i < 2; i++) {
                                        %>
                                        <option><%= addedBy[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="button" class="btn btn-primary btn-block mT-30" id="search"><i class="c-white-500 fa fa fa-search" aria-hidden="true"></i>&nbsp&nbsp&nbsp&nbsp&nbspSearch</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Notice</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Course Type</th>
                                    <th>Education Qualification</th>
                                    <th>Select Stream</th>
                                    <th>Description</th>
                                    <th>Attached File</th>
                                    <th>Course url</th>
                                    <th>Details</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">View Notifications Details</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="masonry-item col-md-12">
                                        <div class="bgc-white p-20 bd">
                                            <div class="mT-30">
                                                <div class="form-row">
                                                    <input type="hidden" id="id">
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCourseType">Course Type</label>
                                                        <input type="text" class="form-control" name="courseType" id="courseTypeModal">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputeducation">Education Qualification</label>
                                                        <input type="text" class="form-control" name="education" id="educationModal">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputStream">Stream</label>
                                                        <input type="text" class="form-control" name="stream" id="streamModal">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputAddedBy">Added by</label>
                                                        <input type="text" class="form-control" name="addedBy" id="addedByModal">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputDescription">Description of Notification</label>
                                                        <textarea name="description" id="descriptionModal" class="form-control" rows="10"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputAttachment">Attachment</label>
                                                        <div style="padding: 8px;border: 1px solid rgba(0, 0, 0, 0.22)!important;border-radius: 5px;">
                                                            <a id="attachmentModal" href=""><span id="attachmentFileNameModal" value=""></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputUrl">url</label>
                                                        <div style="padding: 8px;border: 1px solid rgba(0, 0, 0, 0.22)!important;border-radius: 5px;">
                                                            <a id="urlModal" href=""><span id="urlNameModal" value=""></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="inputFowardTo"> Forward To</label>
                                                        <select name="forwardTo" id="forwardToModal" class="form-control">

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" id="update" data-dismiss="modal"><i class="c-white-500 fa fa-mail-forward"></i> Forward </button>
                                    <!--<button type="button" class="btn btn-primary" id="forwardToAdmin"> Forward to Admin </button>-->
                                    <!--<button type="button" class="btn btn-success" id="sendToUser"> Send to User </button>-->
                                    <button type="button" class="btn btn-danger" id="delete" data-dismiss="modal"><i class="c-white-500 fa fa-trash-o"></i> Delete </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery_Toastr/toastr.min.js"></script>

<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getAllCourse",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
//            "scrollY": 300,
//            "scrollX": true,
            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "courseType"},
                {data: "education"},
                {data: "stream"},
                {data: "description"},
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '">' + data.substr(data.indexOf("r") + 2) + '</a>';
                    }
                },
                {data: "url",
                    render: function (data, type, row, meta) {
                        return '<a href="' + data + '"><button type="button" class="btn btn-info">' + 'view' + '  <i class=\"c-white-500 fa fa-external-link\"></i></button></a>';
                    }
                },
                {defaultContent: "<button id='details' class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"><i class=\"c-white-500 fa fa-info-circle\"></i> Details </button>"}
            ]

        });
        $('#search').click(function () {
            $('#dTable').dataTable().fnDestroy();
            table = $('#dTable').DataTable({
                "ajax": {
                    "url": "./searchCourse",
                    "type": "post",
                    "data": function () {
                        return $('#searchCourse').serialize();
                    }
                },
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [10, 25, 50, 100],
                "scrollX": true,
                "autoWidth": false,
                columns: [

                    {data: "id", "visible": false},
                    {data: null,
                        render: function (data, type, row, meta) {
                            return (meta.row + meta.settings._iDisplayStart + 1);
                        }
                    },
                    {data: "courseType"},
                    {data: "education"},
                    {data: "stream"},
                    {data: "description"},
                    {data: "attachment",
                        render: function (data, type, row, meta) {
                            return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '">' + data.substr(data.indexOf("r") + 2) + '</a>';
                        }
                    },
                    {data: "url",
                        render: function (data, type, row, meta) {
                            return '<a href="' + data + '"><button type="button" class="btn btn-info">' + 'view' + '  <i class=\"c-white-500 fa fa-external-link\"></i></button></a>';
                        }
                    },
                    {defaultContent: "<button id='details' class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"><i class=\"c-white-500 fa fa-info-circle\"></i> Details </button>"}
                ]
            });
        });


        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();

            $('#id').val(data.id);
            $('#courseTypeModal').val(data.courseType);
            $('#educationModal').val(data.education);
            $('#streamModal').val(data.stream);
            $('#addedByModal').val(data.addedBy);
            $('#descriptionModal').val(data.description);
            var path = "${pageContext.request.contextPath}/resources" + (data.attachment);
            $('#attachmentModal').attr('href', path);
            var attachmentFileName = (data.attachment).substr((data.attachment).indexOf("r") + 2);
            $('#attachmentFileNameModal').html(attachmentFileName);
            $('#urlModal').attr('href', (data.url));
            $('#urlNameModal').html(data.url);

            var arr = new Array();
            arr.push("Admin");
            arr.push("Library Members");
            arr.push("Both");
            var strVar = "";
            strVar += "<option value=\"\" selected hidden> -- Selected -- </option>";
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] === data.forwardTo) {
                    strVar += "<option selected value=\"" + arr[i] + "\">" + arr[i] + "<\/option>";
                } else {
                    strVar += "<option value=\"" + arr[i] + "\">" + arr[i] + "<\/option>";
                }
            }
            $('#forwardToModal').html("");
            $('#forwardToModal').html(strVar);

        });
        $('#dTable tbody').on('click', 'a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#attachmentModal').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#urlModal').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
        $('#updateBookIssueId').click(function () {
            var job = {};
            job["id"] = $("#id").val();
            job["forwardTo"] = $('#forwardToModal').val();
            console.log(job);
            $.ajax({
                url: "./courseUpdate",
                type: 'POST',
                data: JSON.stringify(job),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        $(document).ready(function () {
                            table.ajax.reload(null, false);
                            toastr["success"]("Course is forwarded successfully.");
                        });
                    } else {
                        toastr["info"]("Course is not forwarded.");
                    }
                }
            });
        });
        $('#delete').click(function () {
            var job = {};
            job["id"] = $("#id").val();
            console.log(job);
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this Course?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function () {
                $.ajax({
                    url: "./deleteCourse",
                    type: 'POST',
                    data: JSON.stringify(job),
                    dataType: "json",
                    contentType: 'application/json',
                    complete: function (data) {
                        if (data.responseText === "SUCCESS") {
                            $(document).ready(function () {
                                table.ajax.reload();
                                toastr["error"]("Course deleted successfully.");
                            });
                        } else {
                            toastr["info"]("Course is not deleted.");
                        }
                    }
                });
            });
        });
    });
</script>
