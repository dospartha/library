<%--
  Created by IntelliJ IDEA.
  User: DOS12
  Date: 9/4/2018
  Time: 4:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Entry Book Details</title>
<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Book Entry.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Entry Book Details</h4>
                    <div class="mT-30">
                        <form method="post" action="./entryBooks" name="entryBooks">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="book"> Book Name </label>
                                    <select name="book" id="book" class="form-control" required>
                                        <option value="" selected hidden> --Choose Book--</option>
                                        <c:forEach var="book" items="${AllBooks}">
                                            <option value="${book.id}">${book.bookName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" class="form-control" name="quantity" id="quantity" required
                                           placeholder="Quantity">
                                </div>
                                <%--<div class="form-group col-md-3">
                                    <label for="email">User Email Id when user deposit book</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="Enter user email id">
                                </div>--%>
                                <div class="form-group col-md-1">
                                    <button type="submit" class="btn btn-primary btn-block mT-30">Entry Book</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Books</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Publisher Name</th>
                                <th>Subject</th>
                                <th>Book Name</th>
                                <th>Author Name</th>
                                <th>Quantity</th>
                                <th>Entry Date With Time</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getTotalBooksEntry",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "book.publisherName"},
                {data: "book.subject.subjectName"},
                {data: "book.bookName"},
                {data: "book.authorName"},
                {data: "quantity"},
                {data: "entryDate"}
            ]
        });
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</html>