<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Add New Books</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> New Book is Successfully Added.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add New Books</h4>
                    <div class="mT-30">
                        <form method="post" action="./addBooks" name="addbooks">
                            <div class="form-row">
                                <div class="form-group col-md-3" style="display: none">
                                    <label for="library"> Library</label>
                                    <input type="text" class="form-control" name="library" id="library"
                                           value="${library.id}" <%--readonly--%>>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="subject"> Subject </label>
                                    <select name="subject" id="subject" class="form-control">
                                        <option value="" selected hidden> --Choose Subject--</option>
                                        <c:forEach var="allsubject" items="${allSubject}">
                                            <option value="${allsubject.id}">${allsubject.subjectName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="bookName"> Book Name or Title </label>
                                    <input type="text" class="form-control" name="bookName" id="bookName"
                                           placeholder="Book Name">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="authorName"> Author Name </label>
                                    <input type="text" class="form-control" name="authorName" id="authorName"
                                           placeholder="Author Name">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="publisherName"> Publisher Name </label>
                                    <input type="text" class="form-control" name="publisherName" id="publisherName"
                                           placeholder="Publisher Name">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="edition"> Edition </label>
                                    <input type="text" class="form-control" name="edition" id="edition"
                                           placeholder="Enter Edition with year">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="volumeNo"> Volume Number </label>
                                    <input type="text" class="form-control" name="volumeNo" id="volumeNo"
                                           placeholder="Volume Number">
                                </div>
                                <div class="form-group col-md-1">
                                    <button type="submit" class="btn btn-primary btn-block mT-30">Add Book</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent1">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Books</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Publisher Name</th>
                                <th>Subject</th>
                                <th>Book Name</th>
                                <th>Author Name</th>
                                <th>Edition</th>
                                <th>Volume Number</th>
                            </tr>
                            </thead>
                            <%--<tfoot>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Accession Number</th>
                                <th>Call Number</th>
                                <th>Publisher Name</th>
                                <th>Subject</th>
                                <th>Book Name or Title</th>
                                <th>Author Name</th>
                                <th>Edition</th>
                                <th>Volume Number</th>
                                <th>Total Number of Books</th>
                            </tr>
                            </tfoot>--%>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getAllBooks",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "publisherName"},
                {data: "subject.subjectName"},
                {data: "bookName"},
                {data: "authorName"},
                {data: "edition"},
                {data: "volumeNo"}
            ]
        });
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
</script>
</html>