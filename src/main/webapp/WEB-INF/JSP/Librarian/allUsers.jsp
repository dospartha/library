<title>All Users</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">All Users Details</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th>Serial number</th>
                                <th>Name</th>
                                <th>Email Id</th>
                                <th>Status</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="modal fade" id="userModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"> User Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="masonry-item col-md-12">
                                            <div class="bgc-white p-20 bd">
                                                <fieldset disabled="disabled">
                                                    <div class="form-row">
                                                        <input type="hidden" id="id">
                                                        <input type="hidden" id="libraryId" name="libraryId">
                                                        <input type="hidden" id="libraryEmail" name="libraryEmail">
                                                        <div class="form-group col-md-3">
                                                            <label for="name">Name</label>
                                                            <input type="text" class="form-control" id="name"
                                                                   name="name">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="email">Email</label>
                                                            <input type="text" class="form-control" id="email"
                                                                   name="email">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="email">Contact No</label>
                                                            <div><input type="text" class="form-control" id="contactNo"
                                                                        name="contactNo"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="dob" class="col-form-label">Date of Birth</label>
                                                            <div><input type="text" class="form-control" id="dob" name="dob"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="gender" class="col-form-label">Gender</label>
                                                            <input type="text" class="form-control" id="gender"
                                                                   name="gender">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="adharNo" class="col-form-label">Adhar Card
                                                                No</label>
                                                            <div><input type="text" class="form-control" id="adharNo"
                                                                        name="adharNo"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="address" class="col-form-label">Address</label>
                                                            <div><input type="text" class="form-control" id="address"
                                                                        name="address"></div>
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label for="librarySubDivision" class="col-form-label">Select Sub-Division</label>
                                                            <div><input type="text" class="form-control" name="librarySubDivision"
                                                                   id="librarySubDivision"></div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="blockMuniDetails" class="col-form-label"> Library Block/Municipality</label>
                                                            <input type="text" class="form-control" name="blockMuniDetails"
                                                                   id="blockMuniDetails">
                                                        </div>
                                                        <div class="form-group col-md-3" style="display: none">
                                                            <label for="panchayetDetails" class="col-form-label">Library Panchayet</label>
                                                            <input type="text" class="form-control" name="panchayetDetails"
                                                                   id="panchayetDetails">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" id="approveUser"><i
                                                class="c-white-500 fa fa-check"></i> Approve
                                        </button>
                                        <button type="button" class="btn btn-danger" id="discardUser"
                                                <%--style="display: none"--%>><i
                                                class="c-white-500 fa fa-trash-o"></i> Discard
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/jquery_Toastr/toastr.min.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllUsers",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Scheme', className: 'btn-sm'},
                {extend: 'pdf', title: 'Scheme', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
            columns: [
                {data: "id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "name"},
                {data: "email"},
                {
                    data: "status",
                    render: function (data, type, row) {
                        if (data === 1) {
                            return "<span class=\"badge badge-pill badge-primary lh-0 p-15\"> Approved </span>";
                        }
                        if (data === 0) {
                            return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                        }
                        if (data === -1) {
                            return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                        }
                    }
                },
                {defaultContent: "<button id='details' class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"}
            ]
        });

        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
            console.log(data);
            $('#libraryId').val(data.library.id);
            $('#libraryEmail').val(data.library.email);
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#email').val(data.email);
            $('#contactNo').val(data.contactNo);
            $('#dob').val(data.dob);
            $('#gender').val(data.gender);
            $('#adharNo').val(data.adharNo);
            $('#address').val(data.address);
            if (data.library.municipalityDetails != null) {
                $('#panchayetDetails').parent('div').hide();
                $('#librarySubDivision').val(data.library.municipalityDetails.subDivisionDetails.name);
                $('#blockMuniDetails').val(data.library.municipalityDetails.name);
            } else {
                $('#panchayetDetails').parent('div').show();
                $('#librarySubDivision').val(data.library.panchayetDetails.blockDetails.subDivisionDetails.name);
                $('#blockMuniDetails').val(data.library.panchayetDetails.blockDetails.name);
                $('#panchayetDetails').val(data.library.panchayetDetails.name);
            }
            if (data.status === 1) {
//                console.log("cvcvbbv")
//                $('#approveUser').attr("disabled", "disabled");
                $('#approveUser').css("display", "none");
                $('#discardUser').css("display", "block");
            }else if(data.status === -1){
                $('#approveUser').css("display", "block");
                $('#discardUser').css("display", "none");
            }else {
                $('#approveUser').css("display", "block");
                $('#discardUser').css("display", "block");
            }
        });
    });
    //        $('#search_btn').click(function () {
    //            if ($('#status').val() !== '' && $('#finance_id').val() !== '') {
    //                $('#dTable').dataTable().fnDestroy();
    //                table = $('#dTable').DataTable({
    //                    "ajax": "./ViewUserData?companyStatusId=" + $('#status').val() + "&finance_id=" + $('#finance').val(),
    //                    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
    //                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    //
    //                    columns: [
    //
    //                        {data: "id", "visible": false},
    //                        {data: null,
    //                            render: function (data, type, row, meta) {
    //                                return (meta.row + meta.settings._iDisplayStart + 1);
    //                            }
    //                        },
    //                        {data: "username"},
    //                        {data: "email"},
    //                        {defaultContent: "<button id='details' class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"}
    //                    ]
    //                });
    //            } else {
    //                sweetAlert("Error", "please select date", "error");
    //            }
    //        });


    $('#approveUser').click(function () {
        var values = {
            libraryId:$('#libraryId').val(),
            libraryEmail:$('#libraryEmail').val(),
            id: $("#id").val(),
            name: $('#name').val(),
            email:$('#email').val()
        };
        console.log(values);
        $.ajax({
            url: "./userApproval",
            type: 'POST',
            data: JSON.stringify(values),
            dataType: "json",
            contentType: 'application/json',
            complete: function (data) {
                if (data.responseText === "SUCCESS") {
                    swal({
                        title: "Success",
                        text: "User Application is Approved",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeCofirm: true
                    }, function () {
                        window.location.reload();
                    });
                } else {
                    swal({
                        title: "Error",
                        text: "User Application is Approved. But there are some problem to send the mail.",
                        type: "warning"
                    },function () {
                        window.location.reload();
                    });
                }
            }
        });
    });

    $('#discardUser').click(function () {
        var values = {
            libraryId:$('#libraryId').val(),
            libraryEmail:$('#libraryEmail').val(),
            id: $("#id").val(),
            name: $('#name').val(),
            email:$('#email').val()
        };
        console.log(values);
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to discard this user?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: true,
            confirmButtonText: "Yes, discard it!",
            confirmButtonColor: "#ec6c62"
        }, function () {
            $.ajax({
                url: "./userNonApprove",
                type: 'POST',
                data: JSON.stringify(values),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    console.log(data)
                    if (data.responseText === "SUCCESS") {
                        swal({
                            title: "Rejected",
                            text: " User Application is Rejected",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeCofirm: true
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "User Application is Rejected. But there are some problem to send the mail.",
                            type: "warning"
                        },function () {
                            window.location.reload();
                        });
                    }
                }
            });
//            window.location.reload();
        });
    });
</script>