<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Add New Notification</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                
                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> New Notification is Successfully Added.
                    </div>
                </c:if>
                
                <c:if test="${message != null}">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${message}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> <b>File Size</b> is more than Specified Size.
                    </div>
                </c:if>
                        
                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Add Notification</h4>
                    <div class="mT-30">
                        <form method="post" action="./addNotification" name="addNotification" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-3" style="display: none">
                                    <label for="library"> Library</label>
                                    <input type="text" class="form-control" name="library" id="library"
                                           value="${library}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="subject"> Subject of Notification </label>
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="sendTo"> Send To </label>
                                    <select name="sendTo" id="sendTo" class="form-control">
                                        <option value="" selected hidden> -- Select -- </option>
                                        <%
                                            String person[] = {"Library Members",
                                                "Librarian",
                                                "Admin",
                                                "All"};
                                            for (int i = 0; i < 4; i++) {
                                        %>
                                        <option><%= person[i]%></option>
                                        <%}%>
                                    </select>
                                </div> 
                                <div class="form-group col-md-3">
                                    <label for="date"> Date of Notification </label>
                                    <div class='input-group date' id='date'>
                                        <input type="text" class="form-control" name="notificationDate"  placeholder="Date of Notification"  autocomplete="off">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="time"> Time of Notification </label>
                                    <div class='input-group clockpicker' id='time'>
                                        <input type="text" class="form-control" name="notificationTime" placeholder="Time of Notification" autocomplete="off">
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="description"> Description of Notification </label>
                                    <textarea name="description" id="description" class="form-control" placeholder="Write Notice" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="attachmentFile"> Attachment File </label>
                                    <input type="file"  class="form-control" name="attachmentFile" id="attachmentFile" placeholder="Attached the File" accept="image/jpeg,image/gif,image/png,application/pdf" value="">
                                    <input type="hidden" id="fileName" name="fileName">
                                </div>
                                <div class="form-group col-md-8">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Add Notice</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mainContent1">
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Notice</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Subject</th>
                                    <th>Notification Send To</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Description</th>
                                    <th>Attached File</th>

                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DatePicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/ClockPicker/jquery-clockpicker.min.js"></script>

<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllNotifications",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],

            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "subject"},
                {data: "sendTo"},
                {data: "notificationDate"},
                {data: "notificationTime"},
                {data: "description"},
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '">' + data.substr(data.indexOf("r") + 2) + '</a>';
                    }
                }
                
            ]
        });
        $("#date").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/M/yyyy'
        });
        $('#time').clockpicker({
            // 12 or 24 hour 
            twelvehour: true,
            'default': 'now'
//            autoclose: true
        });
        $('#dTable tbody').on('click', 'a', function (e) {
        $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url, '_blank');
        });
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display','none');
            }, 5000);
        }
    });
</script>