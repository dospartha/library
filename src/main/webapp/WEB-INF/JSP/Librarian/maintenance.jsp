<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<title>Maintenance Grant</title>

<main class="main-content bgc-grey-100">

    <button id="hiddenButton" type="button" data-toggle="modal" data-target="#onload" style="display: none"></button>
    <div class="modal fade" id="onload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg"  role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Instructions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <li>The Librarian must need to download specific format from the "Download Maintenance Grant Format" button.</li>
                    <li>Print-out should be taken of that downloaded Maintenance Grant form (Monthly, Yearly, Audit).</li>
                    <li>The scanned copy of filled form should be uploaded along with the Bill.</li>
                    <li>Both the scanned copy of filled Maintenance Grant form and bill should be attached together and upload in a '.pdf' or or '.jpeg' or '.jpg' format.</li>
                    <li>The Specific amount on the bill should be written in the "Total Cost" field.</li>

                </div>
                <div class="modal-footer">
                    <div class="form-row" style="width: 400px;">
                        <div class="form-group col-md-9">
                            <button class="btn btn-info dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-download"></i>  Download Maintenance Grant Format</button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -107px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a class="dropdown-item" href="${pageContext.request.contextPath}/resources/maintenanceGrantFolder/ExampleFile.pdf" download="Monthly Report.pdf">Monthly Report</a> 
                                <a class="dropdown-item" href="${pageContext.request.contextPath}/resources/maintenanceGrantFolder/ExampleFile.pdf" download="yearly Report.pdf">Yearly Report</a> 
                                <a class="dropdown-item" href="${pageContext.request.contextPath}/resources/maintenanceGrantFolder/ExampleFile.pdf" download="Audit Report.pdf">Audit Report</a>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">

                <c:if test="${status=='SUCCESS'}">
                    <div class="alert alert-success alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${status}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> New Maintenance Grant is Successfully Uploaded.
                    </div>
                </c:if>

                <c:if test="${message != null}">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <input type="hidden" id="status" value="${message}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Danger!</strong> <b>File Size</b> is more than Specified Size.
                    </div>
                </c:if>

                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Maintenance Grant</h4>
                    <div class="mT-30">
                        <form method="post" action="./maintenance" name="applyMaintenance" enctype="multipart/form-data">
                            <div class="form-row">
                                <input type="hidden" name="type" value="report">
                                <div class="form-group col-md-3">
                                    <label for="inputMaintenanceType"> Maintenance Grant Type </label>
                                    <select name="maintenanceType" id="maintenanceType" class="form-control" required>
                                        <option value="" selected disabled hidden>-- Choose Maintenance Grant Type --</option>
                                        <%
                                            String maintenanceType[] = {"Book Purchase",
                                                "Furniture",
                                                "Book binding",
                                                "Contingency"};
                                            for (int i = 0; i < 4; i++) {
                                        %>
                                        <option><%= maintenanceType[i]%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputLibraryCode"> Library Code or Id </label>
                                    <input type="text" class="form-control" name="libraryCode" id="libraryCode" placeholder="Library Code or Id" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputLibraryName"> Name of the Library </label>
                                    <input type="text" class="form-control" name="libraryName" id="libraryName" placeholder="Name of the Library" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputLibrarianName"> Name of the Librarian </label>
                                    <input type="text" class="form-control" name="librarianName" id="librarianName" placeholder="Name of the Librarian" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="inputItemName"> Name of the Item </label>
                                    <input type="text" class="form-control" name="itemName" id="itemName" placeholder="Name of the Item" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputAttachedFile"> Attachment File (Scan Copy of Bill) </label>
                                    <input type="file"  class="form-control" name="attachmentFile" id="attachmentFile" placeholder="Attached the File" accept="image/jpeg,image/jpg,application/pdf" value="" style="padding: 4px; padding-left: 7px;" required>
                                    <input type="hidden" id="fileName" name="fileName">
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="inputItemName"> Total Cost </label>
                                    <input type="text" class="form-control" name="totalCost" id="totalCost" value="&#x20b9 &nbsp" style="padding: 7px;" required>
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block mT-30">Submit</button>
                                </div>
                                <div class="dropdown col-md-3">
                                    <button class="btn btn-info dropdown-toggle btn-block mT-30" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-download"></i>  Download Maintenance Grant Format</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -107px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="${pageContext.request.contextPath}/resources/maintenanceGrantFolder/ExampleFile.pdf" download="Monthly Report.pdf">Monthly Report</a> 
                                        <a class="dropdown-item" href="${pageContext.request.contextPath}/resources/maintenanceGrantFolder/ExampleFile.pdf" download="yearly Report.pdf">Yearly Report</a> 
                                        <a class="dropdown-item" href="${pageContext.request.contextPath}/resources/maintenanceGrantFolder/ExampleFile.pdf" download="Audit Report.pdf">Audit Report</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Maintenance Grant</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Maintenance Grant Type</th>
                                    <th>Item Name</th>
                                    <th>Date of Upload</th>
                                    <th>Library Code</th>
                                    <th>Status</th>
                                    <th>Total Cost</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            "ajax": "./getAllMaintenanceGrant",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],

            columns: [

                {data: "id", "visible": false},
                {data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "maintenanceType"},
                {data: "itemName"},
                {data: "entryDate",
                    render: function (data, type, row, meta) {
                        var dateTime = dateTimeFormat(data);
                        return dateTime;
                    }
                },
                {data: "libraryCode"},
                {data: "status",
                    render: function (data, type, row) {
                        if (data === 1) {
                            return "<span class=\"badge badge-pill badge-success lh-0 p-15\"> Approved </span>";
                        }
                        if (data === 0) {
                            return "<span class=\"badge badge-pill badge-warning lh-0 p-15\"> Pending </span>";
                        }
                        if (data === -1) {
                            return "<span class=\"badge badge-pill badge-danger lh-0 p-15\"> Not Approved </span>";
                        }
                    }
                },
                {data: "totalCost",
                    render: function (data, type, row, meta) {
                        return "&#x20b9&nbsp" + data;
                    }
                },
                {data: "attachment",
                    render: function (data, type, row, meta) {
                        return '<a href="' + "${pageContext.request.contextPath}/resources" + data + '"><button type="button" class="btn btn-info">' + 'view' + '  <i class=\"c-white-500 fa fa-external-link\"></i></button></a>';
                    }
                }

            ]
        });
        function dateTimeFormat(data) {
            var month = ["January", "February", "March", "April",
                "May", "June", "July", "August",
                "September", "October", "November", "December"];
            var date = data.substr(0, 10);
            var time = data.substr(11, 8);

            var dd = date.substr(8, 2);
            var M = date.substr(5, 2);
            var yyyy = date.substr(0, 4);
            var hh = time.substr(0, 2);
            var mm = time.substr(3, 2);
            var ss = time.substr(6, 2);

            dd = parseInt(dd, 10);
            M = parseInt(M, 10);
            yyyy = parseInt(yyyy, 10);
            hh = parseInt(hh, 10);
            mm = parseInt(mm, 10);
            ss = parseInt(ss, 10);

            var hm = 0;
            mm = mm + 30;
            if (mm > 60) {
                hm = 1;
                mm = mm - 60;
            }
            hh = hh + 5 + hm;
            var timespan = 'AM';
            if (hh >= 12) {
                if (hh > 12) {
                    hh = hh - 12;
                }
                timespan = 'PM';
            }

            var dateTime = n(dd) + "/" + month[M - 1] + "/" + yyyy + "&nbsp&nbsp" + n(hh) + ":" + n(mm) + ":" + n(ss) + timespan;
            return dateTime;
        }
        ;
        function n(n) {
            return n > 9 ? "" + n : "0" + n;
        }
        ;
        $('#dTable tbody').on('click', 'a', function (e) {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            e.preventDefault();
            var attachment = $(this).attr('href');
            window.open(attachment, '_blank');
        });
    });
    $(document).ready(function () {
        if ($('#status').val() === 'SUCCESS') {
            setTimeout(function () {
                $('.alert-dismissible').css('display', 'none');
            }, 5000);
        }
    });
    $(document).ready(function () {
        $("#hiddenButton").trigger("click");
    });
</script>
