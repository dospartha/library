<title>Update Book Details</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="masonry-item col-md-12">
                <div class="bgc-white p-20 bd">
                    <h4 class="c-grey-900">Search Book</h4>
                    <div class="mT-30">
                        <form name="searchBook" id="searchBook">
                            <!-- for sweet alert -->
                            <input type="hidden" id="status" value="${status}">

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputSubject"> Subject </label>
                                    <select name="subject" id="subject" class="form-control">
                                        <option value="" selected hidden> --Choose Subject-- </option>
                                        <%
                                            String subject[] = {"Bengali literature",
                                                "English literature",
                                                "Mathematics",
                                                "History",
                                                "geography",
                                                "General Science",
                                                "Phiscis",
                                                "Chemistry",
                                                "Biology",
                                                "Phisiology",
                                                "Sociology",
                                                "Hindi Literature",
                                                "Modern History",
                                                "journal",
                                                "Technology",
                                                "Information Technology",
                                                "Computer Science",
                                                "Medical books",
                                                "Magazine",
                                                "General Khowledge",
                                                "Accountancy",
                                                "Political Science",
                                                "Botany",
                                                "Zoology",
                                                "Law",
                                                "Comics",
                                                "Science Fiction",
                                                "Fairy Tales",
                                                "Story Books"};
                                            for (int i = 0; i < 29; i++) {
                                        %>
                                        <option><%= subject[i]%></option>
                                        <%}%>
                                    </select>
                                </div> 
                                <div class="form-group col-md-4">
                                    <label for="inputBookName"> Book Name or Title </label>
                                    <input type="text" class="form-control" name="bookName" id="bookName" placeholder="Book Name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPublisherName"> Publisher Name </label>
                                    <input type="text" class="form-control" name="publisherName" id="publisherName" placeholder="Publisher Name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputAuthorName"> Author Name </label>
                                    <input type="text" class="form-control" name="authorName" id="authorName" placeholder="Author Name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputVolumeNo"> Volume Number </label>
                                    <input type="text" class="form-control" name="volumeNo" id="volumeNo" placeholder="Volume Number">
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="button" class="btn btn-primary btn-block mT-30" id="search"><i class="c-white-500 fa fa fa-search" aria-hidden="true"></i>&nbsp&nbsp&nbsp&nbsp&nbspSearch</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <h4 class="c-grey-900 mT-10 mB-30"></h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">List of Books</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Accession Number</th>
                                    <th>Call Number</th>
                                    <th>Publisher Name</th>
                                    <th>Subject</th>
                                    <th>Book Name or Title</th>
                                    <th>Author Name</th>
                                    <th>Edition</th>
                                    <th>Volume Number</th>
                                    <th>Total Number of Books</th>
                                    <th>Details</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="display: none">Id</th>
                                    <th>Serial number</th>
                                    <th>Accession Number</th>
                                    <th>Call Number</th>
                                    <th>Publisher Name</th>
                                    <th>Subject</th>
                                    <th>Book Name or Title</th>
                                    <th>Author Name</th>
                                    <th>Edition</th>
                                    <th>Volume Number</th>
                                    <th>Total Number of Books</th>
                                    <th>Details</th>

                                </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Update Book Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="masonry-item col-md-12">
                                            <div class="bgc-white p-20 bd">
                                                <div class="mT-30">
                                                    <div class="form-row">
                                                        <input type="hidden" id="id">
                                                        <div class="form-group col-md-4">
                                                            <label for="inputAccNo"> Accession Number </label>
                                                            <input type="text" class="form-control" name="accessionNo" id="accessionNoModal" placeholder="Accession Number">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputCallNo"> Call Number </label>
                                                            <input type="text" class="form-control" name="callNo" id="callNoModal" placeholder="Call Number">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputPublisherName"> Publisher Name </label>
                                                            <input type="text" class="form-control" name="publisherName" id="publisherNameModal" placeholder="Publisher Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label for="inputSubject"> Subject </label>
                                                            <select name="subject" id="subjectModal" class="form-control">
                                                                <option value="" selected hidden>--Choose Subject--</option>
                                                                <%
                                                                    String subject1[] = {"Bengali literature",
                                                                        "English literature",
                                                                        "Mathematics",
                                                                        "History",
                                                                        "geography",
                                                                        "General Science",
                                                                        "Phiscis",
                                                                        "Chemistry",
                                                                        "Biology",
                                                                        "Phisiology",
                                                                        "Sociology",
                                                                        "Hindi Literature",
                                                                        "Modern History",
                                                                        "journal",
                                                                        "Technology",
                                                                        "Information Technology",
                                                                        "Computer Science",
                                                                        "Medical books",
                                                                        "Magazine",
                                                                        "General Khowledge",
                                                                        "Accountancy",
                                                                        "Political Science",
                                                                        "Botany",
                                                                        "Zoology",
                                                                        "Law",
                                                                        "Comics",
                                                                        "Science Fiction",
                                                                        "Fairy Tales",
                                                                        "Story Books"};
                                                                    for (int i = 0; i < 29; i++) {
                                                                %>
                                                                <option><%= subject1[i]%></option>
                                                                <%}%>
                                                            </select>
                                                        </div> 
                                                        <div class="form-group col-md-4">
                                                            <label for="inputBookName"> Book Name or Title </label>
                                                            <input type="text" class="form-control" name="bookName" id="bookNameModal" placeholder="Book Name">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputAuthorName"> Author Name </label>
                                                            <input type="text" class="form-control" name="authorName" id="authorNameModal" placeholder="Author Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label for="inputEdition"> Edition </label>
                                                            <input type="text" class="form-control" name="edition" id="editionModal" placeholder="Edition">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputVolumeNo"> Volume Number </label>
                                                            <input type="text" class="form-control" name="volumeNo" id="volumeNoModal" placeholder="Volume Number">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label for="inputVolumeNo"> Total Number of Books </label>
                                                            <input type="text" class="form-control" name="noOfTotalBook" id="noOfTotalBookModal" placeholder="noOfTotalBook">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" id="updateBookDetails" data-dismiss="modal"><i class="fa fa-edit"></i> Update </button>
                                        <button type="button" class="btn btn-danger" id="delete" data-dismiss="modal"><i class="c-white-500 fa fa-trash-o"></i> Delete </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery_Toastr/toastr.min.js"></script>

<script type="text/javascript">
    var table;
    $(function () {
        table = $('#dTable').DataTable({
            "ajax": "./getAllBooks",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            "scrollX": true,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                var oSettings = this.fnSettings();
                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
                return nRow;
            },
            columns: [

                {data: "id", "visible": false},
                {data: null},
                {data: "accessionNo"},
                {data: "callNo"},
                {data: "publisherName"},
                {data: "subject"},
                {data: "bookName"},
                {data: "authorName"},
                {data: "edition"},
                {data: "volumeNo"},
                {data: "noOfTotalBook"},
                {defaultContent: "<button id='details' class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"}
            ],
            select: true

        });
        $('#search').click(function () {
            $('#dTable').dataTable().fnDestroy();
            table = $('#dTable').DataTable({
                "ajax": {
                    "url": "./searchBook",
                    "type": "post",
                    "data": function () {
                        return $('#searchBook').serialize();
                    }
                },
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [10, 25, 50, 100],
                "ordering": false,
                "scrollX": true,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var oSettings = this.fnSettings();
                    $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
                    return nRow;
                },
                columns: [
                    {data: "id", "visible": false},
                    {data: null},
                    {data: "accessionNo"},
                    {data: "callNo"},
                    {data: "publisherName"},
                    {data: "subject"},
                    {data: "bookName"},
                    {data: "authorName"},
                    {data: "edition"},
                    {data: "volumeNo"},
                    {data: "noOfTotalBook"},
                    {defaultContent: "<button id='details' class=\"btn btn-success\" type=\"button\" data-toggle=\"modal\" data-target=\"#userModal\"> Details </button>"}
                ],
                select: true

            });
        });
        $('#dTable tbody').on('click', 'button#details', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
            $('#id').val(data.id);
            $('#accessionNoModal').val(data.accessionNo);
            $('#callNoModal').val(data.callNo);
            $('#publisherNameModal').val(data.publisherName);
            $('#subjectModal').val(data.subject);
            $('#bookNameModal').val(data.bookName);
            $('#authorNameModal').val(data.authorName);
            $('#editionModal').val(data.edition);
            $('#volumeNoModal').val(data.volumeNo);
            $('#noOfTotalBookModal').val(data.noOfTotalBook);
        });
        $('#updateBookDetails').click(function () {
            var job = {};
            job["id"] = $("#id").val();
            job["accessionNo"] = $('#accessionNoModal').val();
            job["callNo"] = $('#callNoModal').val();
            job["publisherName"] = $('#publisherNameModal').val();
            job["subject"] = $('#subjectModal').val();
            job["bookName"] = $('#bookNameModal').val();
            job["authorName"] = $('#authorNameModal').val();
            job["edition"] = $('#editionModal').val();
            job["volumeNo"] = $('#volumeNoModal').val();
            job["noOfTotalBook"] = $('#noOfTotalBookModal').val();
            console.log(job);
            $.ajax({
                url: "./booksUpdate",
                type: 'POST',
                data: JSON.stringify(job),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        $(document).ready(function () {
                            table.ajax.reload(null, false);
                            toastr["success"]("Book details updated successfully.");
                        });
                    } else {
                        toastr["error"]("Book details is not updated.");
                    }
                }
            });
        });
        $('#delete').click(function () {
            var job = {};
            job["id"] = $("#id").val();
            console.log(job);
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this book?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function () {
                $.ajax({
                    url: "./deleteBooks",
                    type: 'POST',
                    data: JSON.stringify(job),
                    dataType: "json",
                    contentType: 'application/json',
                    complete: function (data) {
                        if (data.responseText === "SUCCESS") {
                            $(document).ready(function () {
                                table.ajax.reload();
                                toastr["success"]("Book deleted successfully.");
                            });
                        } else {
                            toastr["error"]("Book is not deleted.");
                        }
                    }
                });
            });
        });
    });
</script>
</html>