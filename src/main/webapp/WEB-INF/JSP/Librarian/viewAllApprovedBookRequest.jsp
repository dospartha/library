<%--
  Created by IntelliJ IDEA.
  User: Partha Sarothi Banerjee
  Date: 9/29/2018
  Time: 5:59 PM
  To change this template use File | Settings | File Templates.
--%>
<title>All Approved Book Request</title>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">All Approved Book Request</h4>
                        <table id="dTable" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="display: none">Id</th>
                                <th style="display: none">Book Id</th>
                                <th>Serial number</th>
                                <th>User Id</th>
                                <th>Book Name</th>
                                <th>Book Author Name</th>
                                <th>Book Publisher Name</th>
                                <th>Book Edition</th>
                                <th>Request date</th>
                                <th>Approved date</th>
                                <th>Issue Status</th>
                                <%--<th>Issue</th>
                                <th>Deposit</th>--%>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/DataTable/jquery.dataTables.js"></script>
<script>src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"</script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function () {
        var table;
        table = $('#dTable').DataTable({
            autoWidth: false,
            columnDefs: [
                { width: '150px', targets: 10 }
            ],
            "ajax": "./showAllApprovedBookRequest",
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [10, 25, 50, 100],
            columns: [
                {data: "id", "visible": false},
                {data: "book.id", "visible": false},
                {
                    data: null,
                    render: function (data, type, row, meta) {
                        return (meta.row + meta.settings._iDisplayStart + 1);
                    }
                },
                {data: "user.id"},
                {data: "book.bookName"},
                {data: "book.authorName"},
                {data: "book.publisherName"},
                {data: "book.edition"},
                {data: "requestDate"},
                {data: "approveDate"},
                {
                    data: "issueId",
                    render: function (data, type, row) {
                        console.log(row)
                        if (data !== null) {
                            if (row.entryId === null) {
                                return "<span class=\"text-primary font-weight-bold\">Book Issued</span>&nbsp;&nbsp;<button id='bookDeposit' class=\"btn btn-primary\" type=\"button\">Deposit</button>";
                            }else{
                                return "<span class=\"text-primary font-weight-bold\">Book deposited</span>";
                            }
                        }else if (data === null) {
                            return "<span class=\"text-primary font-weight-bold\">Pending</span>&nbsp;&nbsp;<button id='bookIssue' class=\"btn btn-primary\" type=\"button\">Book Issue</button>";
                        }
                    }
                }
//               {defaultContent: "<button id='bookIssue' class=\"btn btn-primary\" type=\"button\">Book Issue</button>"},
//                {defaultContent: "<button id='bookDeposit' class=\"btn btn-primary\" type=\"button\" disabled=\"disabled\">Book Deposit</button>"}
            ]
        });
        $('#dTable tbody').on('click', 'button#bookIssue', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
            console.log(data)
            var values = {
                id: data.id,
                userId: data.user.id,
                userName: data.user.name,
                userEmail: data.user.email,
                bookId: data.book.id
            };
            $.ajax({
                url: "./issueBookToUser",
                type: 'POST',
                data: JSON.stringify(values),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        /*sweetAlert("Success", "Book issued successfully", "success");
                        window.location.reload();*/
                        swal({
                            title: "Success",
                            text: "Book issued successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeCofirm: true
                        }, function () {
                            window.location.reload();
                        });
                    } else if (data.responseText === "Issued") {
                        sweetAlert("Warning", "Book already issued", "warning");
                    } else {
                        sweetAlert("Sorry", data.responseText, "error");
                    }
                }
            });
        });

        $('#dTable tbody').on('click', 'button#bookDeposit', function () {
            $('#dTable tbody tr').css("background-color", "white");
            table.row($(this).parents('tr').css("background-color", "rgb(240,248,255)"));
            var data = table.row($(this).parents('tr')).data();
//            console.log(data)
            var values = {
                user_id: data.user.id,
                book_id: data.book.id
            };
            console.log(values)
            $.ajax({
                url: "./depositBookByUser",
                type: 'POST',
                data: JSON.stringify(values),
                dataType: "json",
                contentType: 'application/json',
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        sweetAlert("Success", "Book deposit successfully", "success");
                        swal({
                            title: "Success",
                            text: "Book deposit successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeCofirm: true
                        }, function () {
                        $('#bookDeposit').prop('disabled', true);
                        });

                    }else {
                        sweetAlert("Sorry", data.responseText, "error");
                    }
                }
            });
        });

        $('.fa-chevron-up').click(function () {
            $('.hpanel').show();
        });
    });
</script>
