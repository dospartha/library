<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <link rel="icon" href="${pageContext.request.contextPath}/resources/Images/icon.png" type="image/png" sizes="16x16">
        <title>Forget Password</title>
        <style>
            #loader {
                transition: all .3s ease-in-out;
                opacity: 1;
                visibility: visible;
                position: fixed;
                height: 100vh;
                width: 100%;
                background: #fff;
                z-index: 90000
            }

            #loader.fadeOut {
                opacity: 0;
                visibility: hidden
            }

            .spinner {
                width: 40px;
                height: 40px;
                position: absolute;
                top: calc(50% - 20px);
                left: calc(50% - 20px);
                background-color: #333;
                border-radius: 100%;
                -webkit-animation: sk-scaleout 1s infinite ease-in-out;
                animation: sk-scaleout 1s infinite ease-in-out
            }

            @-webkit-keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    opacity: 0
                }
            }

            @keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0);
                    transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    transform: scale(1);
                    opacity: 0
                }
            }</style>
        <link href="${pageContext.request.contextPath}/CSS/style.css" rel="stylesheet">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/SweetAlert/sweetalert.css">
    </head>
    <body class="app">
        <div id="loader">
            <div class="spinner"></div>
        </div>
        <script type="text/javascript">
            window.addEventListener('load', function(){
                const loader = document.getElementById('loader');
                setTimeout(function() {
                    loader.classList.add('fadeOut');
                }, 300);
            });
        </script>
        <%--<%String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
            out.println(path);
            path = path.split("/")[2];
            out.println(path);
        %>--%>
        <div class="peers ai-s fxw-nw h-100vh">
            <div class="peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv"
                 style="background-image:url(${pageContext.request.contextPath}/Images/library_1.jpg)">
                <div class="pos-a centerXY">
                </div>
            </div>
            <div class="col-12 col-md-4 peer pX-40 pY-40 h-100 bgc-white scrollable pos-r" style="min-width:320px">

                <div class="col-md-12" style="width: 100%; height: 120px">
                    <img class="pos-a centerXY" src="${pageContext.request.contextPath}/resources/Images/logo_1_1.png" alt="">
                </div>

                <h4 class="fw-300 c-grey-900 mB-40 mT-40">Forget Password</h4>
                <form method="post">
                    <input type="hidden" id="status" value="${status}">
                    <div class="form-group"><label class="text-normal text-dark">Email</label><input type="email"
                                                                                                     class="form-control"
                                                                                                     placeholder="Email"
                                                                                                     name = "email">
                    </div>

                    <div class="form-group">
                        <div class="peers ai-c jc-sb fxw-nw">
                            <div class="peer">
                                Back to <a href="./signin">Sign in</a>
                            </div>
                            <div class="peer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane-o">&nbsp&nbspSend</i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/JS/SweetAlert/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
                if ($("#status").val() === 'SUCCESS') {
                    swal("Sent Successfully", "Email has been sent Successfully.\nCheck your email to get the password.\n", "success");
                }
                if ($("#status").val() === 'FAILURE') {
                    swal("Oops!", "Something went wrong \n Email has not been sent!\n", "error");
                }
            });
        </script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/JS/vendor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/JS/bundle.js"></script>
    </body>
</html>
