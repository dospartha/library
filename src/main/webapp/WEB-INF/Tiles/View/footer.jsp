<footer class="bdT ta-c p-30 fsz-sm c-grey-600">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <span>Copyright � 2017 Designed by 
                <a href="http://dos-infotech.com/" target="_blank" title="Dos_Infotech">Dos Infotech</a>. All rights reserved.</span>
        </div>
        <div class="col-md-3"></div>
    </div>
</footer>
</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/vendor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/JS/bundle.js"></script>
