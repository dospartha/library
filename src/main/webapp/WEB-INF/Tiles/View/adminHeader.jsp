<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<%String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
//    out.println(path);
    path = path.split("/")[2];
//    out.println(path);
%>
<%--<script>
    var base = "${pageContext.request.contextPath}";
    console.log(base);
</script>--%>
<div id="loader">
    <div class="spinner"></div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function(){
        const loader = document.getElementById('loader');
        setTimeout(function() {
            loader.classList.add('fadeOut');
        }, 300);
    });
</script>
<div>
    <div class="sidebar">
        <div class="sidebar-inner">
            <div class="sidebar-logo">
                <div class="peers ai-c fxw-nw">
                    <div class="peer peer-greed">
                        <a class="sidebar-link td-n" href="./dashboard" class="td-n">
                            <div class="peers ai-c fxw-nw">
                                <div class="peer">
                                    <div class="logo">
                                        <img src="${pageContext.request.contextPath}/resources/Images/logo.png" alt="">
                                    </div>
                                </div>
                                <div class="peer peer-greed">
                                    <h5 class="lh-1 mB-0 logo-text">${AdminSession.name}</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="peer">
                        <div class="mobile-toggle sidebar-toggle">
                            <a href="#" class="td-n">
                                <i class="ti-arrow-circle-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="sidebar-menu scrollable pos-r">
                <li class="nav-item mT-30 active">
                    <a class="sidebar-link" href="./dashboard" default>
                        <span class="icon-holder">
                            <i class="c-blue-500 ti-home"></i>
                        </span>
                        <span class="title">Admin Dashboard</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-indigo-500 fa fa-university"></i></span>
                        <span class="title">Library Details</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./addNewLibrary">Add New Library</a></li>
                        <%--<li><a class="sidebar-link" href="./viewAllLibrary">Update Library</a></li>--%>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-orange-500 fa fa-user-o"></i></span>
                        <span class="title">Librarian Details</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./allLibrarian">All Librarian</a></li>
                        <li><a class="sidebar-link" href="./allApprovedLibrarian">All Approved Librarians</a></li>
                        <li><a class="sidebar-link" href="./allNonApprovedLibrarian">All Non-Approved Librarians</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="sidebar-link" href="./allApprovedUsers">
                        <span class="icon-holder"><i class="c-orange-500 fa fa-user-o"></i></span>
                        <span class="title">All Approved Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="sidebar-link" href="./adminViewAlleLink">
                        <span class="icon-holder">
                            <i class="c-blue-500 ti-link"></i>
                        </span>
                        <span class="title">View e-Learning</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-graduation-cap"></i></span>
                        <span class="title">Courses</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <%--<li><a class="sidebar-link" href="./addNewCourse">Add New Course</a></li>--%>
                        <li><a class="sidebar-link" href="./adminViewAllCourses">View All Course</a></li>
                    </ul>
                </li>
                <%--<li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-bell"></i></span>
                        <span class="title">Add Notification</span> 
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./addNotification">Add New Notification</a></li>
                        <li><a class="sidebar-link" href="./viewAllNotification">View All Notification</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-indigo-500 fa fa-pencil-square-o"></i></span>
                        <span class="title">Report & Certificate</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./reportSubmission">Report Submission</a></li>
                        <li><a class="sidebar-link" href="./workingCertificate">Working Certificate</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-indigo-500 fa fa-inr"></i></span>
                        <span class="title">Maintenance Grant</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./maintenance">Apply for Maintenance Grant</a></li>
                        <li><a class="sidebar-link" href="./workingCertificate">Working Certificate</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="sidebar-link" href="./suggestionBox">
                        <span class="icon-holder"><i class="c-light-blue-500 ti-pencil"></i></span>
                        <span class="title">Request & Suggestion Box</span>
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="sidebar-link" href="./addLibraryDetails">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-star-o"></i></span>
                        <span class="title">Manuscript</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-check-square-o"></i></span>
                        <span class="title">Approval or Deletion</span> 
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./courseAdminPanel">Course</a></li>
                        <li><a class="sidebar-link" href="./notificationAdminPanel">Notification</a></li>
                        <li><a class="sidebar-link" href="./elinkAdminPanel">e-Learning link</a></li>
                        <li><a class="sidebar-link" href="./reportAdminPanel">Report</a></li>
                        <li><a class="sidebar-link" href="./workingCertificateAdminPanel">Working Certificate</a></li>
                        <li><a class="sidebar-link" href="./maintenanceAdminPanel">Maintenance Grant</a></li>
                        <li><a class="sidebar-link" href="./reportAndSuggestionAdminPanel">Report And Suggestion</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-red-500 ti-files"></i></span>
                        <span class="title">Pages</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./../error">404</a></li>
                        <li><a class="sidebar-link" href="./../error500">500</a></li>
                        <li><a class="sidebar-link" href="./../signin">Sign In</a></li>
                        <li><a class="sidebar-link" href="./../signup">Sign Up</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-teal-500 ti-view-list-alt"></i></span>
                        <span class="title">Multiple Levels</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="nav-item dropdown"><a href="javascript:void(0);"><span>Menu Item</span></a></li>
                        <li class="nav-item dropdown"><a href="javascript:void(0);"><span>Menu Item</span> <span
                                    class="arrow"><i class="ti-angle-right"></i></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:void(0);">Menu Item</a></li>
                                <li><a href="javascript:void(0);">Menu Item</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>--%>
            </ul>
        </div>
    </div>
    <div class="page-container">
        <div class="header navbar">
            <div class="header-container">
                <ul class="nav-left">
                    <li>
                        <a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a>
                    </li>
                    <!--                    <li class="search-box"><a class="search-toggle no-pdd-right" href="javascript:void(0);"><i
                                                    class="search-icon ti-search pdd-right-10"></i> <i
                                                    class="search-icon-close ti-close pdd-right-10"></i></a></li>
                                        <li class="search-input"><input class="form-control" type="text" placeholder="Search..."></li>-->
                </ul>
                <ul class="nav-right">
                    <%--<li class="notifications dropdown"><span class="counter bgc-red">3</span> <a href="#"
                                                                                                 class="dropdown-toggle no-after"
                                                                                                 data-toggle="dropdown">
                            <i class="ti-bell"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="pX-20 pY-15 bdB"><i class="ti-bell pR-10"></i> <span
                                    class="fsz-sm fw-600 c-grey-900">Notifications</span></li>
                            <li>
                                <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                                    <li><a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                            <div class="peer mR-15"><img class="w-3r bdrs-50p"
                                                                         src="../../../randomuser.me/api/portraits/men/1.jpg"
                                                                         alt=""></div>
                                            <div class="peer peer-greed"><span><span class="fw-500">John Doe</span> <span
                                                        class="c-grey-600">liked your <span class="text-dark">post</span></span></span>
                                                <p class="m-0">
                                                    <small class="fsz-xs">5 mins ago</small>
                                                </p>
                                            </div>
                                        </a></li>
                                    <li><a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                            <div class="peer mR-15"><img class="w-3r bdrs-50p"
                                                                         src="../../../randomuser.me/api/portraits/men/2.jpg"
                                                                         alt=""></div>
                                            <div class="peer peer-greed"><span><span class="fw-500">Moo Doe</span> <span
                                                        class="c-grey-600">liked your <span class="text-dark">cover image</span></span></span>
                                                <p class="m-0">
                                                    <small class="fsz-xs">7 mins ago</small>
                                                </p>
                                            </div>
                                        </a></li>
                                    <li><a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                            <div class="peer mR-15"><img class="w-3r bdrs-50p"
                                                                         src="../../../randomuser.me/api/portraits/men/3.jpg"
                                                                         alt=""></div>
                                            <div class="peer peer-greed"><span><span class="fw-500">Lee Doe</span> <span
                                                        class="c-grey-600">commented on your <span
                                                            class="text-dark">video</span></span></span>
                                                <p class="m-0">
                                                    <small class="fsz-xs">10 mins ago</small>
                                                </p>
                                            </div>
                                        </a></li>
                                </ul>
                            </li>
                            <li class="pX-20 pY-15 ta-c bdT"><span><a href="#" class="c-grey-600 cH-blue fsz-sm td-n">View All Notifications <i
                                            class="ti-angle-right fsz-xs mL-10"></i></a></span></li>
                        </ul>
                    </li>
                    <li class="notifications dropdown"><span class="counter bgc-blue">3</span> <a href="#"
                                                                                                  class="dropdown-toggle no-after"
                                                                                                  data-toggle="dropdown"><i
                                class="ti-email"></i></a>
                        <ul class="dropdown-menu">
                            <li class="pX-20 pY-15 bdB"><i class="ti-email pR-10"></i> <span
                                    class="fsz-sm fw-600 c-grey-900">Emails</span></li>
                            <li>
                                <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                                    <li><a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                            <div class="peer mR-15"><img class="w-3r bdrs-50p"
                                                                         src="../../../randomuser.me/api/portraits/men/1.jpg"
                                                                         alt=""></div>
                                            <div class="peer peer-greed">
                                                <div>
                                                    <div class="peers jc-sb fxw-nw mB-5">
                                                        <div class="peer"><p class="fw-500 mB-0">John Doe</p></div>
                                                        <div class="peer">
                                                            <small class="fsz-xs">5 mins ago</small>
                                                        </div>
                                                    </div>
                                                    <span class="c-grey-600 fsz-sm">Want to create your own customized data generator for your app...</span>
                                                </div>
                                            </div>
                                        </a></li>
                                    <li><a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                            <div class="peer mR-15"><img class="w-3r bdrs-50p"
                                                                         src="../../../randomuser.me/api/portraits/men/2.jpg"
                                                                         alt=""></div>
                                            <div class="peer peer-greed">
                                                <div>
                                                    <div class="peers jc-sb fxw-nw mB-5">
                                                        <div class="peer"><p class="fw-500 mB-0">Moo Doe</p></div>
                                                        <div class="peer">
                                                            <small class="fsz-xs">15 mins ago</small>
                                                        </div>
                                                    </div>
                                                    <span class="c-grey-600 fsz-sm">Want to create your own customized data generator for your app...</span>
                                                </div>
                                            </div>
                                        </a></li>
                                    <li><a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                            <div class="peer mR-15"><img class="w-3r bdrs-50p"
                                                                         src="../../../randomuser.me/api/portraits/men/3.jpg"
                                                                         alt=""></div>
                                            <div class="peer peer-greed">
                                                <div>
                                                    <div class="peers jc-sb fxw-nw mB-5">
                                                        <div class="peer"><p class="fw-500 mB-0">Lee Doe</p></div>
                                                        <div class="peer">
                                                            <small class="fsz-xs">25 mins ago</small>
                                                        </div>
                                                    </div>
                                                    <span class="c-grey-600 fsz-sm">Want to create your own customized data generator for your app...</span>
                                                </div>
                                            </div>
                                        </a></li>
                                </ul>
                            </li>
                            <li class="pX-20 pY-15 ta-c bdT"><span><a href="#" class="c-grey-600 cH-blue fsz-sm td-n">View All Email <i
                                            class="fs-xs ti-angle-right mL-10"></i></a></span></li>
                        </ul>
                    </li>--%>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown" style="padding: 0 20px 0 20px;">
                            <div class="peer mR-10">
                                <img class="w-2r bdrs-50p" src="${pageContext.request.contextPath}/resources${AdminSession.imagePath}" alt="">
                            </div>
                            <div class="peer" style="line-height: 1.5!important"><span class="fsz-md fw-600">${AdminSession.name}</span><br><span>[Role: <%= path %>]</span></div>
                        </a>
                        <ul class="dropdown-menu fsz-sm">
                           <%-- <li>
                                <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-settings mR-10"></i> 
                                    <span>Setting</span>
                                </a>
                            </li>--%>
                            <li>
                                <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-user mR-10"></i>
                                    <span>Profile</span>
                                </a>
                            </li>
                            <%--<li>
                                <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-email mR-10"></i> 
                                    <span>Messages</span>
                                </a>
                            </li>--%>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="./dashboard/logout" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-power-off mR-10"></i> 
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>