<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
//    out.println(path);
    path = path.split("/")[2];
//    out.println(path);
%>
<div id="loader">
    <div class="spinner"></div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function(){
        const loader = document.getElementById('loader');
        setTimeout(function() {
            loader.classList.add('fadeOut');
        }, 300);
    });
</script>
<div>
    <div class="sidebar">
        <div class="sidebar-inner">
            <div class="sidebar-logo">
                <div class="peers ai-c fxw-nw">
                    <div class="peer peer-greed">
                        <a class="sidebar-link td-n" href="./dashboard" class="td-n">
                            <div class="peers ai-c fxw-nw">
                                <div class="peer">
                                    <div class="logo">
                                        <img src="${pageContext.request.contextPath}/resources/Images/logo.png" alt="">
                                    </div>
                                </div>
                                <div class="peer peer-greed">
                                    <h5 class="lh-1 mB-0 logo-text">${LibrarianSession.name}</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="peer">
                        <div class="mobile-toggle sidebar-toggle">
                            <a href="#" class="td-n">
                                <i class="ti-arrow-circle-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="sidebar-menu scrollable pos-r">
                <li class="nav-item mT-30 active">
                    <a class="sidebar-link" href="./dashboard" default>
                        <span class="icon-holder">
                            <i class="c-blue-500 ti-home"></i>
                        </span>
                        <span class="title">Librarian Dashboard</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-orange-500 fa fa-user-o"></i></span>
                        <span class="title">User Details</span> 
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./allUsers">All Users</a></li>
                        <li><a class="sidebar-link" href="./allApprovedUsers">All Approved Users</a></li>
                        <li><a class="sidebar-link" href="./allNonApprovedUsers">All Non-Approved Users</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-book"></i></span>
                        <span class="title">Book Details</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./addNewBook">Add New Book</a></li>
                        <li><a class="sidebar-link" href="./entryBookDetails">Entry Book Details</a></li>
                        <li><a class="sidebar-link" href="./viewAllBookRequest">View Book Request</a></li>
                        <li><a class="sidebar-link" href="./viewAllApprovedBookRequest">View All Approved Book Request</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="sidebar-link" href="./addNeweLink">
                        <span class="icon-holder">
                            <i class="c-blue-500 ti-link"></i>
                        </span>
                        <span class="title">Add e-Learning</span>
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-graduation-cap"></i></span>
                        <span class="title">Courses</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./addNewCourse">Add New Course</a></li>
                        <%--<li><a class="sidebar-link" href="./viewAllCourse">View All Course</a></li>--%>
                    </ul>
                </li>
                <%--<li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-bell"></i></span>
                        <span class="title">Add Notification</span> 
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./addNotification">Add New Notification</a></li>
                        <li><a class="sidebar-link" href="./viewAllNotification">View All Notification</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-indigo-500 fa fa-pencil-square-o"></i></span>
                        <span class="title">Report & Certificate</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./reportSubmission">Report Submission</a></li>
                        <li><a class="sidebar-link" href="./workingCertificate">Working Certificate</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0);">
                        <span class="icon-holder"><i class="c-indigo-500 fa fa-inr"></i></span>
                        <span class="title">Maintenance Grant</span>
                        <span class="arrow"><i class="ti-angle-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="./maintenance">Apply for Maintenance Grant</a></li>
                        <li><a class="sidebar-link" href="./#">View Feedback</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="sidebar-link" href="./suggestionBox">
                        <span class="icon-holder"><i class="c-light-blue-500 ti-pencil"></i></span>
                        <span class="title">Request & Suggestion Box</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="sidebar-link" href="./addLibraryDetails">
                        <span class="icon-holder"><i class="c-indigo-500 fa fa-university"></i></span>
                        <span class="title">Library Details</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="sidebar-link" href="./addLibraryDetails">
                        <span class="icon-holder"><i class="c-blue-500 fa fa-star-o"></i></span>
                        <span class="title">Manuscript</span>
                    </a>
                </li>--%>
            </ul>
        </div>
    </div>
    <div class="page-container">
        <div class="header navbar">
            <div class="header-container">
                <ul class="nav-left">
                    <li>
                        <a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a>
                    </li>
                </ul>
                <ul class="nav-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown" style="padding: 0 20px 0 20px;">
                            <div class="peer mR-10">
                                <img class="w-2r bdrs-50p" src="${pageContext.request.contextPath}/resources${LibrarianSession.imagePath}" alt="">
                            </div>
                            <div class="peer" style="line-height: 1.5!important"><span class="fsz-md fw-600">${LibrarianSession.name}</span><br><span>[Role: <%=path%>]</span></div>
                        </a>
                        <ul class="dropdown-menu fsz-sm">
                            <%--<li>
                                <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-settings mR-10"></i> 
                                    <span>Setting</span>
                                </a>
                            </li>--%>
                            <li>
                                <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-user mR-10"></i>
                                    <span>Profile</span>
                                </a>
                            </li>
                            <%--<li>
                                <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-email mR-10"></i> 
                                    <span>Messages</span>
                                </a>
                            </li>--%>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="./dashboard/logout" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                    <i class="ti-power-off mR-10"></i> 
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>