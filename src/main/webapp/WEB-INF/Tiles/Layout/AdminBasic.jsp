<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="Tiles" %>

<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <link href="${pageContext.request.contextPath}/resources/CSS/style.css" rel="stylesheet">
        <link rel="icon" href="${pageContext.request.contextPath}/resources/Images/icon.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <style>
            #loader {
                transition: all .3s ease-in-out;
                opacity: 1;
                visibility: visible;
                position: fixed;
                height: 100vh;
                width: 100%;
                background: #fff;
                z-index: 90000
            }

            #loader.fadeOut {
                opacity: 0;
                visibility: hidden
            }

            .spinner {
                width: 40px;
                height: 40px;
                position: absolute;
                top: calc(50% - 20px);
                left: calc(50% - 20px);
                background-color: #333;
                border-radius: 100%;
                -webkit-animation: sk-scaleout 1s infinite ease-in-out;
                animation: sk-scaleout 1s infinite ease-in-out
            }

            @-webkit-keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    opacity: 0
                }
            }

            @keyframes sk-scaleout {
                0% {
                    -webkit-transform: scale(0);
                    transform: scale(0)
                }
                100% {
                    -webkit-transform: scale(1);
                    transform: scale(1);
                    opacity: 0
                }
            }
        </style>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/SweetAlert/sweetalert.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/DatePicker/bootstrap-datepicker.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/DatePicker/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/ClockPicker/jquery-clockpicker.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/ClockPicker/jquery-clockpicker.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/CSS/jquery_Toastr/toastr.min.css">
    </head>
    <body class="app">
        <Tiles:insertAttribute name="adminHeader" />
        <Tiles:insertAttribute name="body" />
        <Tiles:insertAttribute name="footer" />
    </body>
</html>