/*
package com.example.LibraryProject.UtilityCustomPackage;

import com.example.LibraryProject.Bean.CourseBean;
import com.example.LibraryProject.Entity.Course;
import java.util.ArrayList;
import java.util.List;

public class CourseSearchList {

    public List<Course> searchCourseOrderedList(List<Course> resultList, CourseBean courseBean) {

        String courseType = courseBean.getCourseType().trim();
        String education = courseBean.getEducation().trim();
        String stream = courseBean.getStream().trim();
        String addedBy = courseBean.getAddedBy().trim();

        List<Course> list = new ArrayList<>();
        int count = 0;

        for (Course course : resultList) {
            count = 0;
            if (course.getCourseType().equals(courseType)) {
                count++;
            }
            if (course.getEducation().equals(education)) {
                count++;
            }
            if (course.getStream().equals(stream)) {
                count++;
            }
            if (course.getAddedBy().equals(addedBy)) {
                count++;
            }
            if (count == 4) {
                list.add(course);
            }
        }

        for (Course course : resultList) {
            count = 0;
            if (course.getCourseType().equals(courseType)) {
                count++;
            }
            if (course.getEducation().equals(education)) {
                count++;
            }
            if (course.getStream().equals(stream)) {
                count++;
            }
            if (course.getAddedBy().equals(addedBy)) {
                count++;
            }
            if (count == 3) {
                list.add(course);
            }
        }

        for (Course course : resultList) {
            count = 0;
            if (course.getCourseType().equals(courseType)) {
                count++;
            }
            if (course.getEducation().equals(education)) {
                count++;
            }
            if (course.getStream().equals(stream)) {
                count++;
            }
            if (course.getAddedBy().equals(addedBy)) {
                count++;
            }
            if (count == 2) {
                list.add(course);
            }
        }

        for (Course course : resultList) {
            count = 0;
            if (course.getCourseType().equals(courseType)) {
                count++;
            }
            if (course.getEducation().equals(education)) {
                count++;
            }
            if (course.getStream().equals(stream)) {
                count++;
            }
            if (course.getAddedBy().equals(addedBy)) {
                count++;
            }
            if (count == 1) {
                list.add(course);
            }
        }
        
        return list;
    }

}
*/
