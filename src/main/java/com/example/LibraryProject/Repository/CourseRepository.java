/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Course;
import java.util.Date;
import java.util.List;

import com.example.LibraryProject.dto.CourseBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long>{
    @Query("from Course where id=?1")
    public Course findCourseBy(Long id);

    @Query("from Course where forwardBy=?1 or addedDate between ?2 and ?3")
    public Iterable<Course> findCourseBy(String libraryCode, Date startDate, Date endDate);

    @Query("from Course c where c.library.librarian.id =?1 ")
    List<Course> getAllCourcesByLibrarianId(Long id);

    @Query("from Course where courseType=?1 and education=?2 and stream=?3")
    List<Course> findCourseByParameter(String courseType, String education, String stream);
}
