package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Book;
import com.example.LibraryProject.Entity.BookEntryDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Partha Sarothi
 * 9/4/2018,5:16 PM
 **/
@Repository
public interface BookEntryDetailsRepository extends CrudRepository<BookEntryDetails, Long> {

    boolean existsByBook(Book book);

    BookEntryDetails findByBook(Book book);

    @Query("SELECT SUM(quantity) FROM BookEntryDetails WHERE book_id=?1")
    int stockOfBooksById(Long bookId);

    long findBookById(Long bookId);
}
