/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Book;
import com.example.LibraryProject.Entity.Library;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    @Query("from Book b where b.library.id= :libraryId AND (" +
            "publisherName LIKE CONCAT('%',:search,'%') OR authorName LIKE CONCAT('%',:search,'%') OR bookName LIKE CONCAT('%',:search,'%'))"
            )
    List<Book> findSearchBook(@Param("libraryId")Long libraryId, @Param("search") String search);

    @Query("from Book b where not (b.library.id = :libraryId) AND ("  +
            "publisherName LIKE CONCAT('%',:search,'%') OR authorName LIKE CONCAT('%',:search,'%') OR bookName LIKE CONCAT('%',:search,'%'))"
          )
            List<Book> getSearchBooksNotInLibrary(@Param("libraryId")Long libraryId, @Param("search") String search);

    @Query("from Book b where b.library.librarian.id=?1")
    List<Book> getAllBooksByLibrarianId(Long id);

    @Query("from Book b where b.library.id=?1")
    List<Book> findBookByLibraryId(Long libraryId);

    List<Book> findBookByLibrary(Library library);

    List<String> findByPublisherNameContains(String data);

    @Query("from Book where id=?1")
    Book findBookByBookId(Long bookId);

}
