package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Admin;
import com.example.LibraryProject.Entity.Librarian;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends CrudRepository<Admin, Long> {
    @Query("from Admin where (email=?1 or username=?1 or contactNo=?1) and Password=?2")
    Admin validateUser(String firstField, String Password);

    @Query("from Admin where Email=?1")
    Admin findUserByEmail(String email);
}
