package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.BookRequest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Partha Sarothi
 * 9/13/2018,1:28 PM
 **/
@Repository
public interface BookRequestRepository extends CrudRepository<BookRequest,Long> {

    List<BookRequest> findByLibraryId(Long id);

    BookRequest findByBookId(Long id);
    @Query("from BookRequest where id=?1")
    BookRequest findBookRequestById(Long id);

    @Query("from BookRequest where id = (select max(id) from BookRequest where userId =?1 and bookId=?2)")
    BookRequest findBookRequestByUserIdAndBookId(Long userId, Long bookId);

    @Query("from BookRequest where libraryId=?1 and requestStatus = '1'")
    List<BookRequest> showAllApprovedBookRequest(Long libraryId);

    @Transactional
    @Modifying
    @Query("update BookRequest set bookIssueId=?1 where bookId=?2 and userId=?3")
    void updateBookIssueId(Long bookIssueDetailsId, Long bookId, Long userId);

    @Transactional
    @Modifying
    @Query("update BookRequest set bookEntryId=?1 where bookId=?2 and userId=?3")
    void updateBookEntryId(Long bookEntryDetailsId, Long bookId, Long userId);

    @Query("SELECT count (id) FROM BookRequest WHERE bookId=?1 and bookIssueId=?2 and bookEntryId is null")
    long issueBookDetails(Long bookId, Long id);

    @Query("SELECT count (id) FROM BookRequest WHERE userId=?1 and bookIssueId is not null and bookEntryId is null ")
    int getAllIssuedBook(Long id);
}
