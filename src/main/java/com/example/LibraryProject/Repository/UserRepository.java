package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
    
    @Query("from User where (email=?1 or username=?1 or contactNo=?1) and password=?2")
    User validateUser(String firstField, String password);
    User findByEmail(String email);

    @Query("from User where id=?1")
    User findUserById(Long id);

    @Query("from User u where u.library.librarian.id  =?1")
    List<User> findAllByLibrarianId(Long librarianId);

    @Query("from User u where u.library.librarian.id  =?1 and status = 1")
    List<User> getAllApprovedUsers(Long id);

    @Query("from User u where u.library.librarian.id  =?1 and status = -1")
    List<User> getAllNonApprovedUsers(Long id);

    @Query("select u.username from User u where u.username =?1")
    String getUsernameFieldValue(String username);

}

