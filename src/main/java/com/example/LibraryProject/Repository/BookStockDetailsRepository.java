/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Book;
import com.example.LibraryProject.Entity.BookStockDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookStockDetailsRepository extends CrudRepository<BookStockDetails,Long>{
    long findByNoOfAllocateBook(Long id);

    BookStockDetails findByBook(Book book);
}
