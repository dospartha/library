package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Library;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddLibraryRepository extends CrudRepository<Library, Long>{
    
    /*@Query("from AddLibrary where libraryCode=?1")
    public Library findByLibraryCode(String libraryCode);*/
    
}
