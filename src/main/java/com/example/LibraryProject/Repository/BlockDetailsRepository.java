package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.BlockDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlockDetailsRepository extends CrudRepository<BlockDetails, Long> {

    @Query("from BlockDetails where subdivision_id=?1")
    public List<BlockDetails> findBlocksBySdoId(Long sdoId);

    @Query("from BlockDetails where id=?1")
    public BlockDetails findBlockDetailsById(Long id);
}
