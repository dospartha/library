package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.ELearningLink;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ELearningLinkRepository extends CrudRepository<ELearningLink, Long>{
    
    /*@Query("from eLearningLink where Id = ?1")
    public ELearningLink findeLinkById(Long id);*/

    /*@Query("from eLearningLink where libraryCode = ?1 or addedDate between ?2 and ?3")
    public Iterable<ELearningLink> findelinkBy(String libraryCode, Date startDate, Date endDate);*/
    @Query("from ELearningLink e where e.library.librarian.id =?1 ")
    List<ELearningLink> getAlleLinksByLibrarianId(Long id);
    List<ELearningLink> findByTopic(String topic);
}
