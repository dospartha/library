package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.MaintenanceGrant;
import java.util.Date;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaintenanceGrantRepository extends CrudRepository<MaintenanceGrant, Long>{
    
    @Query("from MaintenanceGrant where libraryCode=?1 or addedDate between ?2 and ?3")
    public Iterable<MaintenanceGrant> findMiantenanceBy(String libraryCode, Date startDate, Date endDate);

    @Query("from MaintenanceGrant where id=?1")
    public MaintenanceGrant findMiantenanceById(Long id);
    
}
