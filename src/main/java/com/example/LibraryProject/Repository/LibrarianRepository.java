package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Librarian;
import com.example.LibraryProject.Entity.Library;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface LibrarianRepository extends CrudRepository<Librarian, Long> {
    @Query("from Librarian where (email=?1 or username=?1 or contactNo=?1) and Password=?2")
    Librarian validateUser(String firstField, String Password);

    @Query("from Librarian where Email=?1")
    public Librarian findUserByEmail(String email);

    @Query("from Librarian where id=?1")
    public Librarian findUserById(Long id);

    @Query(value = "select * from librarian WHERE status = 1 and librarian.id NOT IN (select library.librarian_id from library)",nativeQuery = true)
    List<Librarian> getAllNotAssignLibrarian();
}
