package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.BookIssueDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Partha Sarothi
 * 9/27/2018,1:42 PM
 **/
@Repository
public interface BookIssueDetailsRepository extends CrudRepository<BookIssueDetails,Long> {
    @Query("from BookIssueDetails where id = (select max(id)from BookIssueDetails where book_id=?1 and user_id=?2)")
    BookIssueDetails isBookIssued(Long bookId, Long userId);

    @Query("FROM BookIssueDetails WHERE id in (SELECT bookIssueId from BookRequest where bookEntryId is null and userId =?1)")
    List<BookIssueDetails> getAllIssuedBook(Long id);
}
