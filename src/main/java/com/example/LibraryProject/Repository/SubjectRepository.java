package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Subject;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Partha sarothi
 * 9/4/2018,1:08 PM
 **/
public interface SubjectRepository  extends CrudRepository<Subject, Long> {
}
