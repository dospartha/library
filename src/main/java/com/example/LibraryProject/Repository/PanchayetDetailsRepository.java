package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.PanchayetDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Partha Sarothi
 * 9/10/2018,4:29 PM
 **/
public interface PanchayetDetailsRepository extends CrudRepository<PanchayetDetails, Long> {
    @Query("from PanchayetDetails where block_id=?1")
    List<PanchayetDetails> findByBlockDetails(Long id);
}
