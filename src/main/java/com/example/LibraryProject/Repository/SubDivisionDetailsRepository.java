package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.SubDivisionDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubDivisionDetailsRepository extends CrudRepository<SubDivisionDetails,Long> {
}
