package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.Librarian;
import com.example.LibraryProject.Entity.Library;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibraryRepository extends CrudRepository<Library, Long>{
    @Query("from Library l where l.panchayetDetails.id=?1")
    List<Library> findLibrariesByPanchayetId(long panchayetId);

    @Query("from Library l where l.librarian.id=?1")
    Library getLibraryByLibrarian(Long id);

    @Query("from Library l where l.municipalityDetails.id=?1")
    List<Library> findLibrariesByMunicipalityId(long municipalityId);

    /*@Query("from Library where blockDetails_Id=?1")
    List<Library> getAllLibraryByMunicipleId(long bdomuniId);*/

}
