/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.ReportAndCertificate;
import java.util.Date;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportAndCertificateRepository extends CrudRepository<ReportAndCertificate, Long>{
    
    @Query("from ReportAndCertificate where libraryCode=?1 or addedDate between ?2 and ?3")
    public Iterable<ReportAndCertificate> findReportBy(String libraryCode, Date startDate, Date endDate);

    @Query("from ReportAndCertificate where id=?1")
    public ReportAndCertificate findReportById(Long id);
    
}
