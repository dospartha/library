package com.example.LibraryProject.Repository;

import com.example.LibraryProject.Entity.MunicipalityDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MunicipalityDetailsRepository extends CrudRepository<MunicipalityDetails,Long> {
    @Query("from MunicipalityDetails where subdivision_id=?1")
    public List<MunicipalityDetails> findMunicipalitiesBySdoId(Long sdoId);

    @Query("from MunicipalityDetails where id=?1")
    public MunicipalityDetails findMunicipalityDetailsById(Long id);
}
