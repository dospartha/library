package com.example.LibraryProject.Util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import com.google.common.base.Charsets;

/**
 * @author Partha Sarothi
 * 9/17/2018,12:26 PM
 **/
public class MailSend {

    public static void sendHtmlTemplateMail(HttpSession session, HashMap<String, String> hashMap, String destinationAddress, String subject) {
        ServletContext context = session.getServletContext();
        String path = context.getRealPath("/resources/html/mailTemplate.html");
        System.out.println("Path=="+ path);
        String content = null;
        try {
            content = com.google.common.io.Files.toString(new File(path), Charsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();;
        }
        if (!hashMap.isEmpty()) {
            Set mapSet = hashMap.entrySet();
            Iterator it = mapSet.iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                content = content.replace("[[" + entry.getKey().toString() + "]]", entry.getValue().toString());
            }
        }
        if (content.indexOf("No button") != -1 && content.indexOf("No button") > 0) {
            int start = content.indexOf("<table class=\"btn-primary");
            int end = content.indexOf("</table>", start);
            String newcontent = content.substring(start, end + 8);
            content = content.replace(newcontent, "");
        } else if (content.indexOf("No second button") != -1 && content.indexOf("No second button") > 0) {
            int start = content.indexOf("<table class=\"btn-primary");
            int start1 = content.indexOf("<td><div>", start);
            int end = content.indexOf("</a>", start1);
            String newcontent = content.substring(start1, end + 8);
            content = content.replace(newcontent, "");
        }
        Mail.sendMail(destinationAddress, subject, content);
    }



}
