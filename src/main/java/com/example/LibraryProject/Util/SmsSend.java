package com.example.LibraryProject.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import javax.servlet.http.HttpSession;

public class SmsSend {
    MailSend mainSend = new MailSend();
    public static void sendOTPMail(HttpSession session, String userName, String otp, String email) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("mail_heading", "E-Subechha - One Time Password");
            hashMap.put("mail_content", "<p>Dear " + userName + ",</p>"
                    + "<p>Thank You For Registering on E-Subechha Portal.</p>"
                    + "<p>Please Use " + otp + " to complete your registration process.</p>"
                    + "<p>Please do not share this one time password with anyone else.</p>");
            hashMap.put("mail_button_text1", "No button");

            MailSend.sendHtmlTemplateMail(session, hashMap, email, "E-Subechha - One Time Password");
        } catch (Exception ex) {
        }
    }

    public static void sendSms(String no, String text) throws Exception {
        InputStream is = new URL(
                "http://api.msg91.com/api/sendhttp.php?authkey=113601AINMyMWBSkG857400860&mobiles=+91" + no + "&sender=LibMng&route=4&country=0&message=" + URLEncoder.encode(text, "UTF-8")).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            String xmlText = sb.toString();
            System.out.println("xmlText=" + xmlText);

        } finally {
            is.close();
        }
    }
}
