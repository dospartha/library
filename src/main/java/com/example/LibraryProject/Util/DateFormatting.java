package com.example.LibraryProject.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatting {

    public Date formatDateType1(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate;
        Date NewDate = null;
        try {
            convertedDate = dateFormat.parse(date);
            String newDate = dateFormat1.format(convertedDate);
            NewDate = dateFormat1.parse(newDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return NewDate;

    }
    
    public Date formatDateType2(String date) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
//        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate;
        Date NewDate = null;
        try {
            convertedDate = dateFormat.parse(date);
            String newDate = dateFormat1.format(convertedDate);
            NewDate = dateFormat1.parse(newDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return NewDate;

    }
}