package com.example.LibraryProject.Service;

import com.example.LibraryProject.Entity.Book;
import com.example.LibraryProject.Entity.BookIssueDetails;
import com.example.LibraryProject.Entity.Course;
import com.example.LibraryProject.Entity.ELearningLink;
import com.example.LibraryProject.dto.BookRequestBean;
import com.example.LibraryProject.dto.CourseBean;

import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:19 PM
 **/
public interface UserService {

    List<BookIssueDetails> getAllIssuedBooks(Long id);

    List<Book> getAllBooksByLibraryId(Long libraryId);

    List<Book> getAllBooks();

    List<Book> getSearchBooksNotInLibrary(Long libraryId, String search);

    List<Book> getBooksBySearchParam(Long id, String search);

    List<ELearningLink> getAllELearning();

    List<ELearningLink> getELearningBySearchParam(String topic);

    String addBookRequest(BookRequestBean bookRequestBean);

    List<Course> getAllCourses();

    List<Course> getCoursesBySearchParam(CourseBean courseBean);
}
