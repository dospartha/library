package com.example.LibraryProject.Service;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.dto.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:57 PM
 **/
public interface LibrarianService {
    Librarian validateUser(String firstField, String password);
    String addLibrarian(Librarian librarian, HttpServletRequest request, HttpSession session);
    String forgetPassword(HttpSession session, String email);
    void userApproval(Long id);
    void userNonApprove(Long id);
    List<User> getAllApprovedUsers(Long id);
    List<User> getAllNonApprovedUsers(Long id);
    String addBooks(Book book);
    List<Subject> getAllSubject();
    Library getLibraryByLibrarian(Long id);
    List<Book> getAllBooks();
    List<Book> getAllBooksByLibrarianId(Long id);
    List<BookEntryDetails> getTotalBooksEntry();
    String entryBooksDetails(BookEntryDetailsBean bookEntryDetailsBean);
    List viewAllBookRequest(Long id);
    String approveBookRequest(Long bookId);
    String discardBookRequest(Long bookId);
    List<UserBookRequestDetailsBean> showAllApprovedBookRequest(Long libraryId);
    String addBookIssueDetails(UserBookIssueDetailsBean userBookIssueDetailsBean);
    String depositBookByUser(BookEntryDetailsBean bookEntryDetailsBean);
    String addeLearningLink(ELearningLink eLearningLink, HttpServletRequest request);
    List<ELearningLink> getAlleLinksByLibrary(Long id);
    String addCourse(CourseBean courseBean, HttpSession session, HttpServletRequest request);

    List getAllCourse();

    List<Course> getAllCourseByLibrary(Long id);

    List getAllNotifications();

    String addNotification(NotificationBean notificationBean, HttpSession session, String notification);


//    public String checkBookEntryByBookId(Long bookId);
//    public String updateBooksDetails(BookEntryDetails bookEntryDetails);
}
