package com.example.LibraryProject.Service.ServiceImpl;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Repository.*;
import com.example.LibraryProject.Service.UserService;
import com.example.LibraryProject.dto.BookRequestBean;
import com.example.LibraryProject.dto.CourseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:17 PM
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    BookRepository bookRepository;
    @Autowired
    BookRequestRepository bookRequestRepository;
    @Autowired
    BookIssueDetailsRepository bookIssueDetailsRepository;
    @Autowired
    private ELearningLinkRepository eLearningLinkRepository;
    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<BookIssueDetails> getAllIssuedBooks(Long id) {
        List<BookIssueDetails> bookIssueDetailsList  = new ArrayList<BookIssueDetails>();
        for (BookIssueDetails bookIssueDetails : bookIssueDetailsRepository.getAllIssuedBook(id)) {
           Book book = bookRepository.findBookByBookId(bookIssueDetails.getBookId());
           if (bookIssueDetails.getBook()== null) {
               bookIssueDetails.setBook(book);
               bookIssueDetailsList.add(bookIssueDetails);
           }
        }
       return bookIssueDetailsList;
    }

    @Override
    public List<Book> getAllBooksByLibraryId(Long libraryId) {
        return bookRepository.findBookByLibraryId(libraryId);
    }

    @Override
    public List<Book> getSearchBooksNotInLibrary(Long libraryId, String search) {
        return bookRepository.getSearchBooksNotInLibrary(libraryId, search);
    }

    public List<Book> getAllBooks() {
        return (List<Book>) bookRepository.findAll();
    }

    public List<Book> getBooksBySearchParam(Long id, String search) {
        return bookRepository.findSearchBook(id,search);
    }

    @Override
    public String addBookRequest(BookRequestBean bookRequestBean) {
        String status = "";
        BookRequest bookRequest = bookRequestRepository.findBookRequestByUserIdAndBookId(bookRequestBean.getUserId(), bookRequestBean.getBookId());
        if (bookRequest != null) {
            if (bookRequest.getRequestStatus() == 1 && bookRequest.getBookIssueId() != null && bookRequest.getBookEntryId() != null) {
                status = insertBookRequestData(bookRequestBean);
            }else if (bookRequest.getRequestStatus()== 2){
                status = insertBookRequestData(bookRequestBean);
            }else {
                status = "This book is already requested by You";
            }
        } else {
            status = insertBookRequestData(bookRequestBean);
        }
        return status;
    }

    private String insertBookRequestData(BookRequestBean bookRequestBean) {
        String status;BookRequest bookRequest1 = new BookRequest();
        bookRequest1.setUserId(bookRequestBean.getUserId());
        bookRequest1.setBookId(bookRequestBean.getBookId());
        bookRequest1.setLibraryId(bookRequestBean.getLibraryId());
        try {
            bookRequestRepository.save(bookRequest1);
            status = "Success";
        } catch (Exception e) {
            status = "Failure";
        }
        return status;
    }

    @Override
    public List<Course> getAllCourses() {
        return (List<Course>) courseRepository.findAll();
    }

    @Override
    public List<Course> getCoursesBySearchParam(CourseBean courseBean) {
        String courseType = courseBean.getCourseType();
        String education = courseBean.getEducation();
        String stream = courseBean.getStream();
        return courseRepository.findCourseByParameter(courseType, education, stream);
    }

    @Override
    public List<ELearningLink> getAllELearning() {
        return (List<ELearningLink>) eLearningLinkRepository.findAll();
    }

    @Override
    public List<ELearningLink> getELearningBySearchParam(String topic) {
        return eLearningLinkRepository.findByTopic(topic);
    }

    /*public List<String> getPublishersByHint(String data) {
        return bookRepository.findByPublisherNameContains(data);
    }*/
    /*public List<Book> getAllBooks(Long id) {
        Library library = new Library();
        library.setId(id);
        return bookRepository.findBookByLibrary(library);
    }*/
}
