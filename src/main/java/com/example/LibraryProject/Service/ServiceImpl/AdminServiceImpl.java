package com.example.LibraryProject.Service.ServiceImpl;

import com.example.LibraryProject.Entity.Admin;
import com.example.LibraryProject.Entity.Librarian;
import com.example.LibraryProject.Entity.Library;
import com.example.LibraryProject.Entity.User;
import com.example.LibraryProject.Repository.AdminRepository;
import com.example.LibraryProject.Repository.LibrarianRepository;
import com.example.LibraryProject.Repository.LibraryRepository;
import com.example.LibraryProject.Repository.UserRepository;
import com.example.LibraryProject.Service.AdminService;
import com.example.LibraryProject.Util.MailSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:48 PM
 **/
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    LibrarianRepository librarianRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LibraryRepository libraryRepository;

    public Admin validateAdmin(String firstField, String password, HttpServletRequest request, HttpSession session) {
        Admin admin = adminRepository.validateUser(firstField, password);
        if (admin != null) {
            session.invalidate();
            HttpSession newSession = request.getSession(true);
            newSession.setAttribute("AdminSession", admin);
            return admin;
        } else {
            return null;
        }
    }

    public String addAdmin(Admin admin, HttpServletRequest request, HttpSession session) {
        try {
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            System.out.println(path);
            byte[] image = admin.getImageUser().getBytes();
            String userImageFolder = path + "/admin_image/" + admin.getName() + ".jpg";
            admin.setImagePath("/admin_image/" + admin.getName() + ".jpg");
            FileOutputStream perimageOutFile = new FileOutputStream(userImageFolder);
            perimageOutFile.write(image);

            adminRepository.save(admin);
            return "Success";
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String forgetPassword(HttpSession session, String email) {
//        MailSend mailSend = new MailSend();
        Admin admin = adminRepository.findUserByEmail(email);
        if (admin != null) {
            try {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("mail_heading", "Forget Password");
                hashMap.put("mail_content", "<p>Hello <b>" + admin.getName() + "</b>,</p>"
                        + "<p><b>Email Id : </b>" + admin.getEmail() + "</p>"
                        + "<p><b>Password : </b>" + admin.getPassword() + "</p>"
                        + "<p>Please change your password at the next login.</p>");
                hashMap.put("mail_button_text1", "No button");

                MailSend.sendHtmlTemplateMail(session, hashMap, email, "Forget Password");
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE";
            }
        } else {
            return "FAILURE";
        }
    }

    public List<Librarian> getAllLibrarian() {
        List<Librarian> list = (List<Librarian>) librarianRepository.findAll();
        return list;
    }

    public String librarianApproval(Long id) {
        Librarian librarian = librarianRepository.findUserById(id);
        if (librarian != null) {
            librarian.setStatus(1);
            librarianRepository.save(librarian);
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public String librarianNonApprove(Long id) {
        Librarian librarian = librarianRepository.findUserById(id);
        if (librarian != null) {
            librarian.setStatus(-1);
            librarianRepository.save(librarian);
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public List<Librarian> getAllApprovedLibrarian() {
        List<Librarian> librarianList = new ArrayList<>();
        for (Librarian librarian : librarianRepository.findAll()) {
            if (librarian.isIsActive() == true) {
                if (librarian.getStatus() == 1) {
                    librarianList.add(librarian);
                }
            }
        }
        return librarianList;
    }

    public List<Librarian> getAllNonApprovedLibrarian() {
        List<Librarian> list = new ArrayList<>();
        for (Librarian librarian : librarianRepository.findAll()) {
            if (librarian.isIsActive() == true) {
                if (librarian.getStatus()== -1) {
                    list.add(librarian);
                }
            }
        }
        return list;
    }

    public List<User> getAllApprovedUsers() {
        List<User> list = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            if (user.isIsActive() == true) {
                if (user.getStatus() == 1) {
                    list.add(user);
                }
            }
        }
        return list;
    }

    public String addLibrary(Library library, HttpSession session) {
        try {
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] libraryImageFile = library.getImageLibrary().getBytes();
            String librarianImageFolder = path + "/libraryImage/" + library.getLibraryName() + ".jpg";
            library.setImagePath("/libraryImage/" + library.getLibraryName() + ".jpg");
            FileOutputStream file = new FileOutputStream(librarianImageFolder);
            file.write(libraryImageFile);

            libraryRepository.save(library);
            return "SUCCESS";
        } catch (IOException e) {
            return "FAILURE";
        }
    }

    public List<Library> getAllLibrary() {
        List<Library> libraryList = new ArrayList<>();
        for (Library library : libraryRepository.findAll()) {
            if (library!=null) {
                libraryList.add(library);
            }
        }
        return libraryList;
    }

    public List<Librarian> getAllNotAssignLibrarian() {
        List<Librarian> librarians = librarianRepository.getAllNotAssignLibrarian();
        return  librarians;
    }
}
