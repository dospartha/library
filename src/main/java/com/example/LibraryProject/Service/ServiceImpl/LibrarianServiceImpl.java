package com.example.LibraryProject.Service.ServiceImpl;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Repository.*;
import com.example.LibraryProject.Service.LibrarianService;
import com.example.LibraryProject.Util.MailSend;
import com.example.LibraryProject.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:55 PM
 **/
@Service
public class LibrarianServiceImpl implements LibrarianService {

    @Autowired
    private LibrarianRepository librarianRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private LibraryRepository libraryRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookEntryDetailsRepository bookEntryDetailsRepository;
    @Autowired
    private ELearningLinkRepository eLearningLinkRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    private BookRequestRepository bookRequestRepository;
    @Autowired
    private BookIssueDetailsRepository bookIssueDetailsRepository;
    @Autowired
    private BookStockDetailsRepository bookStockDetailsRepository;
    @Autowired
    private NotificationRepository notificationRepository;


    public Librarian validateUser(String firstField, String password) {
        Librarian librarian = librarianRepository.validateUser(firstField, password);
        if (librarian != null) {
            return librarian;
        } else {
            return null;
        }
    }

    public String addLibrarian(Librarian librarian, HttpServletRequest request, HttpSession session) {
        try {
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            System.out.println(path);
            byte[] image = librarian.getImageUser().getBytes();
            String librarianImageFolder = path + "/librarian_image/" + librarian.getName() + ".jpg";
            librarian.setImagePath("/librarian_image/" + librarian.getName() + ".jpg");
            FileOutputStream perimageOutFile = new FileOutputStream(librarianImageFolder);
            perimageOutFile.write(image);
            librarianRepository.save(librarian);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public String forgetPassword(HttpSession session, String email) {
        Librarian librarian = librarianRepository.findUserByEmail(email);
        if (librarian != null) {
            try {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("mail_heading", "Forget Password");
                hashMap.put("mail_content", "<p>Hello <b>" + librarian.getName() + "</b>,</p>"
                        + "<p><b>Email Id : </b>" + librarian.getEmail() + "</p>"
                        + "<p><b>Password : </b>" + librarian.getPassword() + "</p>"
                        + "<p>Please change your password at the next login.</p>");
                hashMap.put("mail_button_text1", "No button");

                MailSend.sendHtmlTemplateMail(session, hashMap, email, "Forget Password");
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE";
            }
        } else {
            return "FAILURE";
        }
    }

    public void userApproval(Long id) {
        User user = userRepository.findUserById(id);
        if (user != null) {
            user.setStatus(1);
            userRepository.save(user);
        }
    }

    public void userNonApprove(Long id) {
        User user = userRepository.findUserById(id);
        if (user != null) {
            user.setStatus(-1);
            userRepository.save(user);
        }
    }

    public List<User> getAllApprovedUsers(Long id) {
        List<User> list = userRepository.getAllApprovedUsers(id);
        return list;
    }

    public List<User> getAllNonApprovedUsers(Long id) {
        List<User> list = userRepository.getAllNonApprovedUsers(id);
        return list;
    }

    public String addBooks(Book book) {
        try {
            bookRepository.save(book);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    public List<Subject> getAllSubject() {
        return (List<Subject>) subjectRepository.findAll();
    }

    public Library getLibraryByLibrarian(Long id) {
        Library library = libraryRepository.getLibraryByLibrarian(id);
        return library;
    }

    public List<Book> getAllBooks() {
        return (List<Book>) bookRepository.findAll();
    }

    public List<Book> getAllBooksByLibrarianId(Long id) {
        return bookRepository.getAllBooksByLibrarianId(id);
    }

    public List<BookEntryDetails> getTotalBooksEntry() {
        return (List<BookEntryDetails>) bookEntryDetailsRepository.findAll();
    }

    public String entryBooksDetails(BookEntryDetailsBean bookEntryDetailsBean) {
        BookEntryDetails bookEntryDetails = new BookEntryDetails();
        bookEntryDetails.setBook(bookEntryDetailsBean.getBook());
        bookEntryDetails.setQuantity(bookEntryDetailsBean.getQuantity());
        bookEntryDetails.setLibraryId(bookEntryDetailsBean.getLibraryId());

        try {
            bookEntryDetails = bookEntryDetailsRepository.save(bookEntryDetails);
            int quantity = bookEntryDetails.getQuantity();
            BookStockDetails bookStockDetails = bookStockDetailsRepository.findByBook(bookEntryDetails.getBook());
           if (bookStockDetails!= null){
               bookStockDetails.setNoOfTotalBook(bookStockDetails.getNoOfTotalBook() + quantity);
               bookStockDetailsRepository.save(bookStockDetails);
           }else {
               bookStockDetails = new BookStockDetails();
               Book book=bookRepository.findBookByBookId(new Long(bookEntryDetails.getBook().getId()));
               bookStockDetails.setNoOfTotalBook(bookEntryDetails.getQuantity());
               bookStockDetails.setNoOfAvailableBook(bookEntryDetails.getQuantity());
               bookStockDetails.setBook(book);
               bookStockDetailsRepository.save(bookStockDetails);
           }
           return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "FAILURE";
        }
    }

    @Override
    public List<UserBookRequestDetailsBean> viewAllBookRequest(Long id) {
        List<BookRequest> bookRequest = bookRequestRepository.findByLibraryId(id);
        List<UserBookRequestDetailsBean> userBookRequestList = new ArrayList();
        for (BookRequest request : bookRequest) {
            UserBookRequestDetailsBean userBookRequestDetailsBean = new UserBookRequestDetailsBean();
            Optional<UserBookRequestDetailsBean> existingUser = userBookRequestList.stream().filter(br -> br.getUser().getId() == request.getUserId()).findAny();
            User user = null;
            if (existingUser.isPresent()) {
                user = existingUser.get().getUser();
            } else {
                user = userRepository.findUserById(request.getUserId());
            }
            userBookRequestDetailsBean.setUser(user);
            Optional<UserBookRequestDetailsBean> existingBook = userBookRequestList.stream().filter(br -> br.getBook().getId() == request.getBookId()).findAny();
            Book book = null;
            if (existingBook.isPresent()) {
                book = existingBook.get().getBook();
            } else {
                book = bookRepository.findBookByBookId(request.getBookId());
            }
            userBookRequestDetailsBean.setBook(book);
            userBookRequestDetailsBean.setRequestDate(request.getRequestDate());
            userBookRequestDetailsBean.setRequestStatus(request.getRequestStatus());
            userBookRequestList.add(userBookRequestDetailsBean);
        }
        return userBookRequestList;
    }

    @Override
    public String approveBookRequest(Long bookId) {
        BookRequest bookRequest = bookRequestRepository.findByBookId(bookId);
        try {
            int quantity = bookEntryDetailsRepository.stockOfBooksById(bookId);
            if (quantity > 0 ){
                bookRequest.setRequestStatus(1);
                bookRequest.setApproveDate(new Date());
                bookRequestRepository.save(bookRequest);
                return "SUCCESS";
            }else {
                return "FAILURE";
            }
        }catch (Exception e){
            return e.getMessage();
        }
    }

    @Override
    public String discardBookRequest(Long bookId) {
        BookRequest bookRequest = bookRequestRepository.findByBookId(bookId);
        int quantity = bookEntryDetailsRepository.stockOfBooksById(bookId);
        if (quantity == 0 ){
            bookRequest.setRequestStatus(2);
            bookRequestRepository.save(bookRequest);
            return "SUCCESS";
        }else {
            return "FAILURE";
        }
    }

    @Override
    public List<UserBookRequestDetailsBean> showAllApprovedBookRequest(Long libraryId) {
        List<UserBookRequestDetailsBean> userBookRequestlist = new ArrayList();
        List<BookRequest> bookRequestList = bookRequestRepository.showAllApprovedBookRequest(libraryId);
        for (BookRequest bookRequest : bookRequestList) {
            UserBookRequestDetailsBean userBookRequestDetailsBean = new UserBookRequestDetailsBean();
            userBookRequestDetailsBean.setId(bookRequest.getId());
            User user = null;
            user = userRepository.findUserById(bookRequest.getUserId());
            userBookRequestDetailsBean.setUser(user);
            Book book = null;
            book = bookRepository.findBookByBookId(bookRequest.getBookId());
            userBookRequestDetailsBean.setBook(book);
            userBookRequestDetailsBean.setRequestDate(bookRequest.getRequestDate());
            userBookRequestDetailsBean.setApproveDate(bookRequest.getApproveDate());
            userBookRequestDetailsBean.setRequestStatus(bookRequest.getRequestStatus());
            userBookRequestDetailsBean.setIssueId(bookRequest.getBookIssueId());
            userBookRequestDetailsBean.setEntryId(bookRequest.getBookEntryId());
            userBookRequestlist.add(userBookRequestDetailsBean);
        }
        return userBookRequestlist;
    }

    @Override
    public String addBookIssueDetails(UserBookIssueDetailsBean userBookIssueDetailsBean) {
        BookRequest bookRequest = bookRequestRepository.findBookRequestById(userBookIssueDetailsBean.getId());
        if (bookRequest.getBookIssueId()== null){
            BookStockDetails bookStockDetails = new BookStockDetails();
            long totalNoOfBooks = bookEntryDetailsRepository.stockOfBooksById(userBookIssueDetailsBean.getBookId());
            bookStockDetails.setNoOfTotalBook((int) totalNoOfBooks);
            BookIssueDetails bookIssueDetails = new BookIssueDetails();
            bookIssueDetails.setBookId(userBookIssueDetailsBean.getBookId());
            bookIssueDetails.setUserId(userBookIssueDetailsBean.getUserId());
            bookIssueDetails.setQuantity(1);
            bookIssueDetails.setIssueDate(new Date());
            try {
                bookIssueDetails = bookIssueDetailsRepository.save(bookIssueDetails);
                bookRequestRepository.updateBookIssueId(bookIssueDetails.getId(),bookIssueDetails.getBookId(),bookIssueDetails.getUserId());
                long totalAllocateBooks = bookRequestRepository.issueBookDetails(bookIssueDetails.getBookId(),bookIssueDetails.getId());
                long availableBooks = totalNoOfBooks - totalAllocateBooks;
                bookStockDetails.setNoOfAllocateBook((int) totalAllocateBooks);
                bookStockDetails.setNoOfAvailableBook((int) availableBooks);
                Book book = bookRepository.findBookByBookId(bookRequest.getBookId());
                bookStockDetails.setBook(book);
                bookStockDetailsRepository.save(bookStockDetails);
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE";
            }
        }else {
            return "Issued";
        }
        /*BookIssueDetails issuedBook = bookIssueDetailsRepository.isBookIssued(userBookIssueDetailsBean.getBookId(),userBookIssueDetailsBean.getUserId());
        if (issuedBook ==null) {
            BookIssueDetails bookIssueDetails = new BookIssueDetails();
            bookIssueDetails.setBookId(userBookIssueDetailsBean.getBookId());
            bookIssueDetails.setUserId(userBookIssueDetailsBean.getUserId());
            bookIssueDetails.setQuantity(1);
            bookIssueDetails.setIssueDate(new Date());
            try {
                bookIssueDetails = bookIssueDetailsRepository.save(bookIssueDetails);
                bookRequestRepository.updateBookIssueId(bookIssueDetails.getId(),bookIssueDetails.getBookId(),bookIssueDetails.getUserId());
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE";
            }
        }else {
            BookRequest bookRequest = bookRequestRepository.findByIssueId(issuedBook.getId());
            if (bookRequest.getBookEntryId()!=null){
                BookIssueDetails bookIssueDetails = new BookIssueDetails();
                bookIssueDetails.setBookId(userBookIssueDetailsBean.getBookId());
                bookIssueDetails.setUserId(userBookIssueDetailsBean.getUserId());
                bookIssueDetails.setQuantity(1);
                bookIssueDetails.setIssueDate(new Date());
                try {
                    bookIssueDetails = bookIssueDetailsRepository.save(bookIssueDetails);
                    bookRequestRepository.updateBookIssueId(bookIssueDetails.getId(),bookIssueDetails.getBookId(),bookIssueDetails.getUserId());
                    return "SUCCESS";
                } catch (Exception e) {
                    return "FAILURE";
                }
            }
        }*/
    }

    @Override
    public String depositBookByUser(BookEntryDetailsBean bookEntryDetailsBean) {
        BookEntryDetails bookEntryDetails=new BookEntryDetails();
        BookStockDetails bookStockDetails = new BookStockDetails();
        Book book=bookRepository.findBookByBookId(new Long(bookEntryDetailsBean.getBook_id()));
        bookEntryDetails.setBook(book);
        if (bookEntryDetailsBean.getUser_id()!=0) {
            bookEntryDetails.setUserId(new Long(bookEntryDetailsBean.getUser_id()));
            bookEntryDetails.setQuantity(1);
        }else {
            bookEntryDetails.setQuantity(bookEntryDetailsBean.getQuantity());
        }
        bookEntryDetails.setLibraryId(book.getLibrary().getId());

        try {
            bookEntryDetails = bookEntryDetailsRepository.save(bookEntryDetails);
            bookStockDetails = bookStockDetailsRepository.findByBook(bookEntryDetails.getBook());
            int totalAllocateBooks = bookStockDetails.getNoOfAllocateBook();
            int totalAllocateBooksAfterDeposit = totalAllocateBooks - bookEntryDetails.getQuantity();
            int availableBooks = bookStockDetails.getNoOfAvailableBook();
            int totalAvailableBooksAfterDeposit = availableBooks + bookEntryDetails.getQuantity();
            bookStockDetails.setNoOfAllocateBook(totalAllocateBooksAfterDeposit);
            bookStockDetails.setNoOfAvailableBook(totalAvailableBooksAfterDeposit);
            bookStockDetailsRepository.save(bookStockDetails);
            if (bookEntryDetails.getUserId()!= null) {
                bookRequestRepository.updateBookEntryId(bookEntryDetails.getId(), bookEntryDetails.getBook().getId(), bookEntryDetails.getUserId());

            }
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "FAILURE";
        }
    }

    @Override
    public String addeLearningLink(ELearningLink eLearningLink, HttpServletRequest request) {
        try {
            eLearningLinkRepository.save(eLearningLink);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    @Override
    public List<ELearningLink> getAlleLinksByLibrary(Long id) {
        return eLearningLinkRepository.getAlleLinksByLibrarianId(id);
    }

    public String addCourse(CourseBean courseBean, HttpSession session, HttpServletRequest request) {
        try {
            Course course = new Course();
            course.setLibrary(courseBean.getLibrary());
            course.setCourseType(courseBean.getCourseType());
            course.setEducation(courseBean.getEducation());
            course.setStream(courseBean.getStream());
            course.setDescription(courseBean.getDescription());
            course.setUrl(courseBean.getUrl());
            courseRepository.save(course);
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    @Override
    public List<Course> getAllCourseByLibrary(Long id) {
        return courseRepository.getAllCourcesByLibrarianId(id);
    }

    public List getAllCourse() {
        List<Course> list = new ArrayList<>();
        for (Course course : courseRepository.findAll()) {
            if (course.isIsActive() == true) {
                list.add(course);
            }
        }
        return list;
    }

    @Override
    public List<Notification> getAllNotifications() {
        List<Notification> list = new ArrayList<>();
        for (Notification notification : notificationRepository.findAll()) {
            if (notification.isIsActive() == true && notification.isNoticeForLibrarian() == true && notification.getType().equals("notification")) {
                list.add(notification);
            }
        }
        return list;
    }

    @Override
    public String addNotification(NotificationBean notificationBean, HttpSession session, String notification) {
        try {
            Notification notification1 = new Notification();
            notification1.setLibrary(notificationBean.getLibrary());
            notification1.setSubject(notificationBean.getSubject());
            notification1.setSendTo(notificationBean.getSendTo());
            switch (notificationBean.getSendTo()) {
                case "Library Members":
                    notification1.setNoticeForUser(true);
                    notification1.setNoticeForLibrarian(false);
                    notification1.setNoticeForAdmin(false);
                    break;
                case "Librarian":
                    notification1.setNoticeForUser(false);
                    notification1.setNoticeForLibrarian(true);
                    notification1.setNoticeForAdmin(false);
                    break;
                case "Admin":
                    notification1.setNoticeForUser(false);
                    notification1.setNoticeForLibrarian(false);
                    notification1.setNoticeForAdmin(true);
                    break;
                case "All":
                    notification1.setNoticeForUser(true);
                    notification1.setNoticeForLibrarian(true);
                    notification1.setNoticeForAdmin(true);
                    break;
            }
            User user = (User) session.getAttribute("UserSession");
            notification1.setAddedDate(new Date());
            notification1.setNotificationDate(notificationBean.getNotificationDate());
            notification1.setNotificationTime(notificationBean.getNotificationTime());
            notification1.setDescription(notificationBean.getDescription());

            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            byte[] noticeFile = notificationBean.getAttachmentFile().getBytes();
            String attachmentName = notificationBean.getAttachmentFile().getOriginalFilename();

            String noticeFileFolder = path + "/noticeFolder/" + attachmentName;
            notification1.setAttachment("/noticeFolder/" + attachmentName);
            FileOutputStream perimageOutFile = new FileOutputStream(noticeFileFolder);
            perimageOutFile.write(noticeFile);
            notificationRepository.save(notification1);
            return "SUCCESS";
        } catch (IOException e) {
            return "FAILURE";
        }
    }


}
