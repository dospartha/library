package com.example.LibraryProject.Service.ServiceImpl;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Repository.*;
import com.example.LibraryProject.Service.HomeService;
import com.example.LibraryProject.Util.DCEncryptDecrypt;
import com.example.LibraryProject.Util.MailSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:25 PM
 **/
@Service
public class HomeServiceImpl implements HomeService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BlockDetailsRepository blockDetailsRepository;
    @Autowired
    private SubDivisionDetailsRepository subDivisionDetailsRepository;
    @Autowired
    private MunicipalityDetailsRepository municipalityDetailsRepository;
    @Autowired
    private PanchayetDetailsRepository panchayetDetailsRepository;
    @Autowired
    private LibraryRepository libraryRepository;


    @Override
    public String getUsernameFieldValue(String username) {
        return userRepository.getUsernameFieldValue(username);
    }

    public String addUser(User user, HttpSession session) {
        try {
            ServletContext cx = session.getServletContext();
            String path = cx.getRealPath("/resources");
            System.out.println(path);
            byte[] image = user.getImageUser().getBytes();
            String userImageFolder = path + "/user_Image/" + user.getName() + ".jpg";
            user.setImagePath("/user_Image/" + user.getName() + ".jpg");
            FileOutputStream perimageOutFile = new FileOutputStream(userImageFolder);
            perimageOutFile.write(image);
            DCEncryptDecrypt encryptDecrypt = new DCEncryptDecrypt();
            user.setPassword(encryptDecrypt.encrypt(user.getPassword()));
            userRepository.save(user);
            return "Success";
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public User validateUser(String firstField, String password, HttpServletRequest request) {
        DCEncryptDecrypt encryptDecrypt = new DCEncryptDecrypt();
        User user = userRepository.validateUser(firstField, encryptDecrypt.encrypt(password));
        if (user != null) {
            return user;
        } else {
            return null;
        }
    }

    public String forgetPassword(HttpSession session, String email) {
//        MailSend mailSend = new MailSend();
        User user = userRepository.findByEmail(email);
        if (user != null) {
            try {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("mail_heading", "Forget Password");
                hashMap.put("mail_content", "<p>Hello <b>" + user.getName() + "</b>,</p>"
                        + "<p><b>Email Id : </b>" + user.getEmail() + "</p>"
                        + "<p><b>Password : </b>" + user.getPassword() + "</p>"
                        + "<p>Please use this password to login.</p>");
                hashMap.put("mail_button_text1", "No button");

                MailSend.sendHtmlTemplateMail(session, hashMap, email, "Forget Password");
                return "SUCCESS";
            } catch (Exception e) {
                return "FAILURE";
            }
        } else {
            return "FAILURE";
        }
    }

    public List<SubDivisionDetails> getAllSdo() {
        return (List<SubDivisionDetails>) subDivisionDetailsRepository.findAll();
    }

    public List<BlockDetails> getAllBdo() {
        return (List<BlockDetails>) blockDetailsRepository.findAll();
    }

    public List<BlockDetails> getAllBdobySdo(long sdoId) {
        return blockDetailsRepository.findBlocksBySdoId(sdoId);
    }

    public List<MunicipalityDetails> getAllMunicipalitybySdo(long sdoId) {
        return municipalityDetailsRepository.findMunicipalitiesBySdoId(sdoId);
    }

    @Override
    public List<PanchayetDetails> getAllPanchayetByBlock(long bdoId) {
        return panchayetDetailsRepository.findByBlockDetails(bdoId);
    }

    public List<Library> getAllLibraryByPanchayet(long panchayetId) {
        return libraryRepository.findLibrariesByPanchayetId(panchayetId);
    }

    @Override
    public List<Library> getAllLibraryByMunicipalityId(long municipalityId) {
        return libraryRepository.findLibrariesByMunicipalityId(municipalityId);
    }

    public List<User> getAllUser() {
        return (List<User>) userRepository.findAll();
    }

    public List<User> getAllUserByLibrarian(Long id) {
        List<User> usersByLibrarian = userRepository.findAllByLibrarianId(id);
        return usersByLibrarian;
    }

    @Override
    public String sendToUserApprovalDetails(String name, String email, HttpSession session) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("mail_heading", "Approval Details");
            hashMap.put("mail_content", "<p>Hello <b>" + name + "</b>,</p>"
                    + "<p><b>Email Id : </b>" + email + "</p>"
                    + "<p>Your approval request is success. You can login now.<br> Thank you.</p>");
            hashMap.put("mail_button_text1", "No button");

            MailSend.sendHtmlTemplateMail(session, hashMap, email, "Approval Details");
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    @Override
    public String sendToUserNonApprovalDetails(String name, String email, HttpSession session) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("mail_heading", "Non Approval Details");
            hashMap.put("mail_content", "<p>Hello <b>" + name + "</b>,</p>"
                    + "<p><b>Email Id : </b>" + email + "</p>"
                    + "<p>Your approval request is rejected.</p>");
            hashMap.put("mail_button_text1", "No button");

            MailSend.sendHtmlTemplateMail(session, hashMap, email, "Non Approval Details");
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    @Override
    public String sendToUserBookRequestApprovalDetails(String name, String email, HttpSession session) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("mail_heading", "Book Request Approval Details");
            hashMap.put("mail_content", "<p>Hello <b>" + name + "</b>,</p>"
                    + "<p><b>Email Id : </b>" + email + "</p>"
                    + "<p>Your book request is approved.<br> Thank you.</p>");
            hashMap.put("mail_button_text1", "No button");

            MailSend.sendHtmlTemplateMail(session, hashMap, email, "Book Request Approval Details");
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }

    @Override
    public String sendToUserBookRequestDiscardDetails(String userName, String userEmail, HttpSession session) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("mail_heading", "Book Request Discard Details");
            hashMap.put("mail_content", "<p>Hello <b>" + userName + "</b>,</p>"
                    + "<p><b>Email Id : </b>" + userEmail + "</p>"
                    + "<p>Your book request is discarded.</p>");
            hashMap.put("mail_button_text1", "No button");

            MailSend.sendHtmlTemplateMail(session, hashMap, userEmail, "Book Request Discard Details");
            return "SUCCESS";
        } catch (Exception e) {
            return "FAILURE";
        }
    }
    /*public List<Library> getAllLibraryByMunicipleId(long bdomuniId) {
        return libraryRepository.getAllLibraryByMunicipleId(bdomuniId);
    }*/
}


