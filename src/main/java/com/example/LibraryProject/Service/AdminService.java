package com.example.LibraryProject.Service;

import com.example.LibraryProject.Entity.Admin;
import com.example.LibraryProject.Entity.Librarian;
import com.example.LibraryProject.Entity.Library;
import com.example.LibraryProject.Entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:50 PM
 **/
public interface AdminService {
    Admin validateAdmin(String firstField, String password, HttpServletRequest request, HttpSession session);
    String addAdmin(Admin admin, HttpServletRequest request, HttpSession session);
    String forgetPassword(HttpSession session, String email);
    List<Librarian> getAllLibrarian();
    String librarianApproval(Long id);
    String librarianNonApprove(Long id);
    List<Librarian> getAllApprovedLibrarian();
    List<Librarian> getAllNonApprovedLibrarian();
    List<User> getAllApprovedUsers();
    String addLibrary(Library library, HttpSession session);
    List<Library> getAllLibrary();
    List<Librarian> getAllNotAssignLibrarian();
}
