package com.example.LibraryProject.Service;

import com.example.LibraryProject.Entity.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Partha Sarothi
 * 9/6/2018,6:28 PM
 **/
public interface HomeService {
    String getUsernameFieldValue(String username);
    String addUser(User user, HttpSession session);
    User validateUser(String email, String password, HttpServletRequest request);
    String forgetPassword(HttpSession session, String email);
    List<SubDivisionDetails> getAllSdo();
    List<BlockDetails> getAllBdo();
    List<BlockDetails> getAllBdobySdo(long sdoId);
    List<MunicipalityDetails> getAllMunicipalitybySdo(long sdoId);
    List<PanchayetDetails> getAllPanchayetByBlock(long bdoId);
    List<Library> getAllLibraryByPanchayet(long panchayetId);
    List<Library> getAllLibraryByMunicipalityId(long municipalityId);
    List<User> getAllUser();
    List<User> getAllUserByLibrarian(Long id);
    String sendToUserApprovalDetails(String name, String email, HttpSession session);
    String sendToUserNonApprovalDetails(String name, String email, HttpSession session);

    String sendToUserBookRequestApprovalDetails(String name, String email, HttpSession session);

    String sendToUserBookRequestDiscardDetails(String userName, String userEmail, HttpSession session);
}
