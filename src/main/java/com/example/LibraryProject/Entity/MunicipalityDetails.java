package com.example.LibraryProject.Entity;

import javax.persistence.*;

@Entity
@Table(name = "municipality_details")
public class MunicipalityDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;
    @Column(name = "name")
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "subdivision_id")
    private SubDivisionDetails subDivisionDetails;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubDivisionDetails getSubDivisionDetails() {
        return subDivisionDetails;
    }

    public void setSubDivisionDetails(SubDivisionDetails subDivisionDetails) {
        this.subDivisionDetails = subDivisionDetails;
    }
}
