package com.example.LibraryProject.Entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "MaintenanceGrant")
public class MaintenanceGrant {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;
    @Column(name = "maintenanceType")
    private String maintenanceType;
    @Column(name = "libraryCode")
    private String libraryCode;
    @Column(name = "libraryName")
    private String libraryName;
    @Column(name = "librarianName")
    private String librarianName;
    @Column(name = "itemName")
    private String itemName;
    @Column(name = "totalCost")
    private String totalCost;
    @Column(name = "attachment")
    private String attachment;
    
    @Column(name = "status", columnDefinition = "tinyint(1) default 0")
    private int status;
    
    @Column(name = "addedDate")
    private Date addedDate;
    
    @Column(name = "approvedBy")
    private String approvedBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "approvedDate")
    private Date approvedDate;
    @Column(name = "approvedStatus", columnDefinition = "tinyint(1) default 0")
    private boolean approvedStatus = false;
    
    @Column(name = "rejectBy")
    private String rejectBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "rejectDate")
    private Date rejectDate;
    @Column(name = "rejectStatus", columnDefinition = "tinyint(1) default 0")
    private boolean rejectStatus = false;
    
    @Column(name = "isActive", columnDefinition="tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updateDate")
    private Date updateDate = new Date();

    public MaintenanceGrant() {
    }

    public MaintenanceGrant(Long Id, String maintenanceType, String libraryCode, String libraryName, String librarianName, String itemName, String totalCost, String attachment, int status, Date addedDate, String approvedBy, Date approvedDate, String rejectBy, Date rejectDate) {
        this.Id = Id;
        this.maintenanceType = maintenanceType;
        this.libraryCode = libraryCode;
        this.libraryName = libraryName;
        this.librarianName = librarianName;
        this.itemName = itemName;
        this.totalCost = totalCost;
        this.attachment = attachment;
        this.status = status;
        this.addedDate = addedDate;
        this.approvedBy = approvedBy;
        this.approvedDate = approvedDate;
        this.rejectBy = rejectBy;
        this.rejectDate = rejectDate;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getMaintenanceType() {
        return maintenanceType;
    }

    public void setMaintenanceType(String maintenanceType) {
        this.maintenanceType = maintenanceType;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibrarianName() {
        return librarianName;
    }

    public void setLibrarianName(String librarianName) {
        this.librarianName = librarianName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public boolean isApprovedStatus() {
        return approvedStatus;
    }

    public void setApprovedStatus(boolean approvedStatus) {
        this.approvedStatus = approvedStatus;
    }

    public String getRejectBy() {
        return rejectBy;
    }

    public void setRejectBy(String rejectBy) {
        this.rejectBy = rejectBy;
    }

    public Date getRejectDate() {
        return rejectDate;
    }

    public void setRejectDate(Date rejectDate) {
        this.rejectDate = rejectDate;
    }

    public boolean isRejectStatus() {
        return rejectStatus;
    }

    public void setRejectStatus(boolean rejectStatus) {
        this.rejectStatus = rejectStatus;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    
}
