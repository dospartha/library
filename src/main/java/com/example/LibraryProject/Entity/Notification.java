package com.example.LibraryProject.Entity;

import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "Notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "library_id")
    private Library library;
    @Column(name = "subject")
    private String subject;
    @Column(name = "notificationDate")
    private String notificationDate;
    @Column(name = "notificationTime")
    private String notificationTime;
    @Column(name = "description", columnDefinition = "TEXT")
    private String description;
    @Column(name = "attachment")
    private String attachment;
    @Column(name = "isActive", columnDefinition = "tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updateDate")
    private Date updateDate = new Date();
    @Column(name = "sendTo")
    private String sendTo;
    @Column(name = "forwardTo")
    private String forwardTo;
    @Column(name = "noticeForUser", columnDefinition = "tinyint(1) default 0")
    private boolean noticeForUser = false;
    @Column(name = "noticeForAdmin", columnDefinition = "tinyint(1) default 0")
    private boolean noticeForAdmin = false;
    @Column(name = "noticeForLibrarian", columnDefinition = "tinyint(1) default 0")
    private boolean noticeForLibrarian = false;
    @Column(name = "addedBy")
    private String addedBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "addedDate")
    private Date addedDate;
    @Column(name = "forwardBy")
    private String forwardBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "forwardDate")
    private Date forwardDate;
    @Column(name = "forwardStatus", columnDefinition = "tinyint(1) default 0")
    private boolean forwardStatus = false;
    @Column(name = "rejectBy")
    private String rejectBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "rejectDate")
    private Date rejectDate;
    @Column(name = "rejectStatus", columnDefinition = "tinyint(1) default 0")
    private boolean rejectStatus = false;
    @Column(name = "approvedBy")
    private String approvedBy;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "approvedDate")
    private Date approvedDate;
    @Column(name = "approvedStatus", columnDefinition = "tinyint(1) default 0")
    private boolean approvedStatus = false;
    @Column(name = "type")
    private String type;
    @Column(name = "status", columnDefinition = "tinyint(1) default 0")
    private int status;

    public Notification() {
    }

    public Notification(Long id, Library library, String subject, String notificationDate, String notificationTime, String description, String attachment, String sendTo, String forwardTo, String addedBy, Date addedDate, String forwardBy, Date forwardDate, String rejectBy, Date rejectDate, String approvedBy, Date approvedDate, String type, int status) {
        this.id = id;
        this.library = library;
        this.subject = subject;
        this.notificationDate = notificationDate;
        this.notificationTime = notificationTime;
        this.description = description;
        this.attachment = attachment;
        this.sendTo = sendTo;
        this.forwardTo = forwardTo;
        this.addedBy = addedBy;
        this.addedDate = addedDate;
        this.forwardBy = forwardBy;
        this.forwardDate = forwardDate;
        this.rejectBy = rejectBy;
        this.rejectDate = rejectDate;
        this.approvedBy = approvedBy;
        this.approvedDate = approvedDate;
        this.type = type;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public void setForwardTo(String forwardTo) {
        this.forwardTo = forwardTo;
    }

    public boolean isNoticeForUser() {
        return noticeForUser;
    }

    public void setNoticeForUser(boolean noticeForUser) {
        this.noticeForUser = noticeForUser;
    }

    public boolean isNoticeForAdmin() {
        return noticeForAdmin;
    }

    public void setNoticeForAdmin(boolean noticeForAdmin) {
        this.noticeForAdmin = noticeForAdmin;
    }

    public boolean isNoticeForLibrarian() {
        return noticeForLibrarian;
    }

    public void setNoticeForLibrarian(boolean noticeForLibrarian) {
        this.noticeForLibrarian = noticeForLibrarian;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getForwardBy() {
        return forwardBy;
    }

    public void setForwardBy(String forwardBy) {
        this.forwardBy = forwardBy;
    }

    public Date getForwardDate() {
        return forwardDate;
    }

    public void setForwardDate(Date forwardDate) {
        this.forwardDate = forwardDate;
    }

    public boolean isForwardStatus() {
        return forwardStatus;
    }

    public void setForwardStatus(boolean forwardStatus) {
        this.forwardStatus = forwardStatus;
    }

    public String getRejectBy() {
        return rejectBy;
    }

    public void setRejectBy(String rejectBy) {
        this.rejectBy = rejectBy;
    }

    public Date getRejectDate() {
        return rejectDate;
    }

    public void setRejectDate(Date rejectDate) {
        this.rejectDate = rejectDate;
    }

    public boolean isRejectStatus() {
        return rejectStatus;
    }

    public void setRejectStatus(boolean rejectStatus) {
        this.rejectStatus = rejectStatus;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public boolean isApprovedStatus() {
        return approvedStatus;
    }

    public void setApprovedStatus(boolean approvedStatus) {
        this.approvedStatus = approvedStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
