package com.example.LibraryProject.Entity;

import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "library_id")
    private Library library;
    @Column(name = "courseType")
    private String courseType;
    @Column(name = "education")
    private String education;
    @Column(name = "stream")
    private String stream;
    @Column(name = "description", columnDefinition = "TEXT")
    private String description;
    @Column(name = "url")
    private String url;
    @Column(name = "isActive", columnDefinition = "tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();

    public Course() {
    }

    public Course(Long Id, Library library, String courseType, String education, String stream, String description, String attachment, String url, String addedBy, Date addedDate, String forwardBy, Date forwardDate, String rejectBy, Date rejectDate, String approvedBy, Date approvedDate, String forwardTo, int status) {
        this.Id = Id;
        this.library = library;
        this.courseType = courseType;
        this.education = education;
        this.stream = stream;
        this.description = description;
        this.url = url;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

}