package com.example.LibraryProject.Entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Partha Sarothi
 * 9/13/2018,1:23 PM
 **/
@Entity
@Table(name = "book_request")
public class BookRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "book_id")
    private Long bookId;
    @Column(name = "library_id")
    private Long libraryId;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "requestDate")
    private Date requestDate = new Date();
    @Column(name = "approveDate")
    private Date approveDate;
    @Column(name = "requestStatus")
    private int requestStatus = 0;
    @Column(name = "bookEntry_id")
    private Long bookEntryId;
    @Column(name = "bookIssue_id")
    private Long bookIssueId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Long libraryId) {
        this.libraryId = libraryId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    public int getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(int requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Long getBookEntryId() {
        return bookEntryId;
    }

    public void setBookEntryId(Long bookEntryId) {
        this.bookEntryId = bookEntryId;
    }

    public Long getBookIssueId() {
        return bookIssueId;
    }

    public void setBookIssueId(Long bookIssueId) {
        this.bookIssueId = bookIssueId;
    }
}
