package com.example.LibraryProject.Entity;

import javax.persistence.*;

@Entity
@Table(name ="sub_division_details")
public class SubDivisionDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;
    @Column(name = "name")
    private String name;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
