package com.example.LibraryProject.Entity;

import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "book_stock_details")
public class BookStockDetails {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;
    @Column(name = "noOfTotalBook")
    private int noOfTotalBook = 0;
    @Column(name = "noOfAvailableBook")
    private int noOfAvailableBook = 0;
    @Column(name = "noOfAllocateBook")
    private int noOfAllocateBook = 0;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "book_id")
    public Book book;
    @Column(name = "isActive", columnDefinition="tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updateDate")
    private Date updateDate;

    public BookStockDetails() {

    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public int getNoOfTotalBook() {
        return noOfTotalBook;
    }

    public void setNoOfTotalBook(int noOfTotalBook) {
        this.noOfTotalBook = noOfTotalBook;
    }

    public int getNoOfAvailableBook() {
        return noOfAvailableBook;
    }

    public void setNoOfAvailableBook(int noOfAvailableBook) {
        this.noOfAvailableBook = noOfAvailableBook;
    }

    public int getNoOfAllocateBook() {
        return noOfAllocateBook;
    }

    public void setNoOfAllocateBook(int noOfAllocateBook) {
        this.noOfAllocateBook = noOfAllocateBook;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    
}
