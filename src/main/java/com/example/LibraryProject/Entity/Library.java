/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.LibraryProject.Entity;

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "Library")
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id", nullable = false)
    private Long Id;
    @Column(name = "libraryCode", unique = true)
    private String libraryCode;
    @Column(name = "libraryName")
    private String libraryName;
    @Column(name = "image_path")
    private String imagePath;
    @OneToOne
    @JoinColumn(name = "librarian_Id")
    private Librarian librarian;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="municipalityDetails_Id")
    private MunicipalityDetails municipalityDetails;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "panchayetDetails_Id")
    private PanchayetDetails panchayetDetails;
    @Column(name = "contactNumber", columnDefinition = "TEXT")
    private String contactNumber;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "latitude")
    private String latitude;
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "isActive", columnDefinition="tinyint(1) default 1")
    private boolean isActive = true;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "createDate")
    private Date createDate = new Date();
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "updateDate")
    private Date updateDate = new Date();
    @Transient
    private MultipartFile imageLibrary;
    
    public Library() {
    }

    public Library(Long Id, String libraryCode, String libraryName,String imagePath, Librarian librarian,MunicipalityDetails municipalityDetails,PanchayetDetails panchayetDetails, String contactNumber,String email,String latitude,String longitude,MultipartFile imageLibrary) {
        this.Id = Id;
        this.libraryCode = libraryCode;
        this.libraryName = libraryName;
        this.imagePath = imagePath;
        this.librarian = librarian;
        this.municipalityDetails = municipalityDetails;
        this.panchayetDetails = panchayetDetails;
        this.contactNumber = contactNumber;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.imageLibrary = imageLibrary;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public MunicipalityDetails getMunicipalityDetails() {
        return municipalityDetails;
    }

    public void setMunicipalityDetails(MunicipalityDetails municipalityDetails) {
        this.municipalityDetails = municipalityDetails;
    }

    public PanchayetDetails getPanchayetDetails() {
        return panchayetDetails;
    }

    public void setPanchayetDetails(PanchayetDetails panchayetDetails) {
        this.panchayetDetails = panchayetDetails;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Librarian getLibrarian() {
        return librarian;
    }

    public void setLibrarian(Librarian librarian) {
        this.librarian = librarian;
    }
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public MultipartFile getImageLibrary() {
        return imageLibrary;
    }

    public void setImageLibrary(MultipartFile imageLibrary) {
        this.imageLibrary = imageLibrary;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


}
