package com.example.LibraryProject.controller;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Service.UserService;
import com.example.LibraryProject.Util.AllDataBean;
import com.example.LibraryProject.dto.BookRequestBean;
import com.example.LibraryProject.dto.CourseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("User")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("dashboard")
    public String dashboard(HttpSession session,ModelMap modelMap) {
        User user = (User) session.getAttribute("UserSession");
        List<BookIssueDetails> issuedBooks = userService.getAllIssuedBooks(user.getId());
        modelMap.addAttribute("totalIssuedBooks",issuedBooks);
        return "userDashboard";
    }

    @RequestMapping("viewAllBooks")
    public String viewAllBooks() {
        return "viewAllBooks";
    }

    @RequestMapping("showAllBooks")
    @ResponseBody
    public AllDataBean showAllBooks(HttpSession session){
        /*View all books by library id*/
        User user = (User) session.getAttribute("UserSession");
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(userService.getAllBooksByLibraryId(user.getLibrary().getId()));
        return bookDataBean;

        /*View all Libraries all books*/
        /*AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(userService.getAllBooks());
        return bookDataBean;*/
    }

    @RequestMapping("searchBooksFromOtherLibrary")
    public String searchBooksFromOtherLibrary(){
        return "searchBooksFromOtherLibrary";
    }

    @RequestMapping(value = "searchOtherLibraryBooks",method = RequestMethod.POST)
    @ResponseBody
    public List<Book> getSearchBooksFromOtherLibrary(@RequestParam(value = "search") String search, HttpSession session){
        User user = (User) session.getAttribute("UserSession");
        /*AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(userService.getSearchBooksNotInLibrary(user.getLibrary().getId(), bookBean));
        return bookDataBean;*/
        List<Book> bookList = userService.getSearchBooksNotInLibrary(user.getLibrary().getId(), search);
        return  bookList;
    }

    @RequestMapping(value = "searchBooks",method = RequestMethod.POST)
    @ResponseBody
    public List<Book> showBooksBySearchParam(@RequestParam(value = "search") String search, HttpSession session){
        User user = (User) session.getAttribute("UserSession");
        List<Book> bookList = userService.getBooksBySearchParam(user.getLibrary().getId(),search);
        return bookList;
    }

    @RequestMapping(value = "requestBooks", method = RequestMethod.POST)
//    public String requestBooks(@RequestParam long bookId,@RequestParam long libraryId, HttpSession session){
    @ResponseBody
    public String requestBooks(@RequestBody BookRequestBean bookRequestBean, HttpSession session){
        User user = (User) session.getAttribute("UserSession");
        bookRequestBean.setUserId(user.getId());
        String status = userService.addBookRequest(bookRequestBean);
        return status;
    }

    @RequestMapping("viewAlleLink")
    public String viewAlleLink() {
        return "viewAlleLink";
    }

    @RequestMapping("viewAlleLearningEntry")
    @ResponseBody
    public AllDataBean viewAllELearningEntry() {
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(userService.getAllELearning());
        return bookDataBean;
    }

    @RequestMapping(value = "searcheLink",method = RequestMethod.GET)
    @ResponseBody
    public List<ELearningLink> showELinkBySearchParam(@RequestParam(value = "topic") String topic) {
        List<ELearningLink> eLearningLinkList = userService.getELearningBySearchParam(topic);
        return eLearningLinkList;
    }

    @RequestMapping("viewAllCources")
    public String viewAllCourses() {
        return "viewAllCourses";
    }

    @RequestMapping("viewAllCourcesDetails")
    @ResponseBody
    public AllDataBean viewAllCoursesDetails() {
        AllDataBean courseDataBean = new AllDataBean();
        courseDataBean.setData(userService.getAllCourses());
        return courseDataBean;
    }

    @RequestMapping(value = "searchCources",method = RequestMethod.POST)
    @ResponseBody
    public List<Course> showECoursesBySearchParam(@RequestBody CourseBean courseBean) {
        List<Course> coursesList = userService.getCoursesBySearchParam(courseBean);
        return coursesList;
    }
}

