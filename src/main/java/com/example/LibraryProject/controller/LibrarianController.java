package com.example.LibraryProject.controller;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Service.HomeService;
import com.example.LibraryProject.Service.LibrarianService;
import com.example.LibraryProject.Util.AllDataBean;
import com.example.LibraryProject.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping(value = "Librarian")
public class LibrarianController {
    @Autowired
    private LibrarianService librarianService;
    @Autowired
    private HomeService homeService;

    @RequestMapping({"/", "/signin"})
    public String librarianHome(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("LibrarianSession") != null) {
            return "redirect:/Librarian/dashboard";
        } else {
            return "signin";
        }
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request, HttpSession session, RedirectAttributes redir) {
        ModelAndView mav = new ModelAndView();
        String firstField = request.getParameter("firstField").trim();
        String password = request.getParameter("password").trim();
        Librarian librarian = librarianService.validateUser(firstField, password);
        if (librarian != null) {
            if (librarian.getStatus() == 1) {
                session.invalidate();
                HttpSession newSession = request.getSession(true);
                newSession.setAttribute("LibrarianSession", librarian);
//                homeService.getAllUserByLibrarian(librarian.getId());
                mav.setViewName("redirect:/Librarian/dashboard");
            } else {
                redir.addFlashAttribute("status", "Signin");
                mav.setViewName("redirect:/Librarian/");
            }
        } else {
            redir.addFlashAttribute("status", "Email Id or Password mismatch");
            mav.setViewName("redirect:/Librarian/");
        }
        return mav;
    }

    @RequestMapping("/dashboard/logout")
    public ModelAndView logout(HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        session.invalidate();
        mav.setViewName("redirect:/Librarian/");
        return mav;
    }

    @RequestMapping("/signup")
    public String signup() {
        return "signup";
    }

    @RequestMapping(value = "signup", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("signupForm") Librarian librarian, HttpServletRequest request, HttpSession session, RedirectAttributes redir) {
        ModelAndView mav = new ModelAndView();
        String status = librarianService.addLibrarian(librarian, request, session);
        if (status.equals("Success")) {
            redir.addFlashAttribute("status", "Signup");
            mav.setViewName("redirect:/Librarian/");
        } else {
            if(status.contains("ConstraintViolationException")) {
                status = "Duplicate Entry Found";
                redir.addFlashAttribute("status", status);
            }else {
                redir.addFlashAttribute("status", status);
            }
            mav.setViewName("redirect:/Librarian/signup");
        }
        return mav;
    }

    @RequestMapping("/forgetpassword")
    public String forgetpassword() {
        return "forgetpassword";
    }

    @RequestMapping(value = "/forgetpassword", method = RequestMethod.POST)
    public String forgetPassword(RedirectAttributes redir, HttpServletRequest request, HttpSession session) {
        String status = librarianService.forgetPassword(session, request.getParameter("email").trim());
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Librarian/forgetpassword";
    }

    @RequestMapping("dashboard")
    public String dashboard() {
        return "librarianDashboard";
    }

    @RequestMapping("allUsers")
    public String allUsers() {
        return "allUsers";
    }

    @RequestMapping("allApprovedUsers")
    public String allApprovedUsers() {
        return "allApprovedUsers";
    }

    @RequestMapping("allNonApprovedUsers")
    public String allNonApprovedUsers() {
        return "allNon-ApprovedUsers";
    }

    @RequestMapping(value = "getAllUsers", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean AllUserList(HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        AllDataBean userDataBean = new AllDataBean();
        userDataBean.setData(homeService.getAllUserByLibrarian(librarian.getId()));
        return userDataBean;
    }

    @RequestMapping(value = "userApproval", method = RequestMethod.POST)
    @ResponseBody
    public String userApproval(@RequestBody User user,HttpSession session) {
        librarianService.userApproval(user.getId());
        String status = homeService.sendToUserApprovalDetails(user.getName(),user.getEmail(),session);
        return status;
    }

    @RequestMapping(value = "userNonApprove", method = RequestMethod.POST)
    @ResponseBody
    public String userNonApprove(@RequestBody User user,HttpSession session) {
        librarianService.userNonApprove(user.getId());
        String status = homeService.sendToUserNonApprovalDetails(user.getName(),user.getEmail(),session);
        return status;
    }

    @RequestMapping(value = "getAllApprovedUsers", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllApprovedUsers(HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        AllDataBean userDataBean = new AllDataBean();
        userDataBean.setData(librarianService.getAllApprovedUsers(librarian.getId()));
        return userDataBean;
    }

    @RequestMapping(value = "getAllNonApprovedUsers", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllNonApprovedUsers(HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        AllDataBean userDataBean = new AllDataBean();
        userDataBean.setData(librarianService.getAllNonApprovedUsers(librarian.getId()));
        return userDataBean;
    }

    @RequestMapping("addNewBook")
    public String addNewBook(ModelMap modelMap, HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        modelMap.addAttribute("library", librarianService.getLibraryByLibrarian(librarian.getId()));
        modelMap.addAttribute("allSubject", librarianService.getAllSubject());
        return "addNewBook";
    }

    @RequestMapping(value = "addBooks", method = RequestMethod.POST)
    public String addBooks(@ModelAttribute("addBooks") Book book, RedirectAttributes redir) {
        String status = librarianService.addBooks(book);
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Librarian/addNewBook";
    }

    @RequestMapping(value = "getAllBooks", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllBooks(HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(librarianService.getAllBooksByLibrarianId(librarian.getId()));
        return bookDataBean;
    }

    @RequestMapping("entryBookDetails")
    public String entryBookDetails(ModelMap modelMap, HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        modelMap.addAttribute("AllBooks", librarianService.getAllBooksByLibrarianId(librarian.getId()));
        return "entryBookDetails";
    }

    @RequestMapping(value = "entryBooks", method = RequestMethod.POST)
    public String entryBooks(@ModelAttribute("entryBooks") BookEntryDetailsBean bookEntryDetailsBean, RedirectAttributes redir,HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        Library library = librarianService.getLibraryByLibrarian(librarian.getId());
        bookEntryDetailsBean.setLibraryId(library.getId());
        String entryStatus = librarianService.entryBooksDetails(bookEntryDetailsBean);
        if (entryStatus.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        }else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Librarian/entryBookDetails";
    }

    @RequestMapping(value = "getTotalBooksEntry", method = RequestMethod.GET)
    @ResponseBody
    public AllDataBean getTotalBooksEntry() {
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(librarianService.getTotalBooksEntry());
        return bookDataBean;
    }

    @RequestMapping("viewAllBookRequest")
    public String viewAllBookRequest() {
        return "viewAllBookRequest";
    }

    @RequestMapping(value = "showAllBookRequest", method = RequestMethod.GET)
    @ResponseBody
    public AllDataBean viewAllBookRequest(HttpSession session){
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        Library library = librarianService.getLibraryByLibrarian(librarian.getId());
        List<UserBookRequestDetailsBean> list = librarianService.viewAllBookRequest(library.getId());
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(list);
        return bookDataBean;
    }

    @RequestMapping(value = "approveUserBookRequest", method = RequestMethod.POST)
    @ResponseBody
    public String approveUserBookRequest(@RequestBody UserBookIssueDetailsBean userBookIssueDetailsBean,HttpSession session){
        String status = "";
        status = librarianService.approveBookRequest(userBookIssueDetailsBean.getBookId());
        if (status.equals("SUCCESS")) {
            status = homeService.sendToUserBookRequestApprovalDetails(userBookIssueDetailsBean.getUserName(), userBookIssueDetailsBean.getUserEmail(), session);
            if (status.equals("SUCCESS")){
                return status;
            }else {
                return "Your request is approved. But something problem in your Email Id.Please check your email id and provide a valid email id. Thank you.";
            }
        }else {
            return status;
        }
    }

    @RequestMapping(value = "discardUserBookRequest", method = RequestMethod.POST)
    @ResponseBody
    public String discardUserBookRequest(@RequestBody UserBookIssueDetailsBean userBookIssueDetailsBean,HttpSession session){
        String status = "";
        status = librarianService.discardBookRequest(userBookIssueDetailsBean.getBookId());
        if (status.equals("SUCCESS")) {
            status = homeService.sendToUserBookRequestDiscardDetails(userBookIssueDetailsBean.getUserName(), userBookIssueDetailsBean.getUserEmail(), session);
            if (status.equals("SUCCESS")){
                return status;
            }else {
                return "Your request is discarded. But something problem in your Email Id.Please check your email id and provide a valid email id. Thank you.";
            }
        }else {
            return status;
        }
    }

    @RequestMapping("viewAllApprovedBookRequest")
    public String viewAllApprovedBookRequest() {
        return "viewAllApprovedBookRequest";
    }

    @RequestMapping(value = "showAllApprovedBookRequest", method = RequestMethod.GET)
    @ResponseBody
    public AllDataBean showAllApprovedBookRequest(HttpSession session){
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        Library library = librarianService.getLibraryByLibrarian(librarian.getId());
        List<UserBookRequestDetailsBean> list = librarianService.showAllApprovedBookRequest(library.getId());
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(list);
        return bookDataBean;
    }

    @RequestMapping(value = "issueBookToUser", method = RequestMethod.POST)
    @ResponseBody
    public String issueBookToUser(@RequestBody UserBookIssueDetailsBean userBookIssueDetailsBean,HttpSession session){
        String status = "";
        status = librarianService.addBookIssueDetails(userBookIssueDetailsBean);
        if (status.equals("SUCCESS")) {
            status = homeService.sendToUserBookRequestApprovalDetails(userBookIssueDetailsBean.getUserName(), userBookIssueDetailsBean.getUserEmail(), session);
            if (status.equals("SUCCESS")){
                return status;
            }else {
                return "Your book is issued. But something problem in your Email Id.Please check your email id or internet connection. Thank you.";
            }
        }else {
            return status;
        }
    }

    @RequestMapping(value = "depositBookByUser", method = RequestMethod.POST)
    @ResponseBody
    public String depositBookByUser(@RequestBody BookEntryDetailsBean bookEntryDetailsBean){

        return librarianService.depositBookByUser(bookEntryDetailsBean);

    }

    @RequestMapping("addNeweLink")
    public String addeLink(ModelMap modelMap, HttpSession session){
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        modelMap.addAttribute("library", librarianService.getLibraryByLibrarian(librarian.getId()));
        modelMap.addAttribute("allSubject", librarianService.getAllSubject());
        return "addNeweLink";
    }
    @RequestMapping(value = "addeLink", method = RequestMethod.POST)
    public String addeLink(@ModelAttribute("addeLink") ELearningLink eLearningLink, RedirectAttributes redir, HttpServletRequest request) {
        String status = librarianService.addeLearningLink(eLearningLink, request);
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Librarian/addNeweLink";
    }
    @RequestMapping(value = "getAlleLinks", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAlleLinks(HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(librarianService.getAlleLinksByLibrary(librarian.getId()));
        return bookDataBean;
    }

    @RequestMapping("addNewCourse")
    public String addNewCourse(HttpSession session, ModelMap modelMap) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        modelMap.addAttribute("library", librarianService.getLibraryByLibrarian(librarian.getId()));
        return "addNewCourse";
    }

    @RequestMapping(value = "addNewCourse", method = RequestMethod.POST)
    public String addCourse(@ModelAttribute("addCourse") CourseBean courseBean, HttpSession session, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String status = librarianService.addCourse(courseBean, session, request);
        if (status.equals("SUCCESS")) {
            redirectAttributes.addFlashAttribute("status", "SUCCESS");
        } else {
            redirectAttributes.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Librarian/addNewCourse";
    }

    @RequestMapping(value = "getAllCourse", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllCourse(HttpSession session) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        AllDataBean dataBean = new AllDataBean();
        dataBean.setData(librarianService.getAllCourseByLibrary(librarian.getId()));
        return dataBean;
    }
    @RequestMapping("addNotification")
    public String addNotificationforUsers(HttpSession session, ModelMap modelMap) {
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        modelMap.addAttribute("library", librarianService.getLibraryByLibrarian(librarian.getId()));
        return "addNotification";
    }

    @RequestMapping(value = "addNotification", method = RequestMethod.POST)
    public String addNotification(@ModelAttribute("addNotification") NotificationBean notificationBean, RedirectAttributes redir, HttpSession session, ModelMap modelMap, HttpServletRequest request) {
        String status = librarianService.addNotification(notificationBean, session, "notification");
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Librarian/addNotification";
    }

    @RequestMapping(value = "getAllNotifications", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllNotifications() {
        AllDataBean notificationDataBean = new AllDataBean();
        notificationDataBean.setData(librarianService.getAllNotifications());
        return notificationDataBean;
    }
    @RequestMapping(value = "maintenance")
    public String maintenance() {
        return "maintenance";
    }
}
