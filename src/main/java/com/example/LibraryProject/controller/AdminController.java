package com.example.LibraryProject.controller;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Repository.BlockDetailsRepository;
import com.example.LibraryProject.Repository.MunicipalityDetailsRepository;
import com.example.LibraryProject.Service.AdminService;
import com.example.LibraryProject.Service.HomeService;
import com.example.LibraryProject.Service.UserService;
import com.example.LibraryProject.Util.AllDataBean;
import com.example.LibraryProject.dto.CourseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "Admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private HomeService homeService;
    @Autowired
    private BlockDetailsRepository blockDetailsRepository;
    @Autowired
    private MunicipalityDetailsRepository municipalityDetailsRepository;
    @Autowired
    private UserService userService;

    @RequestMapping({"/", "/signin"})
    public String adminHome(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("AdminSession") != null) {
            return "redirect:/Admin/dashboard";
        } else {
            return "signin";
        }
    }

    @RequestMapping("/dashboard/logout")
    public ModelAndView logout(HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        session.invalidate();
        mav.setViewName("redirect:/Admin/");
        return mav;
    }
    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request, HttpSession session, RedirectAttributes redir) {
        ModelAndView mav = new ModelAndView();
        String firstField = request.getParameter("firstField").trim();
        String password = request.getParameter("password").trim();
        Admin admin = adminService.validateAdmin(firstField, password, request, session);
        if (admin != null) {
            mav.setViewName("redirect:/Admin/dashboard");
        } else {
            redir.addFlashAttribute("status", "Email Id or Password mismatch");
            mav.setViewName("redirect:/Admin/");
        }
//        System.out.println("........." + request.getRequestURI());
        return mav;
    }

    @RequestMapping("signup")
    public String signup() {
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("signupForm") Admin admin, HttpServletRequest request, HttpSession session, RedirectAttributes redir) {
        ModelAndView mav = new ModelAndView();
        String status = adminService.addAdmin(admin, request, session);
        if (status.equals("Success")) {
            redir.addFlashAttribute("status", "Signup");
            mav.setViewName("redirect:/Admin/");
        } else {
            if(status.contains("ConstraintViolationException")) {
                status = "Duplicate Entry Found";
                redir.addFlashAttribute("status", status);
            }else {
                redir.addFlashAttribute("status", status);
            }
            mav.setViewName("redirect:/Admin/signup");
        }
        return mav;
    }

    @RequestMapping("/forgetpassword")
    public String forgetpassword() {
        return "forgetpassword";
    }

    @RequestMapping(value = "/forgetpassword", method = RequestMethod.POST)
    public String forgetPassword(RedirectAttributes redir, HttpServletRequest request, HttpSession session) {
        String status = adminService.forgetPassword(session, request.getParameter("email").trim());
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        String rootPath = request.getContextPath();
        System.out.println("rootPath = " + rootPath);
        return "redirect:/Admin/forgetpassword";
    }

    @RequestMapping("dashboard")
    public String dashboard() {
        return "adminDashboard";
    }

    @RequestMapping("allLibrarian")
    public String allLibrarian() {
        return "allLibrarian";
    }

    @RequestMapping("allApprovedLibrarian")
    public String allApprovedLibrarian() {
        return "allApprovedLibrarian";
    }

    @RequestMapping("allNonApprovedLibrarian")
    public String allNonApprovedLibrarian() {
        return "allNon-ApprovedLibrarian";
    }

    @RequestMapping(value = "getAllLibrarian", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean AllLibrarianList() {
        AllDataBean librarianDataBean = new AllDataBean();
        librarianDataBean.setData(adminService.getAllLibrarian());
        return librarianDataBean;
    }

    @RequestMapping(value = "librarianApproval", method = RequestMethod.POST)
    @ResponseBody
    public String librarianApproval(@RequestBody Librarian librarian) {
        String status = adminService.librarianApproval(librarian.getId());
        return status;
    }

    @RequestMapping(value = "librarianNonApprove", method = RequestMethod.POST)
    @ResponseBody
    public String librarianNonApprove(@RequestBody Librarian librarian) {
        String status = adminService.librarianNonApprove(librarian.getId());
        return status;
    }

    @RequestMapping(value = "getAllApprovedLibrarian", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllApprovedLibrarian() {
        AllDataBean librarianDataBean = new AllDataBean();
        librarianDataBean.setData(adminService.getAllApprovedLibrarian());
        return librarianDataBean;
    }

    @RequestMapping(value = "getAllNonApprovedLibrarian", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllNonApprovedLibrarian() {
        AllDataBean librarianDataBean = new AllDataBean();
        librarianDataBean.setData(adminService.getAllNonApprovedLibrarian());
        return librarianDataBean;
    }


    @RequestMapping("allApprovedUsers")
    public String allApprovedUsers() {
        return "showAllUsers";
    }

    @RequestMapping(value = "getAllApprovedUsers", method = RequestMethod.GET)
    public @ResponseBody
    AllDataBean getAllApprovedUsers() {
        AllDataBean userDataBean = new AllDataBean();
        userDataBean.setData(adminService.getAllApprovedUsers());
        return userDataBean;
    }

    @RequestMapping("addNewLibrary")
    public String addNewLibrary(ModelMap modelMap) {
        List<Librarian> librarians = adminService.getAllNotAssignLibrarian();
        modelMap.addAttribute("allSDO", homeService.getAllSdo());
        if (!librarians.isEmpty()) {
            modelMap.addAttribute("allLibrarian", librarians);
        }
        return "addNewLibrary";
    }

    @RequestMapping(value = "/getAllMunicipalityDataBySDO", method = RequestMethod.GET)
    @ResponseBody
    public List<MunicipalityDetails> getAllMunicipalityBySDO(@RequestParam(required = true) long sdoId) {
        return homeService.getAllMunicipalitybySdo(sdoId);
    }

    @RequestMapping(value = "/getAllBdoDataBySDO", method = RequestMethod.GET)
    @ResponseBody
    public List<BlockDetails> getAllBdoDataBySDO(@RequestParam(required = true) long sdoId) {
        return homeService.getAllBdobySdo(sdoId);
    }

    @RequestMapping(value = "/getAllPanchayetByBlock", method = RequestMethod.GET)
    @ResponseBody
    public List<PanchayetDetails> getAllPanchayetByBlock(@RequestParam(required = true) long bdoId) {
        System.out.println(bdoId);
        return homeService.getAllPanchayetByBlock(bdoId);
    }

    @RequestMapping(value = "addLibrary", method = RequestMethod.POST)
    public String addLibraryDetails(@ModelAttribute("addLibrary") Library library, HttpSession session, RedirectAttributes redirectAttributes) {
        /*String blockMuniId = request.getParameter("blockMuniId");
        String bdomuniId = request.getParameter("bdomuniId");
        if (blockMuniId.equals("1")){
            BlockDetails details=blockDetailsRepository.findBlockDetailsById(Long.parseLong(bdomuniId));
            library.setBlockDetails(details);
        }else {
            MunicipalityDetails municipalityDetails = municipalityDetailsRepository.findMunicipalityDetailsById(Long.parseLong(bdomuniId));
            library.setMunicipalityDetails(municipalityDetails);
        }*/
        String status = adminService.addLibrary(library, session);
        if (status.equals("SUCCESS")) {
            redirectAttributes.addFlashAttribute("status", "SUCCESS");
        } else {
            redirectAttributes.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/Admin/addNewLibrary";
    }

    @RequestMapping(value = "getAllLibrary", method = RequestMethod.GET)
    @ResponseBody
    public AllDataBean getAllLibrary() {
        AllDataBean libraryDataBean = new AllDataBean();
        libraryDataBean.setData(adminService.getAllLibrary());
        return libraryDataBean;
    }

    @RequestMapping("adminViewAlleLink")
    public String viewAlleLink() {
        return "adminViewAlleLink";
    }

    @RequestMapping("viewAlleLearningEntry")
    @ResponseBody
    public AllDataBean viewAlleLearningEntry() {
        AllDataBean bookDataBean = new AllDataBean();
        bookDataBean.setData(userService.getAllELearning());
        return bookDataBean;
    }

    @RequestMapping(value = "searcheLink",method = RequestMethod.GET)
    @ResponseBody
    public List<ELearningLink> showeLinkBySearchParam(@RequestParam(value = "topic") String topic) {
        List<ELearningLink> eLearningLinkList = userService.getELearningBySearchParam(topic);
        return eLearningLinkList;
    }

    @RequestMapping("adminViewAllCourses")
    public String viewAllCources() {
        return "adminViewAllCourses";
    }

    @RequestMapping("viewAllCourcesDetails")
    @ResponseBody
    public AllDataBean viewAllCourcesDetails() {
        AllDataBean courceDataBean = new AllDataBean();
        courceDataBean.setData(userService.getAllCourses());
        return courceDataBean;
    }

    @RequestMapping(value = "searchCources",method = RequestMethod.POST)
    @ResponseBody
    public List<Course> showeCourcesBySearchParam(@RequestBody CourseBean courseBean) {
        List<Course> courcesList = userService.getCoursesBySearchParam(courseBean);
        return courcesList;
    }
}
