package com.example.LibraryProject.controller;

import com.example.LibraryProject.Entity.*;
import com.example.LibraryProject.Service.HomeService;
import com.example.LibraryProject.Util.SmsSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Partha sarothi
 * 8/23/2018,12:15 PM
 **/

@Controller
public class HomeController {
    @Autowired
    HomeService homeService;
    @RequestMapping({"/","/signin","User/"})
    public String userHome(HttpServletRequest request){
        HttpSession session = request.getSession();
        if (session.getAttribute("UserSession")!=null){
            return "redirect:/User/dashboard";
        }else {
            return "signin";
        }
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request, HttpSession session, RedirectAttributes redir){
        ModelAndView mav = new ModelAndView();
        String firstField = request.getParameter("firstField").trim();
        String password = request.getParameter("password").trim();
        User user = homeService.validateUser(firstField,password,request);
        if (user!=null){
            if (user.getStatus()== 1){
                session.invalidate();
                HttpSession newSession = request.getSession(true);
                newSession.setAttribute("UserSession", user);
                mav.setViewName("redirect:/User/dashboard");
            }else {
                redir.addFlashAttribute("status", "Signin");
                mav.setViewName("redirect:/");
            }
        }else {
            redir.addFlashAttribute("status", "Email Id or Password mismatch");
            mav.setViewName("redirect:/");
        }
        return mav;
    }

    @RequestMapping("signup")
    public String signup(ModelMap modelMap) {
        modelMap.addAttribute("allSDO", homeService.getAllSdo());
        modelMap.addAttribute("allBDO", homeService.getAllBdo());
        return "signup";
    }

    @RequestMapping(value = "/checkUsernameInput", method = RequestMethod.GET)
    @ResponseBody
    public String checkUsernameInput(@RequestParam String username) {
        return homeService.getUsernameFieldValue(username);
    }

    @RequestMapping(value = "/getAllBdoDataBySDO", method = RequestMethod.GET)
    @ResponseBody
    public List<BlockDetails> getAllBdoDataBySDO(@RequestParam(required = true) long sdoId) {
        return homeService.getAllBdobySdo(sdoId);
    }

    @RequestMapping(value = "/getAllMunicipalityDataBySDO", method = RequestMethod.GET)
    @ResponseBody
    public List<MunicipalityDetails> getAllMunicipalityBySDO(@RequestParam(required = true) long sdoId) {
        return homeService.getAllMunicipalitybySdo(sdoId);
    }

    @RequestMapping(value = "/getAllPanchayetByBlock", method = RequestMethod.GET)
    @ResponseBody
    public List<PanchayetDetails> getAllPanchayetByBlock(@RequestParam(required = true) long bdoId) {
        System.out.println(bdoId);
        return homeService.getAllPanchayetByBlock(bdoId);
    }

    @RequestMapping(value = "/getAllLibraryByMunicipalityId", method = RequestMethod.GET)
    @ResponseBody
    public List<Library> getAllLibraryByMunicipalityId(@RequestParam(required = true) long municipalityId) {
        return homeService.getAllLibraryByMunicipalityId(municipalityId);
    }

    @RequestMapping(value = "/getAllLibraryByPanchayet", method = RequestMethod.GET)
    @ResponseBody
    public List<Library> getAllLibraryByPanchayet(@RequestParam(required = true) long panchayetId) {
        return homeService.getAllLibraryByPanchayet(panchayetId);
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("signupForm") User user, HttpSession session, RedirectAttributes redir){
        ModelAndView mav = new ModelAndView();
        String status = homeService.addUser(user,session);
        if (status.equals("Success")) {
            if (user.getContactNo() != null) {
                String msg = "Hello " + user.getName() + " "  + ", Welcome to Library Management System"
                        + "You have successfully registered.Please wait for approval";
                try {
                    SmsSend.sendSms(user.getContactNo(), msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            redir.addFlashAttribute("status","Signup");
            mav.setViewName("redirect:signin");
        } else {
            if(status.contains("ConstraintViolationException")) {
                status = "Duplicate Entry Found";
                redir.addFlashAttribute("status", status);
            }else {
                redir.addFlashAttribute("status", status);
            }
            mav.setViewName("redirect:/signup");
        }
        return mav;
    }

    @RequestMapping("/forgetpassword")
    public String forgetpassword() {
        return "forgetpassword";
    }

    @RequestMapping(value = "/forgetpassword", method = RequestMethod.POST)
    public String forgetPassword(RedirectAttributes redir, HttpServletRequest request, HttpSession session) {
        String status = homeService.forgetPassword(session, request.getParameter("email").trim());
        if (status.equals("SUCCESS")) {
            redir.addFlashAttribute("status", "SUCCESS");
        } else {
            redir.addFlashAttribute("status", "FAILURE");
        }
        return "redirect:/forgetpassword";
    }

    @RequestMapping("/dashboard/logout")
    public ModelAndView logout(HttpSession session, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        session.invalidate();
        mav.setViewName("redirect:/");
        return mav;
    }
}
