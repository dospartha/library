package com.example.LibraryProject.Interceptor;

import com.example.LibraryProject.Entity.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class UserCustomInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("UserSession");
        String context = request.getServletContext().getContextPath();
        String pathWords[] = request.getRequestURI().split("/");
        String pathUser = pathWords[2];
        if (user == null) {
            response.sendRedirect(context + "/");
            return false;
        } else {
            if (!pathUser.equals("User")) {
                response.sendRedirect(context + "/");
                return false;
            }
            return true;
        }
    }

}
