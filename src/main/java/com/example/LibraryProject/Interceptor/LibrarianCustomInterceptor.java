package com.example.LibraryProject.Interceptor;

import com.example.LibraryProject.Entity.Librarian;
import com.example.LibraryProject.Entity.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class LibrarianCustomInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Librarian librarian = (Librarian) session.getAttribute("LibrarianSession");
        
        String context = request.getServletContext().getContextPath();
        
        String pathWords[] = request.getRequestURI().split("/");
        String pathUser = pathWords[2];
        
        if (librarian == null) {
            response.sendRedirect(context + "/Librarian/");
            return false;
        } else {
            if (!pathUser.equals("Librarian")) {
                response.sendRedirect(context + "/");
                return false;
            }
            return true;
        }
    }

}
