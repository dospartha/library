package com.example.LibraryProject.Interceptor;

import com.example.LibraryProject.Entity.Admin;
import com.example.LibraryProject.Entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class AdminCustomInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Admin admin = (Admin) session.getAttribute("AdminSession");
        String sessionValue = session.getId();
        String context = request.getServletContext().getContextPath();

        String pathWords[] = request.getRequestURI().split("/");
        String pathAdmin = pathWords[2];
//        String lastValue = pathWords[3];

        if (admin == null) {
            response.sendRedirect(context + "/Admin/");
            return false;
        } else {
            if (!pathAdmin.equals("Admin")) {
                response.sendRedirect(context + "/");
                return false;
            }
            return true;
        }
    }
}

