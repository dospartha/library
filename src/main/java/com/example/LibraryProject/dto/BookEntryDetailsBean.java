package com.example.LibraryProject.dto;

import com.example.LibraryProject.Entity.Book;
import com.example.LibraryProject.Entity.User;

/**
 * @author Partha Sarothi
 * 10/3/2018,1:50 PM
 **/
public class BookEntryDetailsBean {
    private Book book;
    private User user;
    private int quantity;

    private int book_id;
    private int user_id;
    private Long libraryId;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Long getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Long libraryId) {
        this.libraryId = libraryId;
    }
}
