package com.example.LibraryProject.dto;

import com.example.LibraryProject.Entity.Library;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Partha Sarothi
 * 9/28/2018,6:12 PM
 **/
public class NotificationBean {
    private Long Id;
    private Library library;
    private String subject;
    private String sendTo;
    private String forwardBy;
    private String notificationDate;
    private String notificationTime;
    private String description;
    private String attachmentPath;
    private MultipartFile attachmentFile;

    private String startDate;
    private String endDate;

    private String type;

    public NotificationBean() {
    }

    public NotificationBean(Long Id, Library library, String subject, String sendTo, String forwardBy, String notificationDate, String notificationTime, String description, String attachmentPath, MultipartFile attachmentFile, String startDate, String endDate, String type) {
        this.Id = Id;
        this.library = library;
        this.subject = subject;
        this.sendTo = sendTo;
        this.forwardBy = forwardBy;
        this.notificationDate = notificationDate;
        this.notificationTime = notificationTime;
        this.description = description;
        this.attachmentPath = attachmentPath;
        this.attachmentFile = attachmentFile;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getForwardBy() {
        return forwardBy;
    }

    public void setForwardBy(String forwardBy) {
        this.forwardBy = forwardBy;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public MultipartFile getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(MultipartFile attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
