package com.example.LibraryProject.dto;

/**
 * @author Partha Sarothi
 * 10/1/2018,3:46 PM
 **/
public class BookBean {
    private Long Id;
    private String bookName;
    private String publisherName;
    private String authorName;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
