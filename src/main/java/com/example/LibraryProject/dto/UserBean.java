package com.example.LibraryProject.dto;

import com.example.LibraryProject.Entity.Library;
import com.example.LibraryProject.Entity.User;

/**
 * @author Partha Sarothi
 * 9/20/2018,4:51 PM
 **/
public class UserBean {
    private Long userId;
    private String userName;
    private String userEmail;
    private Long libraryId;
    private String libraryEmail;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Long getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Long libraryId) {
        this.libraryId = libraryId;
    }

    public String getLibraryEmail() {
        return libraryEmail;
    }

    public void setLibraryEmail(String libraryEmail) {
        this.libraryEmail = libraryEmail;
    }
}
