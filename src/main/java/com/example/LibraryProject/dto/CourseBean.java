package com.example.LibraryProject.dto;


import com.example.LibraryProject.Entity.Library;

/**
 * @author Partha Sarothi
 * 9/15/2018,5:53 PM
 **/


public class CourseBean {

    private Long Id;
    private Library library;
    private String courseType;
    private String education;
    private String stream;
    private String description;
    private String url;
    private String createDate;


    public CourseBean() {
    }

    public CourseBean(Long Id, Library library, String courseType, String education, String stream, String description, String url, String createDate) {
        this.Id = Id;
        this.library = library;
        this.courseType = courseType;
        this.education = education;
        this.stream = stream;
        this.description = description;
        this.url = url;
        this.createDate = createDate;
    }

    public Long getId() {
        return Id;
    }
    public void setId(Long Id) {
        this.Id = Id;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public String getCourseType() {
        return courseType;
    }
    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }
    public String getEducation() {
        return education;
    }
    public void setEducation(String education) {
        this.education = education;
    }
    public String getStream() {
        return stream;
    }
    public void setStream(String stream) {
        this.stream = stream;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }



}

