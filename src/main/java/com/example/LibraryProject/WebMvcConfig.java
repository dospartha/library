package com.example.LibraryProject;

import com.example.LibraryProject.Interceptor.AdminCustomInterceptor;
import com.example.LibraryProject.Interceptor.LibrarianCustomInterceptor;
import com.example.LibraryProject.Interceptor.UserCustomInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new UserCustomInterceptor()).addPathPatterns("/User/**");
        registry.addInterceptor(new LibrarianCustomInterceptor()).addPathPatterns("/Librarian/**").excludePathPatterns("/Librarian/","/Librarian/signin","/Librarian/signup","/Librarian/forgetpassword","/Librarian/forgetPassword");
        registry.addInterceptor(new AdminCustomInterceptor()).addPathPatterns("/Admin/**").excludePathPatterns("/Admin/","/Admin/signin","/Admin/signup","/Admin/forgetpassword","/Admin/forgetPassword");
    }

}
