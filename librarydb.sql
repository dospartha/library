-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2018 at 09:34 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `librarydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_library`
--

CREATE TABLE `add_library` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `district_name` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `librarian_name` varchar(255) DEFAULT NULL,
  `library_code` varchar(255) DEFAULT NULL,
  `library_name` varchar(255) DEFAULT NULL,
  `mucipality_or_block` varchar(255) DEFAULT NULL,
  `pin_code` varchar(255) DEFAULT NULL,
  `police_station` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `sub_division_name` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `adhar_no` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `create_date`, `image_path`, `is_active`, `name`, `status`, `update_date`, `address`, `contact_no`, `gender`, `username`, `dob`, `adhar_no`) VALUES
(6, 'parthasarothidos@gmail.com', '123456', '2018-10-04 18:49:15', '/admin_image/Admin.jpg', 1, 'Admin', 0, '2018-10-04 18:49:15', 'hlkjdhlfgb', '7872724254', 'male', 'adminpurba', '2000-01-08', '132134564688');

-- --------------------------------------------------------

--
-- Table structure for table `block_details`
--

CREATE TABLE `block_details` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `subdivision_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_details`
--

INSERT INTO `block_details` (`id`, `name`, `subdivision_id`) VALUES
(20, 'Bhagwanpur-II', 3),
(19, 'Khejuri-II', 3),
(18, 'Khejuri-I', 3),
(17, 'Ramnagar-II', 3),
(16, 'Ramnagar-I', 3),
(15, 'Contai-III', 3),
(14, 'Deshapran', 3),
(13, 'Contai-I', 3),
(12, 'Nandigram-II', 2),
(11, 'Nandigram-I', 2),
(10, 'Haldia', 2),
(9, 'Sutahata', 2),
(8, 'Mahisadal', 2),
(7, 'Chandipur', 1),
(6, 'Panskura-I', 1),
(5, 'Kolaghat', 1),
(4, 'Shahid Matangini', 1),
(3, 'Tamluk', 1),
(2, 'Moyna', 1),
(1, 'Nandakumar', 1),
(21, 'Bhagwanpur-I', 4),
(22, 'Egra-I', 4),
(23, 'Egra-II', 4),
(24, 'Patashpur-I', 4),
(25, 'Patashpur-II', 4);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` bigint(20) NOT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `book_name` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `publisher_name` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `volume_no` varchar(255) DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL,
  `subject_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `author_name`, `book_name`, `create_date`, `edition`, `is_active`, `publisher_name`, `update_date`, `volume_no`, `library_id`, `subject_id`) VALUES
(9, 'Yashavant Kanetkar', 'Let Us C', '2018-10-04 18:55:13', '15, 2016', 1, 'Bpb Publications', '2018-10-04 18:55:13', '123654', 19, 17),
(10, 'Shanti Narayan', 'Real Analysis', '2018-10-04 19:22:45', '8th,2017', 1, 'S Chand', '2018-10-04 19:22:45', '123654', 19, 3);

-- --------------------------------------------------------

--
-- Table structure for table `bookentrydetails`
--

CREATE TABLE `bookentrydetails` (
  `id` bigint(20) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookentrydetails`
--

INSERT INTO `bookentrydetails` (`id`, `quantity`, `book_id`, `entry_date`, `user_id`, `library_id`) VALUES
(19, 10, 9, '2018-10-04 18:55:28', NULL, 19),
(20, 5, 10, '2018-10-04 19:23:19', NULL, 19);

-- --------------------------------------------------------

--
-- Table structure for table `bookissuedetails`
--

CREATE TABLE `bookissuedetails` (
  `id` bigint(20) NOT NULL,
  `issue_date` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookissuedetails`
--

INSERT INTO `bookissuedetails` (`id`, `issue_date`, `quantity`, `book_id`, `user_id`) VALUES
(22, '2018-10-04 19:00:29', 1, 9, 22),
(23, '2018-10-04 19:31:52', 1, 10, 22);

-- --------------------------------------------------------

--
-- Table structure for table `book_details`
--

CREATE TABLE `book_details` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `no_of_allocate_book` int(11) DEFAULT NULL,
  `no_of_available_book` int(11) DEFAULT NULL,
  `no_of_total_book` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_request`
--

CREATE TABLE `book_request` (
  `id` bigint(20) NOT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `request_status` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `book_entry_id` bigint(20) DEFAULT NULL,
  `book_issue_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_request`
--

INSERT INTO `book_request` (`id`, `book_id`, `library_id`, `user_id`, `request_date`, `request_status`, `approve_date`, `book_entry_id`, `book_issue_id`) VALUES
(28, 10, 19, 22, '2018-10-04 19:26:02', 1, '2018-10-04 19:30:22', NULL, 23),
(27, 9, 19, 22, '2018-10-04 18:59:07', 1, '2018-10-04 18:59:53', NULL, 22);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` bigint(20) NOT NULL,
  `course_type` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` text,
  `education` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `stream` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course_type`, `create_date`, `description`, `education`, `is_active`, `stream`, `url`, `library_id`) VALUES
(3, 'Academic Course', '2018-10-04 18:56:36', 'Course after HS', 'After Higher Secondary', 1, 'Science', 'https://https://www.shiksha.com/courses-after-12th/science', 19),
(4, 'Academic Course', '2018-10-04 18:57:13', 'Course after HS', 'After Higher Secondary', 1, 'Commerce', 'https://www.shiksha.com/courses-after-12th/commerce', 19);

-- --------------------------------------------------------

--
-- Table structure for table `e_learning_link`
--

CREATE TABLE `e_learning_link` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `topic` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL,
  `subject_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_learning_link`
--

INSERT INTO `e_learning_link` (`id`, `description`, `entry_date`, `is_active`, `topic`, `update_date`, `url`, `library_id`, `subject_id`) VALUES
(11, 'Java tutorials for beginners', '2018-10-04 18:55:53', 1, 'Java Tutorial', '2018-10-04 18:55:53', 'https://www.javatpoint.com/java-tutorial', 19, 17);

-- --------------------------------------------------------

--
-- Table structure for table `librarian`
--

CREATE TABLE `librarian` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `adhar_no` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `librarian`
--

INSERT INTO `librarian` (`id`, `email`, `password`, `create_date`, `image_path`, `is_active`, `name`, `status`, `update_date`, `address`, `contact_no`, `gender`, `username`, `dob`, `adhar_no`) VALUES
(9, 'parthasarothidos@gmail.com', '123456', '2018-10-04 18:44:14', '/librarian_image/Librarian.jpg', 1, 'Librarian', 1, '2018-10-04 18:44:14', 'fdsfdgb', '7872724254', 'male', 'librarian', '2000-01-08', '132134564688'),
(10, 'sarothi.partha87@gmail.com', '123456', '2018-10-04 19:14:32', '/librarian_image/Partha Sarothi.jpg', 1, 'Partha Sarothi', 1, '2018-10-04 19:14:32', 'fghgfhgfh', '9876543210', 'male', 'parthasarothi', '2000-01-08', '132134564689');

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE `library` (
  `id` bigint(20) NOT NULL,
  `contact_number` text,
  `create_date` datetime DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `latitude` varchar(255) DEFAULT NULL,
  `library_code` varchar(255) DEFAULT NULL,
  `library_name` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `librarian_id` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `municipality_details_id` bigint(20) DEFAULT NULL,
  `panchayet_details_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`id`, `contact_number`, `create_date`, `image_path`, `is_active`, `latitude`, `library_code`, `library_name`, `longitude`, `update_date`, `librarian_id`, `email`, `municipality_details_id`, `panchayet_details_id`) VALUES
(19, '1321321', '2018-10-04 18:52:56', '/libraryImage/Dhalpara Library.jpg', 1, '37.15888899999999', '123', 'Dhalpara Library', '-93.2685000dfg0000002', '2018-10-04 18:52:56', 9, 'dhalpara@gmail.com', NULL, 37),
(20, '1321321', '2018-10-04 19:16:01', '/libraryImage/Haldia Library.jpg', 1, '37.15888899999999', '111', 'Haldia Library', '-93.2685000dfg0000002', '2018-10-04 19:16:01', 10, 'haldia@gmail.com', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `library_details`
--

CREATE TABLE `library_details` (
  `id` bigint(20) NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `contact_number` text,
  `course_offered` text,
  `create_date` datetime DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `email_id` text,
  `is_active` tinyint(1) DEFAULT '1',
  `latitude` varchar(255) DEFAULT NULL,
  `librarian_name` varchar(255) DEFAULT NULL,
  `library_code` varchar(255) DEFAULT NULL,
  `library_name` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `municipality` varchar(255) DEFAULT NULL,
  `other_facilities` text,
  `pin_code` varchar(255) DEFAULT NULL,
  `police_station` varchar(255) DEFAULT NULL,
  `post_office` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `street_no` varchar(255) DEFAULT NULL,
  `sub_division_name` varchar(255) DEFAULT NULL,
  `total_books` varchar(255) DEFAULT NULL,
  `total_casual_stuff` varchar(255) DEFAULT NULL,
  `total_library_area` varchar(255) DEFAULT NULL,
  `total_permanent_stuff` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `village` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_grant`
--

CREATE TABLE `maintenance_grant` (
  `id` bigint(20) NOT NULL,
  `added_date` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `approved_status` tinyint(1) DEFAULT '0',
  `attachment` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `item_name` varchar(255) DEFAULT NULL,
  `librarian_name` varchar(255) DEFAULT NULL,
  `library_code` varchar(255) DEFAULT NULL,
  `library_name` varchar(255) DEFAULT NULL,
  `maintenance_type` varchar(255) DEFAULT NULL,
  `reject_by` varchar(255) DEFAULT NULL,
  `reject_date` datetime DEFAULT NULL,
  `reject_status` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `total_cost` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `municipality_details`
--

CREATE TABLE `municipality_details` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `subdivision_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `municipality_details`
--

INSERT INTO `municipality_details` (`id`, `name`, `subdivision_id`) VALUES
(1, 'Tamluk Municipality', 1),
(2, 'Panskura Municipality', 1),
(3, 'Haldia Municipality', 2),
(4, 'Contai Municipality', 3),
(5, 'Egra Municipality', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL,
  `added_by` varchar(255) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `approved_status` tinyint(1) DEFAULT '0',
  `attachment` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` text,
  `forward_by` varchar(255) DEFAULT NULL,
  `forward_date` datetime DEFAULT NULL,
  `forward_status` tinyint(1) DEFAULT '0',
  `forward_to` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `notice_for_admin` tinyint(1) DEFAULT '0',
  `notice_for_librarian` tinyint(1) DEFAULT '0',
  `notice_for_user` tinyint(1) DEFAULT '0',
  `notification_date` varchar(255) DEFAULT NULL,
  `notification_time` varchar(255) DEFAULT NULL,
  `reject_by` varchar(255) DEFAULT NULL,
  `reject_date` datetime DEFAULT NULL,
  `reject_status` tinyint(1) DEFAULT '0',
  `send_to` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `subject` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_and_suggestion`
--

CREATE TABLE `notification_and_suggestion` (
  `id` bigint(20) NOT NULL,
  `added_by` varchar(255) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `approved_status` tinyint(1) DEFAULT '0',
  `attachment` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` text,
  `forward_by` varchar(255) DEFAULT NULL,
  `forward_date` datetime DEFAULT NULL,
  `forward_status` tinyint(1) DEFAULT '0',
  `forward_to` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `library_code` varchar(255) DEFAULT NULL,
  `notice_for_admin` tinyint(1) DEFAULT '0',
  `notice_for_librarian` tinyint(1) DEFAULT '0',
  `notice_for_user` tinyint(1) DEFAULT '0',
  `notification_date` varchar(255) DEFAULT NULL,
  `notification_time` varchar(255) DEFAULT NULL,
  `reject_by` varchar(255) DEFAULT NULL,
  `reject_date` datetime DEFAULT NULL,
  `reject_status` tinyint(1) DEFAULT '0',
  `send_to` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `subject` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `panchayet_details`
--

CREATE TABLE `panchayet_details` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `block_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panchayet_details`
--

INSERT INTO `panchayet_details` (`id`, `name`, `block_id`) VALUES
(93, 'Ashadtalia', 9),
(94, 'Chaitanyapur', 9),
(95, 'Deulpota', 9),
(96, 'Guaberia', 9),
(97, 'Horekhali', 9),
(98, 'Joynagar', 9),
(99, 'Kukrahati', 9),
(100, 'Baruttarhingly', 10),
(101, 'Chakdwipa', 10),
(102, 'Debhog', 10),
(103, 'Deulpota', 10),
(104, 'Bhekutia', 11),
(105, 'Daudpur', 11),
(106, 'Gokulnagar', 11),
(107, 'Haripur', 11),
(108, 'Kalicharanpur', 11),
(109, 'Kendemarijalpai', 11),
(110, 'Mahammadpur', 11),
(111, 'Samsabad', 11),
(112, 'Sonachura', 11),
(113, 'Amdabad I', 12),
(114, 'Amdabad II', 12),
(115, 'Birulia', 12),
(116, 'Boyal I', 12),
(117, 'Boyal II', 12),
(118, 'Khodambari I', 12),
(119, 'Khodambari II', 12),
(120, 'Badalpur', 13),
(121, 'Dulalpur', 13),
(122, 'Haipur', 13),
(123, 'Mahisagote', 13),
(124, 'Majilapur', 13),
(125, 'Nayaput', 13),
(126, 'Raipur Paschimbar', 13),
(127, 'Sabajpur', 13),
(128, 'Amtalia', 14),
(129, 'Aurai', 14),
(130, 'Bamunia', 14),
(131, 'Basantia', 14),
(132, 'Chalti', 14),
(133, 'Dariapur', 14),
(134, 'Dhobaberia', 14),
(135, 'Sarda', 14),
(136, 'Bhajachauli', 15),
(137, 'Debendra', 15),
(138, 'Durmuth', 15),
(139, 'Kaniadighi', 15),
(140, 'Kumirda', 15),
(141, 'Kusumpur', 15),
(142, 'Lauda', 15),
(143, 'Marishda', 15),
(144, 'Basantapur', 16),
(145, 'Gobra', 16),
(146, 'Haldia I', 16),
(147, 'Haldia II', 16),
(148, 'Padima I', 16),
(149, 'Padima II', 16),
(150, 'Badhia', 16),
(151, 'Telgachhari I', 16),
(152, 'Telgachhari II', 16),
(153, 'Badalpur', 17),
(154, 'Balisai', 17),
(155, 'Depal', 17),
(156, 'Kadua', 17),
(157, 'Kalindi', 17),
(158, 'Maithana', 17),
(159, 'Paldhui', 17),
(160, 'Satilapur', 17),
(161, 'Birbandar', 18),
(162, 'Heria', 18),
(163, 'Kalagechia', 18),
(164, 'Kamarda', 18),
(165, 'Lakshi', 18),
(166, 'Tikashi', 18),
(167, 'Baratala', 19),
(168, 'Haludbari', 19),
(169, 'Janka', 19),
(170, 'Khejuri', 19),
(171, 'Nijkasba', 19),
(172, 'Arjunnagar', 20),
(173, 'Basudevberia', 20),
(174, 'Boroj', 20),
(175, 'Garbari I', 20),
(92, 'Satish Samanta', 8),
(91, 'Natshal II', 8),
(90, 'Natshal I', 8),
(89, 'Lakshya II', 8),
(88, 'Lakshya I', 8),
(87, 'Kismat-Naikundi', 8),
(86, 'Itamogra II', 8),
(85, 'Itamogra I', 8),
(84, 'Garhkamalpur', 8),
(83, 'Betkundu', 8),
(82, 'Amritberia', 8),
(81, 'Usmanpur', 7),
(80, 'Nandapur-Baraghuni', 7),
(79, 'Kulbari', 7),
(78, 'Jalpai', 7),
(77, 'Iswarpur', 7),
(76, 'Dibakarpur', 7),
(75, 'Chowkhali', 7),
(74, 'Brindabanpur II', 7),
(73, 'Brindabanpur I', 7),
(72, 'Brajalalchak', 7),
(71, 'Raghunathbari', 6),
(70, 'Radhaballavchak', 6),
(69, 'Purusottampur', 6),
(68, 'Pratapur II', 6),
(67, 'Pratapur I', 6),
(66, 'Panskura I', 6),
(65, 'Mysora', 6),
(64, 'Khandakhola', 6),
(63, 'Keshapat', 6),
(62, 'Haur', 6),
(61, 'Gobindanagar', 6),
(60, 'Ghoshpur', 6),
(59, 'Chaitanyapur II', 6),
(58, 'Chaitanyapur I', 6),
(57, 'Siddha II', 5),
(56, 'Siddha I', 5),
(55, 'Sagarbarh', 5),
(54, 'Pulsita', 5),
(53, 'Kola II', 5),
(52, 'Kola I', 5),
(51, 'Khanyadihi', 5),
(50, 'Gopal Nagar', 5),
(49, 'Deriachak', 5),
(48, 'Brindabanchak', 5),
(47, 'Bhogpur', 5),
(46, 'Baisnabchak', 5),
(45, 'Amalhanda', 5),
(44, 'Santipur II', 4),
(43, 'Santipur I', 4),
(42, 'Raghunathpur II', 4),
(41, 'Raghunathpur I', 4),
(40, 'Kharui II', 4),
(39, 'Kharui I', 4),
(38, 'Kakharda', 4),
(37, 'Dhalpara', 4),
(36, 'Balluk II', 4),
(35, 'Balluk I', 4),
(34, 'Srirampur II', 3),
(33, 'Srirampur I', 3),
(32, 'Pipulberia II', 3),
(31, 'Pipulberia I', 3),
(30, 'Padumpur II', 3),
(29, 'Padumpur I', 3),
(28, 'Nilkuntha', 3),
(27, 'Bishnubar II', 3),
(26, 'Bishnubar I', 3),
(25, 'Anantapur II', 3),
(24, 'Anantapur I', 3),
(23, 'Tilkhoja', 2),
(22, 'Srikantha', 2),
(21, 'Ramchak', 2),
(20, 'Paramanandapur', 2),
(19, 'Naichanpur II', 2),
(18, 'Naichanpur I', 2),
(17, 'Moyna II', 2),
(16, 'Moyna I', 2),
(15, 'Gokulnagar', 2),
(14, 'Gojina', 2),
(13, 'Bakcha', 2),
(12, 'Sitalpur Paschim', 1),
(11, 'Saorberia-Jalpai II', 1),
(10, 'Saorberia-Jalpai I', 1),
(9, 'Kumarchak', 1),
(8, 'Kumarara', 1),
(7, 'Kalyanpur', 1),
(6, 'Dakshin Narikeldanga', 1),
(5, 'Chaksimulia', 1),
(4, 'Byabattarhat Purba', 1),
(3, 'Byabattarhat Paschim', 1),
(2, 'Basudebpur', 1),
(1, 'Bargodadgdar', 1),
(176, 'Garbari II', 20),
(177, 'Itaberia', 20),
(178, 'Jukhia', 20),
(179, 'Mugberia', 20),
(180, 'Radhapur', 20),
(181, 'Benudia', 21),
(182, 'Bhagbanpur', 21),
(183, 'Bibhisanpur', 21),
(184, 'Gurgram', 21),
(185, 'Kajlagarh', 21),
(186, 'Kakra', 21),
(187, 'Koatbarh', 21),
(188, 'Mahammadpur I', 21),
(189, 'Mahammadpur II', 21),
(190, 'Simulia', 21),
(191, 'Barida', 22),
(192, 'Chhatri', 22),
(193, 'Jerthan', 22),
(194, 'Jumki', 22),
(195, 'Kasba Egra', 22),
(196, 'Panchrol', 22),
(197, 'Rishi Bankimchandra', 22),
(198, 'Sahara', 22),
(199, 'Basudevpur', 23),
(200, 'Bathuary', 23),
(201, 'Deshbandhu', 23),
(202, 'Dubda', 23),
(203, 'Manjushree', 23),
(204, 'Paniparul', 23),
(205, 'Sarbodaya', 23),
(206, 'Vivekananda', 23),
(207, 'Amarshi I', 24),
(208, 'Amarshi II', 24),
(209, 'Barhat', 24),
(210, 'Brajalalpur', 24),
(211, 'Chistipur I', 24),
(212, 'Chistipur II', 24),
(213, 'Gokulpur', 24),
(214, 'Gopalpur', 24),
(215, 'Naipur', 24),
(216, 'Argoal', 25),
(217, 'Khar', 25),
(218, 'Mathura', 25),
(219, 'Panchet', 25),
(220, 'Patashpur', 25),
(221, 'South Khanda', 25),
(222, 'Srirampur', 25),
(223, 'Nandigram', 11);

-- --------------------------------------------------------

--
-- Table structure for table `report_and_certificate`
--

CREATE TABLE `report_and_certificate` (
  `id` bigint(20) NOT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `approved_status` tinyint(1) DEFAULT '0',
  `attachment` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `librarian_name` varchar(255) DEFAULT NULL,
  `library_code` varchar(255) DEFAULT NULL,
  `library_name` varchar(255) DEFAULT NULL,
  `reject_by` varchar(255) DEFAULT NULL,
  `reject_date` datetime DEFAULT NULL,
  `reject_status` tinyint(1) DEFAULT '0',
  `report_type` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------


--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` bigint(20) NOT NULL,
  `subject_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `subject_name`) VALUES
(21, 'Zoology'),
(20, 'Botany'),
(19, 'Magazine'),
(18, 'Medical books'),
(17, 'Computer Science'),
(16, 'Information Technology'),
(15, 'Technology'),
(14, 'Journal'),
(13, 'Modern History'),
(12, 'Hindi Literature'),
(11, 'Sociology'),
(10, 'Phisiology'),
(9, 'Biology'),
(8, 'Chemistry'),
(7, 'Phiscis'),
(6, 'General Science'),
(5, 'geography'),
(4, 'History'),
(3, 'Mathematics'),
(2, 'English literature'),
(1, 'Bengali literature'),
(22, 'Political Science'),
(23, 'Accountancy'),
(24, 'General Khowledge'),
(25, 'Law'),
(26, 'Story Books'),
(27, 'Comics'),
(28, 'Science Fiction'),
(29, 'Fairy Tales');

-- --------------------------------------------------------

--
-- Table structure for table `sub_division_details`
--

CREATE TABLE `sub_division_details` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_division_details`
--

INSERT INTO `sub_division_details` (`id`, `name`) VALUES
(1, 'TAMLUK'),
(2, 'HALDIA'),
(3, 'CONTAI'),
(4, 'EGRA');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `library_id` bigint(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `adhar_no` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `create_date`, `email`, `image_path`, `is_active`, `name`, `password`, `status`, `update_date`, `library_id`, `address`, `contact_no`, `gender`, `username`, `dob`, `adhar_no`) VALUES
(22, '2018-10-04 18:53:59', 'parthasarothidos@gmail.com', '/user_Image/Partha Sarothi.jpg', 1, 'Partha Sarothi', 'vDeNN;', 1, '2018-10-04 18:53:59', 19, 'hgffgjnfg', '7872724254', 'male', 'parthasarothi', '2000-01-08', '132134564688');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_library`
--
ALTER TABLE `add_library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_c0r9atamxvbhjjvy5j8da1kam` (`email`),
  ADD UNIQUE KEY `UK_gfn44sntic2k93auag97juyij` (`username`),
  ADD UNIQUE KEY `UK_8n0mqjr71sfcu788f52h6ba9v` (`adhar_no`);

--
-- Indexes for table `block_details`
--
ALTER TABLE `block_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK2rb2jmmvbr70psk96ag67iad6` (`subdivision_id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKaojxagnfmppd09k35kye5eph5` (`library_id`),
  ADD KEY `FK7ql5tfbkjvbc3k9xhtfva8nxq` (`subject_id`);

--
-- Indexes for table `bookentrydetails`
--
ALTER TABLE `bookentrydetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKb3nifrrkr82enmfrdkrrn2ffs` (`book_id`);

--
-- Indexes for table `bookissuedetails`
--
ALTER TABLE `bookissuedetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKqjj6ryoqdo7rjvisfuri6fgqo` (`book_id`);

--
-- Indexes for table `book_details`
--
ALTER TABLE `book_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKowm7t5r8qjacwt8wxeag6l1r` (`book_id`);

--
-- Indexes for table `book_request`
--
ALTER TABLE `book_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKhqju29xkx4a779hsu1o3jywm4` (`library_id`);

--
-- Indexes for table `e_learning_link`
--
ALTER TABLE `e_learning_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKdmegivhah0jxomcda4xe4uiw6` (`library_id`),
  ADD KEY `FKhnerymtuxcgoxdll9lqafo8bl` (`subject_id`);

--
-- Indexes for table `librarian`
--
ALTER TABLE `librarian`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_csxsqgaf8936ljpw57xawwr4e` (`email`),
  ADD UNIQUE KEY `UK_glh4t2l2m662sacuyko7lit7p` (`username`),
  ADD UNIQUE KEY `UK_68ckmh407ecahofnyg0tycequ` (`adhar_no`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_32hgmtpicig9hgt2mybin0kva` (`library_code`),
  ADD UNIQUE KEY `UK_rxgiipxnarh7ihhhjtq931a78` (`email`),
  ADD KEY `FKaaq764dlk846xhcc9y6nse9st` (`librarian_id`),
  ADD KEY `FKnlvqgy33vf41wh55wrtei7sdk` (`municipality_details_id`),
  ADD KEY `FK6l9tle32p5qffjjuqijji64ws` (`panchayet_details_id`);

--
-- Indexes for table `library_details`
--
ALTER TABLE `library_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintenance_grant`
--
ALTER TABLE `maintenance_grant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `municipality_details`
--
ALTER TABLE `municipality_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKk3gr6yk316yw4u17598pag6g1` (`subdivision_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1haeyjx0h79cxice8dip9ap3a` (`library_id`);

--
-- Indexes for table `notification_and_suggestion`
--
ALTER TABLE `notification_and_suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panchayet_details`
--
ALTER TABLE `panchayet_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK2a905bq22p827yn3lventpmog` (`block_id`);

--
-- Indexes for table `report_and_certificate`
--
ALTER TABLE `report_and_certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_division_details`
--
ALTER TABLE `sub_division_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  ADD UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`),
  ADD UNIQUE KEY `UK_ju7lblmwebt0p21tkmohiqi63` (`adhar_no`),
  ADD KEY `FKbfbrr0di61ixdf940wfbd9v6r` (`library_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_library`
--
ALTER TABLE `add_library`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `block_details`
--
ALTER TABLE `block_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `bookentrydetails`
--
ALTER TABLE `bookentrydetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `bookissuedetails`
--
ALTER TABLE `bookissuedetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `book_details`
--
ALTER TABLE `book_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `book_request`
--
ALTER TABLE `book_request`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `e_learning_link`
--
ALTER TABLE `e_learning_link`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `librarian`
--
ALTER TABLE `librarian`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `library_details`
--
ALTER TABLE `library_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `maintenance_grant`
--
ALTER TABLE `maintenance_grant`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `municipality_details`
--
ALTER TABLE `municipality_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_and_suggestion`
--
ALTER TABLE `notification_and_suggestion`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `panchayet_details`
--
ALTER TABLE `panchayet_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;
--
-- AUTO_INCREMENT for table `report_and_certificate`
--
ALTER TABLE `report_and_certificate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `sub_division_details`
--
ALTER TABLE `sub_division_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
